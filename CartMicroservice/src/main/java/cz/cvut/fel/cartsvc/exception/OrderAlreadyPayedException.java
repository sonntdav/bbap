package cz.cvut.fel.cartsvc.exception;

public class OrderAlreadyPayedException extends RuntimeException{
    public OrderAlreadyPayedException(String message) {
        super(message);
    }
}
