package cz.cvut.fel.cartsvc.kafka.transfer;

import java.io.Serializable;

public class CartCreatedResponse implements Serializable {
    private Integer accountId;
    private Integer cartId;

    public CartCreatedResponse(Integer accountId, Integer cartId) {
        this.accountId = accountId;
        this.cartId = cartId;
    }

    public Integer getAccountId() {
        return accountId;
    }

    public void setAccountId(Integer accountId) {
        this.accountId = accountId;
    }

    public Integer getCartId() {
        return cartId;
    }

    public void setCartId(Integer cartId) {
        this.cartId = cartId;
    }
}
