package cz.cvut.fel.cartsvc.kafka;

import cz.cvut.fel.cartsvc.kafka.transfer.CartCreatedResponse;
import cz.cvut.fel.cartsvc.model.CartEntity;
import cz.cvut.fel.cartsvc.service.CartService;
import lombok.AllArgsConstructor;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.stereotype.Component;

@Component
public class MyKafkaListener {
    private final CartService cartService;
    private final MyKafkaProducer kafkaProducer;

    @Autowired
    public MyKafkaListener(CartService cartService, MyKafkaProducer kafkaProducer) {
        this.cartService = cartService;
        this.kafkaProducer = kafkaProducer;
    }

    @KafkaListener(topics = "accountCreated", groupId = "cart_group_id")
    public void accountWasCreated(Integer accountID){
        CartEntity cart = cartService.createCart(accountID);
        CartCreatedResponse cartCreatedResponse = new CartCreatedResponse(accountID, cart.getCartId());
        kafkaProducer.cartWasCreated(cartCreatedResponse);
    }
}
