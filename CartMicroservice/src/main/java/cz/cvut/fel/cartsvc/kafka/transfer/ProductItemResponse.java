package cz.cvut.fel.cartsvc.kafka.transfer;

import java.io.Serializable;

public class ProductItemResponse implements Serializable {
    private Integer productEntityID;
    private Integer amount;



    public ProductItemResponse(Integer productEntityID,Integer amount) {
        this.amount = amount;
        this.productEntityID = productEntityID;
    }

    public Integer getProductEntityID() {
        return productEntityID;
    }

    public void setProductEntityID(Integer productEntityID) {
        this.productEntityID = productEntityID;
    }

    public Integer getAmount() {
        return amount;
    }

    public void setAmount(Integer amount) {
        this.amount = amount;
    }


}
