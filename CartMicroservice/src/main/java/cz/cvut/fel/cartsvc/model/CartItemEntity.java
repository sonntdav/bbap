package cz.cvut.fel.cartsvc.model;

import com.fasterxml.jackson.annotation.JsonBackReference;
import jakarta.persistence.*;
import lombok.Data;
import lombok.NoArgsConstructor;

@Entity
@Data
@NoArgsConstructor
@Table(name = "cartItem_MC")
public class CartItemEntity {
    public CartItemEntity(Integer product, CartEntity cartEntity){
        this.setAmount(1);
        this.setCartEntity(cartEntity);
        this.setProductEntity(product);
    }

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Integer CartItemId;
    private Integer amount;

    private Integer productEntity;
    @JsonBackReference
    @ManyToOne(cascade = CascadeType.ALL,fetch = FetchType.EAGER)
    private CartEntity cartEntity;

    public void increaseAmount(){
        this.amount++;
    }
    public void decreaseAmount(){
        this.amount--;
    }
}
