package cz.cvut.fel.cartsvc.service.impl;

import cz.cvut.fel.cartsvc.exception.ItemNotInCartException;
import cz.cvut.fel.cartsvc.kafka.MyKafkaProducer;
import cz.cvut.fel.cartsvc.kafka.transfer.CartResponse;
import cz.cvut.fel.cartsvc.kafka.transfer.ProductItemResponse;
import cz.cvut.fel.cartsvc.model.CartEntity;
import cz.cvut.fel.cartsvc.model.CartItemEntity;
import cz.cvut.fel.cartsvc.repository.CartRepository;

import cz.cvut.fel.cartsvc.service.CartService;
import jakarta.persistence.EntityNotFoundException;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
@AllArgsConstructor
@Slf4j
public class CartServiceImpl implements CartService {
    private CartRepository cartRepository;
    private MyKafkaProducer kafkaProducer;
    /**
     * Retrieves the {@link CartEntity} with the specified ID.
     *
     * @param cartId the ID of the cart to retrieve
     * @return the {@link CartEntity} with the specified ID
     * @throws EntityNotFoundException if the cart is not found
     */
    @Override
    public CartEntity getCart(Integer cartId) {
        return cartRepository.findById(cartId).orElseThrow(() -> new EntityNotFoundException("cart was not found. ID of cart: " + cartId));
    }

    @Override
    public CartEntity createCart(Integer accountID) {
        CartEntity cartEntity = new CartEntity(accountID);
        cartRepository.save(cartEntity);
        return cartEntity;
    }

    /**
     * Adds a product to the specified cart.
     *
     * @param cartID    the ID of the cart
     * @param productID the ID of the product to add
     * @throws EntityNotFoundException if the cart or product is not found
     */
    @Override
    public void addProduct(Integer cartID, Integer productID) {
        CartEntity cartEntity = cartRepository.findById(cartID).orElseThrow(() -> new EntityNotFoundException("cart was not found. ID of cart: " + cartID));
        for (CartItemEntity cartItemEntity : cartEntity.getCartItemEntities()) {
            if(cartItemEntity.getProductEntity().equals(productID)){
                increaseQuantity(cartID,productID);
                return;
            }
        }
        CartItemEntity newCartItemEntity = new CartItemEntity(productID,cartEntity);
        cartEntity.getCartItemEntities().add(newCartItemEntity);
        cartRepository.save(cartEntity);
    }

    /**
     * Increases the quantity of a product in the specified cart.
     *
     * @param cartID    the ID of the cart
     * @param productID the ID of the product
     * @throws EntityNotFoundException    if the cart or product is not found
     * @throws ItemNotInCartException     if the product is not in the cart
     */
    @Override
    public void increaseQuantity(Integer cartID, Integer productID) {
        CartEntity cartEntity = cartRepository.findById(cartID).orElseThrow(() -> new EntityNotFoundException("cart was not found. ID of cart: " + cartID));
        for (CartItemEntity cartItemEntity : cartEntity.getCartItemEntities()) {
            if(cartItemEntity.getProductEntity().equals(productID)){
                cartItemEntity.increaseAmount();
                cartRepository.save(cartEntity);
                return;
            }
        }
        throw new ItemNotInCartException("product with this ID was not in the cart!");
    }

    /**
     * Decreases the quantity of a product in the specified cart.
     * If the quantity becomes less than 1, the item is removed from the cart.
     *
     * @param cartID    the ID of the cart
     * @param productID the ID of the product
     * @throws EntityNotFoundException    if the cart or product is not found
     * @throws ItemNotInCartException     if the product is not in the cart
     */
    @Override
    public void decreaseQuantity(Integer cartID, Integer productID) {
        CartEntity cartEntity = cartRepository.findById(cartID).orElseThrow(() -> new EntityNotFoundException("cart was not found. ID of cart: " + cartID));
        for (CartItemEntity cartItemEntity : cartEntity.getCartItemEntities()) {
            if(cartItemEntity.getProductEntity().equals(productID)){
                cartItemEntity.decreaseAmount();
                if (cartItemEntity.getAmount() < 1){
                    removeItem(cartID,productID);
                }
                else {
                    cartRepository.save(cartEntity);
                }
                return;
            }
        }
        throw new ItemNotInCartException("product with this ID was not in the cart!");
    }


    /**
     * Removes an item from the specified cart.
     *
     * @param cartID    the ID of the cart
     * @param productID the ID of the product to remove
     * @throws EntityNotFoundException if the cart or product is not found
     * @throws ItemNotInCartException  if the product is not in the cart
     */
    @Override
    public void removeItem(Integer cartID, Integer productID) {
        CartEntity cartEntity = cartRepository.findById(cartID).orElseThrow(() -> new EntityNotFoundException("cart was not found. ID of cart: " + cartID));

        for (CartItemEntity cartItemEntity : cartEntity.getCartItemEntities()) {
            if(cartItemEntity.getProductEntity().equals(productID)){
                cartEntity.getCartItemEntities().remove(cartItemEntity);
                cartRepository.save(cartEntity);
                return;
            }
        }
        throw new ItemNotInCartException("product with this ID was not in the cart!");
    }


    /**
     * Retrieves all items in the specified cart.
     *
     * @param cartID the ID of the cart
     * @return a list of {@link CartItemEntity} representing the items in the cart
     * @throws EntityNotFoundException if the cart is not found
     */
    @Override
    public List<CartItemEntity> getAllItemsInCart(Integer cartID) {
        CartEntity cartEntity = cartRepository.findById(cartID).orElseThrow(() -> new EntityNotFoundException("cart was not found. ID of cart: " + cartID));
        return cartEntity.getCartItemEntities();
    }

    /**
     * Performs the checkout process for the specified cart,
     * creating an order with the cart items and associating it with the account.
     *
     * @param cartID the ID of the cart
     * @throws EntityNotFoundException if the cart is not found
     */
    @Override
    public void checkout(Integer cartID) {
        CartEntity cartEntity = cartRepository.findById(cartID).orElseThrow(() -> new EntityNotFoundException("cart was not found. ID of cart: " + cartID));

        CartResponse cartResponse = new CartResponse(cartEntity.getAccount());
        for (CartItemEntity cartItemEntity : cartEntity.getCartItemEntities()) {
            cartResponse.addItem(cartItemEntity.getProductEntity(), cartItemEntity.getAmount());
        }
        kafkaProducer.createOrder(cartResponse);

        cartEntity.setCartItemEntities(new ArrayList<>());
        cartRepository.save(cartEntity);
    }

}
