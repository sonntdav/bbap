package cz.cvut.fel.cartsvc.service;


import cz.cvut.fel.cartsvc.model.CartEntity;
import cz.cvut.fel.cartsvc.model.CartItemEntity;

import java.util.List;

public interface CartService {
    CartEntity getCart(Integer cartId);

    CartEntity createCart(Integer accountID);

    void addProduct(Integer cartID, Integer productID);

    void increaseQuantity(Integer cartID, Integer productID);

    void decreaseQuantity(Integer cartID, Integer productID);

    void removeItem(Integer cartID, Integer productID);

    List<CartItemEntity> getAllItemsInCart(Integer cartID);

    void checkout(Integer cartID);


}

