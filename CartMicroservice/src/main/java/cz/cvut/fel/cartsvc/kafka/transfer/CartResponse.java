package cz.cvut.fel.cartsvc.kafka.transfer;

import java.util.ArrayList;
import java.util.List;

public class CartResponse {
    private Integer accID;
    private List<ProductItemResponse> productItemResponses;

    public CartResponse(Integer accID, List<ProductItemResponse> productItemResponses) {
        this.accID = accID;
        this.productItemResponses = productItemResponses;
    }

    public CartResponse(Integer accID) {
        this.accID = accID;
        this.productItemResponses = new ArrayList<>();
    }

    public Integer getAccID() {
        return accID;
    }

    public void setAccID(Integer accID) {
        this.accID = accID;
    }

    public List<ProductItemResponse> getProductItemResponses() {
        return productItemResponses;
    }

    public void setProductItemResponses(List<ProductItemResponse> productItemResponses) {
        this.productItemResponses = productItemResponses;
    }

    public void addItem(Integer productID, int amount){
        this.productItemResponses.add(new ProductItemResponse(productID,amount));
    }
}
