package cz.cvut.fel.cartsvc.kafka;

import com.fasterxml.jackson.core.JsonProcessingException;
import cz.cvut.fel.cartsvc.kafka.transfer.CartCreatedResponse;
import cz.cvut.fel.cartsvc.kafka.transfer.CartResponse;
import cz.cvut.fel.cartsvc.kafka.transfer.ProductItemResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.stereotype.Component;

import java.util.List;


@Component
public class MyKafkaProducer {
    private final JsonSerializer jsonSerializer = new JsonSerializer();
    private final KafkaTemplate<String, String> kafkaTemplate;
    @Autowired
    public MyKafkaProducer(KafkaTemplate<String, String> kafkaTemplate) {
        this.kafkaTemplate = kafkaTemplate;
    }

    public void cartWasCreated(CartCreatedResponse cartCreatedResponse){
        try {
            kafkaTemplate.send("cartCreated", jsonSerializer.transferClassToJson(cartCreatedResponse));
        } catch (JsonProcessingException e) {
            e.printStackTrace();
        }
    }

    public void createOrder(CartResponse cartResponse){
        try {
            kafkaTemplate.send("createOrder", jsonSerializer.transferClassToJson(cartResponse));
        } catch (JsonProcessingException e) {
            e.printStackTrace();
        }
    }
}
