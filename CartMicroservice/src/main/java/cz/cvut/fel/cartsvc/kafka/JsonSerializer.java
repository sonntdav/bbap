package cz.cvut.fel.cartsvc.kafka;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import cz.cvut.fel.cartsvc.kafka.transfer.CartCreatedResponse;

public class JsonSerializer {
    private final ObjectMapper obj = new ObjectMapper();

    public CartCreatedResponse departmentFromJson(String json) throws JsonProcessingException {
        return obj.readValue(json,CartCreatedResponse.class);
    }

    public String transferClassToJson(Object o) throws JsonProcessingException {
        return obj.writeValueAsString(o);
    }

}


