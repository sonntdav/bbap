package cz.cvut.fel.cartsvc.model;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonManagedReference;
import jakarta.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

@Entity
@Data
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "cart_MC")
public class CartEntity {
    public CartEntity(Integer account){
        this.account = account;
        cartItemEntities = new ArrayList<>();
    }

    public CartEntity(Integer account, List<CartItemEntity> cartItemEntities) {
        this.account = account;
        this.cartItemEntities = cartItemEntities;
    }

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Integer CartId;
    private Integer account;

    @JsonManagedReference
    @OneToMany(mappedBy = "cartEntity",cascade = CascadeType.ALL,fetch = FetchType.EAGER)
    private List<CartItemEntity> cartItemEntities;

    @Override
    public String toString() {
        return "CartEntity{" +
                "CartId=" + CartId +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof CartEntity)) return false;
        CartEntity that = (CartEntity) o;
        return CartId.equals(that.CartId) && account.equals(that.account);
    }

    @Override
    public int hashCode() {
        return Objects.hash(CartId, account);
    }
}
