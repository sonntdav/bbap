package cz.cvut.fel.cartsvc.exception;

public class ItemNotInCartException extends RuntimeException{
    public ItemNotInCartException(String message) {
        super(message);
    }
}