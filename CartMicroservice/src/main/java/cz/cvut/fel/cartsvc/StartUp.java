package cz.cvut.fel.cartsvc;

import cz.cvut.fel.cartsvc.service.CartService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.event.ApplicationReadyEvent;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public class StartUp {
    private final CartService paymentService;

    @Autowired
    public StartUp(CartService reviewService) {
        this.paymentService = reviewService;
    }

    @EventListener(ApplicationReadyEvent.class)
    public void init() {
        paymentService.createCart(1);
        paymentService.addProduct(1,1);
    }

}
