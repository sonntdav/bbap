package cz.cvut.fel.cartsvc.repository;

import cz.cvut.fel.cartsvc.model.CartEntity;
import org.springframework.data.jpa.repository.JpaRepository;

public interface CartRepository extends JpaRepository<CartEntity,Integer> {
}
