package cz.cvut.fel.wishlistsvc.kafka;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;


public class JsonSerializer {
    private final ObjectMapper obj = new ObjectMapper();

    public String transferClassToJson(Object o) throws JsonProcessingException {
        return obj.writeValueAsString(o);
    }



}


