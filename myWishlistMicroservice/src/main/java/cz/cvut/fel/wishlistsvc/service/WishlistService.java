package cz.cvut.fel.wishlistsvc.service;

import cz.cvut.fel.wishlistsvc.model.WishlistEntity;

import java.util.List;

public interface WishlistService {
    void addItemToWishlist(Integer wishlistId,Integer itemId);

    void removeItemFromWishlist(Integer wishlistId,Integer itemId);

    WishlistEntity getWishlist(Integer id);

    List<Integer> getAllItemsFromWishList(Integer id);

    WishlistEntity createWishlist(Integer accID);
}

