package cz.cvut.fel.wishlistsvc.kafka;

import com.fasterxml.jackson.core.JsonProcessingException;
import cz.cvut.fel.wishlistsvc.kafka.transfer.WishlistCreatedResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.stereotype.Component;

import java.io.Serializable;

@Component
public class MyKafkaProducer implements Serializable {
    private final KafkaTemplate<String, String> kafkaTemplate;
    private final JsonSerializer jsonSerializer = new JsonSerializer();
    @Autowired
    public MyKafkaProducer(KafkaTemplate<String, String> kafkaTemplate) {
        this.kafkaTemplate = kafkaTemplate;
    }

    public void wishlistCreated(Integer reviewID, Integer accountID){
        WishlistCreatedResponse wishlistCreatedResponse = new WishlistCreatedResponse(accountID,reviewID);
        try {
            kafkaTemplate.send("wishlistCreated",jsonSerializer.transferClassToJson(wishlistCreatedResponse));
        } catch (JsonProcessingException e) {
            e.printStackTrace();
        }
    }
}
