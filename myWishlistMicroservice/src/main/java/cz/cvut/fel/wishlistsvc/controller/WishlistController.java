package cz.cvut.fel.wishlistsvc.controller;

import cz.cvut.fel.wishlistsvc.model.WishlistEntity;
import cz.cvut.fel.wishlistsvc.service.WishlistService;
import jakarta.persistence.EntityNotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/wishlist")
public class WishlistController {
    private final WishlistService wishlistService;

    @Autowired
    public WishlistController(WishlistService wishlistService) {
        this.wishlistService = wishlistService;
    }

    @PostMapping("/{wishlistId}/items/{itemId}")
    public ResponseEntity<?> addItemToWishlist(
            @PathVariable Integer wishlistId,
            @PathVariable Integer itemId
    ) {
        try {
            wishlistService.addItemToWishlist(wishlistId, itemId);
            return ResponseEntity.ok().build();
        } catch (EntityNotFoundException e) {
            return ResponseEntity.status(HttpStatus.NOT_FOUND).body(e.getMessage());
        }
    }

    @DeleteMapping("/{wishlistId}/items/{itemId}")
    public ResponseEntity<?> removeItemFromWishlist(
            @PathVariable Integer wishlistId,
            @PathVariable Integer itemId
    ) {
        try {
            wishlistService.removeItemFromWishlist(wishlistId, itemId);
            return ResponseEntity.ok().build();
        } catch (EntityNotFoundException e) {
            return ResponseEntity.status(HttpStatus.NOT_FOUND).body(e.getMessage());
        }
    }

    @GetMapping("/{wishlistId}/all")
    public ResponseEntity<?> getAllItemsFromWishlist(@PathVariable Integer wishlistId) {
        try {
            List<Integer> items = wishlistService.getAllItemsFromWishList(wishlistId);
            return ResponseEntity.ok(items);
        } catch (EntityNotFoundException e) {
            return ResponseEntity.status(HttpStatus.NOT_FOUND).body(e.getMessage());
        }
    }

    @GetMapping("/{wishlistId}")
    public ResponseEntity<?> getWishlist(@PathVariable Integer wishlistId) {
        try {
            WishlistEntity wishlist = wishlistService.getWishlist(wishlistId);
            return ResponseEntity.ok(wishlist);
        } catch (EntityNotFoundException e) {
            return ResponseEntity.status(HttpStatus.NOT_FOUND).body(e.getMessage());
        }
    }
}
