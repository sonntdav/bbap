package cz.cvut.fel.wishlistsvc.repository;


import cz.cvut.fel.wishlistsvc.model.WishlistEntity;
import org.springframework.data.jpa.repository.JpaRepository;

public interface WishlistRepository extends JpaRepository<WishlistEntity, Integer> {
}
