package cz.cvut.fel.wishlistsvc.model;

import com.fasterxml.jackson.annotation.JsonBackReference;
import jakarta.persistence.*;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Entity
@Data
@NoArgsConstructor
@Table(name = "Wishlist_MC")
public class WishlistEntity {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Integer wishlistID;

    private Integer account;
    @ElementCollection(fetch = FetchType.EAGER)
    @CollectionTable(name = "products_in_wishlist")
    private List<Integer> productsInWishlist;
}
