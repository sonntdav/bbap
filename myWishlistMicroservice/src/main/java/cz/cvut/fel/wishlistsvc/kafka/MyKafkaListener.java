package cz.cvut.fel.wishlistsvc.kafka;

import cz.cvut.fel.wishlistsvc.model.WishlistEntity;
import cz.cvut.fel.wishlistsvc.service.WishlistService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.stereotype.Component;

@Component
public class MyKafkaListener {
    private final WishlistService wishlistService;
    private final MyKafkaProducer kafkaProducer;
    @Autowired
    public MyKafkaListener(WishlistService accountService, MyKafkaProducer kafkaProducer) {
        this.wishlistService = accountService;
        this.kafkaProducer = kafkaProducer;
    }

    @KafkaListener(topics = "accountCreated2", groupId = "wishlist_group_id")
    public void cartWasCreated(Integer  accountID){
        WishlistEntity wishlist = wishlistService.createWishlist(accountID);
        kafkaProducer.wishlistCreated(wishlist.getWishlistID(),accountID);
    }
}
