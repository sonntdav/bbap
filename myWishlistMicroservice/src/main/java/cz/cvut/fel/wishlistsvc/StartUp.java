package cz.cvut.fel.wishlistsvc;

import cz.cvut.fel.wishlistsvc.service.WishlistService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.event.ApplicationReadyEvent;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

@Component
public class StartUp {
    private final WishlistService paymentService;

    @Autowired
    public StartUp(WishlistService reviewService, RestTemplate restTemplate) {
        this.paymentService = reviewService;
    }

    @EventListener(ApplicationReadyEvent.class)
    public void init() {
        paymentService.createWishlist(1);
        paymentService.addItemToWishlist(1,1);
    }

}
