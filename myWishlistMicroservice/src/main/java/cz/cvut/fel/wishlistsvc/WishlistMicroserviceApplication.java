package cz.cvut.fel.wishlistsvc;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class WishlistMicroserviceApplication {
    public static void main(String[] args) {
        SpringApplication.run(WishlistMicroserviceApplication.class, args);
    }


}
