package cz.cvut.fel.wishlistsvc.service.impl;

import cz.cvut.fel.wishlistsvc.model.WishlistEntity;
import cz.cvut.fel.wishlistsvc.repository.WishlistRepository;
import cz.cvut.fel.wishlistsvc.service.WishlistService;
import jakarta.persistence.EntityNotFoundException;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * Implementation of the {@link WishlistService} interface that provides methods
 * to manage wishlists.
 */
@Service
@Slf4j
public class WishlistServiceImpl implements WishlistService {
    private final WishlistRepository wishlistRepository;

    @Autowired
    public WishlistServiceImpl(WishlistRepository wishlistRepository) {
        this.wishlistRepository = wishlistRepository;
    }

    /**
     * Adds an item to the wishlist.
     *
     * @param wishlistId the ID of the wishlist
     * @param itemId     the ID of the item to add
     * @throws EntityNotFoundException if the wishlist or item with the specified IDs does not exist
     */
    @Override
    public void addItemToWishlist(Integer wishlistId, Integer itemId) throws EntityNotFoundException {
        WishlistEntity wishlist = getWishlist(wishlistId);
        wishlist.getProductsInWishlist().add(itemId);
        wishlistRepository.save(wishlist);
    }

    /**
     * Removes an item from the wishlist.
     *
     * @param wishlistId the ID of the wishlist
     * @param itemId     the ID of the item to remove
     * @throws EntityNotFoundException if the wishlist or item with the specified IDs does not exist
     */
    @Override
    public void removeItemFromWishlist(Integer wishlistId, Integer itemId) throws EntityNotFoundException {
        WishlistEntity wishlist = getWishlist(wishlistId);
        wishlist.getProductsInWishlist().remove(itemId);
        wishlistRepository.save(wishlist);
    }

    /**
     * Retrieves the wishlist with the specified ID.
     *
     * @param id the ID of the wishlist to retrieve
     * @return the {@link WishlistEntity} object with the specified ID
     * @throws EntityNotFoundException if the wishlist with the specified ID does not exist
     */
    @Override
    public WishlistEntity getWishlist(Integer id) throws EntityNotFoundException {
        return wishlistRepository.findById(id).orElseThrow(() -> new EntityNotFoundException("Wishlist with this ID not found."));
    }

    /**
     * Retrieves all items from the wishlist with the specified ID.
     *
     * @param id the ID of the wishlist
     * @return a list of integers representing the items in the wishlist
     * @throws EntityNotFoundException if the wishlist with the specified ID does not exist
     */
    @Override
    public List<Integer> getAllItemsFromWishList(Integer id) throws EntityNotFoundException {
        WishlistEntity wishlist = getWishlist(id);
        return wishlist.getProductsInWishlist();
    }

    @Override
    public WishlistEntity createWishlist(Integer accID) {
        WishlistEntity wishlist = new WishlistEntity();
        wishlist.setAccount(accID);
        wishlistRepository.save(wishlist);
        return wishlist;
    }
}
