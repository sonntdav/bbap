package cz.cvut.fel.apigateway.service.impl;

import cz.cvut.fel.apigateway.aggregator.Aggregator;
import cz.cvut.fel.apigateway.dto.AccountDto;
import cz.cvut.fel.apigateway.dto.OrderDto;
import cz.cvut.fel.apigateway.dto.OrderItemDto;
import cz.cvut.fel.apigateway.service.OrderService;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@AllArgsConstructor
@Slf4j
public class OrderServiceImpl implements OrderService {
    private Aggregator aggregator;


    @Override
    public ResponseEntity<OrderDto> getOrder(Integer orderId) {
        return aggregator.getOrder(orderId);
    }

    @Override
    public ResponseEntity<Void> createOrder(OrderDto order) {
        return aggregator.createOrder(order);
    }

    @Override
    public ResponseEntity<List<OrderDto>> getAllOrders() {
        return aggregator.getAllOrders();
    }

    @Override
    public ResponseEntity<AccountDto> getOrderOwner(Integer orderId) {
        return aggregator.getOrderOwner(orderId);
    }

    @Override
    public ResponseEntity<List<OrderItemDto>> getAllItemsInOrder(Integer orderId) {
        return aggregator.getAllItemsInOrder(orderId);
    }

    @Override
    public ResponseEntity<Void> payForOrder(Integer orderID) {
        return aggregator.payForOrder(orderID);
    }
}
