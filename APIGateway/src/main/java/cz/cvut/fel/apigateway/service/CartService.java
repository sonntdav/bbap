package cz.cvut.fel.apigateway.service;

import cz.cvut.fel.apigateway.dto.AccountDto;
import cz.cvut.fel.apigateway.dto.CartDto;
import cz.cvut.fel.apigateway.dto.CartItemDto;
import org.springframework.http.ResponseEntity;

import java.util.List;

public interface CartService {
    ResponseEntity<CartDto> getCart(Integer cartId);

    ResponseEntity<Void> addProductToCart(Integer cartId, Integer productId);

    ResponseEntity<AccountDto> getOwnerOfCart(Integer cartId);

    ResponseEntity<List<CartItemDto>> getAllItemsInCart(Integer cartId);

    ResponseEntity<Void> checkout(Integer cartID);
}

