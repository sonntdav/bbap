package cz.cvut.fel.apigateway.service;

import cz.cvut.fel.apigateway.dto.AccountDto;
import cz.cvut.fel.apigateway.dto.ReviewDto;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;

import java.util.List;

public interface ReviewService {
    ResponseEntity<ReviewDto> getReview(Integer reviewID);

    ResponseEntity<AccountDto> getAuthorOfReview(Integer reviewID);

    ResponseEntity<List<ReviewDto>> getAllReviews(Integer productID);

    ResponseEntity<Void> addReview(Integer productID, Integer userID, String reviewMessage);

    ResponseEntity<Void> updateReview(ReviewDto reviewEntity);

}

