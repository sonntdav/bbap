package cz.cvut.fel.apigateway.controller;

import cz.cvut.fel.apigateway.dto.AccountDto;
import cz.cvut.fel.apigateway.dto.ReviewDto;
import cz.cvut.fel.apigateway.service.ReviewService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api/review")
public class ReviewController {
    private final ReviewService reviewService;

    @Autowired
    public ReviewController(ReviewService reviewService) {
        this.reviewService = reviewService;
    }

    @GetMapping("/{reviewID}")
    public ResponseEntity<ReviewDto> getReview(@PathVariable Integer reviewID) {
        ResponseEntity<ReviewDto> review = reviewService.getReview(reviewID);
        if (review.getBody() != null) {
            return ResponseEntity.ok(review.getBody());
        } else {
            return ResponseEntity.status(HttpStatus.NOT_FOUND).body(null);
        }
    }

    @GetMapping("/{reviewID}/author")
    public ResponseEntity<AccountDto> getAuthorOfReview(@PathVariable Integer reviewID) {
        ResponseEntity<AccountDto> account = reviewService.getAuthorOfReview(reviewID);
        if (account.getBody() != null) {
            return ResponseEntity.ok(account.getBody());
        } else {
            return ResponseEntity.status(HttpStatus.NOT_FOUND).body(null);
        }
    }

    @GetMapping("/product/{productID}")
    public ResponseEntity<List<ReviewDto>> getAllReviews(@PathVariable Integer productID) {
        ResponseEntity<List<ReviewDto>> reviews = reviewService.getAllReviews(productID);
        if (reviews.getBody() != null) {
            return ResponseEntity.ok(reviews.getBody());
        } else {
            return ResponseEntity.status(HttpStatus.NOT_FOUND).body(null);
        }
    }

    @PostMapping("/{productID}/{userID}")
    public ResponseEntity<?> addReview(
            @PathVariable Integer productID,
            @PathVariable Integer userID,
            @RequestBody String reviewMessage
    ) {
        ResponseEntity<Void> response = reviewService.addReview(productID, userID, reviewMessage);
        if (response.getStatusCode() == HttpStatus.CREATED) {
            return ResponseEntity.status(HttpStatus.CREATED).build();
        } else {
            return ResponseEntity.status(HttpStatus.NOT_FOUND).body("Review not added.");
        }
    }

    @PutMapping
    public ResponseEntity<?> updateReview(@RequestBody ReviewDto reviewEntity) {
        ResponseEntity<Void> response = reviewService.updateReview(reviewEntity);
        if (response.getStatusCode() == HttpStatus.OK) {
            return ResponseEntity.ok().build();
        } else {
            return ResponseEntity.status(HttpStatus.NOT_FOUND).body("Review not found.");
        }
    }
}

