package cz.cvut.fel.apigateway.controller;

import cz.cvut.fel.apigateway.dto.AccountDto;
import cz.cvut.fel.apigateway.service.AccountService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api/account")
public class AccountController {

    private final AccountService accountService;

    @Autowired
    public AccountController(AccountService accountService) {
        this.accountService = accountService;
    }

    @GetMapping("/byName/{accountName}")
    public ResponseEntity<AccountDto> getAccountByName(@PathVariable String accountName) {
        return accountService.getAccountByName(accountName);
    }
    @GetMapping()
    public ResponseEntity<List<AccountDto>> getAllAccounts(){
        return accountService.getAllAccounts();
    }

    @GetMapping("/{accountId}")
    public ResponseEntity<AccountDto> getAccount(@PathVariable Integer accountId) {
        return accountService.getAccount(accountId);
    }

    @PostMapping
    public ResponseEntity<?> createAccount(@RequestBody AccountDto account) {
        return accountService.createAccount(account);
    }

    @PutMapping("/{accountId}")
    public ResponseEntity<Void> updateAccount(@PathVariable Integer accountId, @RequestBody AccountDto account) {
        return accountService.updateAccount(accountId, account);
    }

    @GetMapping("/{accountId}/balance")
    public ResponseEntity<Double> getAccountBalance(@PathVariable Integer accountId) {
        return accountService.getAccountBalance(accountId);
    }

    @PostMapping("/{accountId}/addFunds/{amount}")
    public ResponseEntity<Void> addFunds(
            @PathVariable Integer accountId,
            @PathVariable Double amount
    ) {
        return accountService.addFunds(accountId, amount);
    }

}

