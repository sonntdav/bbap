package cz.cvut.fel.apigateway.service;

import cz.cvut.fel.apigateway.dto.AccountDto;
import cz.cvut.fel.apigateway.dto.OrderDto;
import cz.cvut.fel.apigateway.dto.PaymentDto;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;

public interface PaymentService {
    ResponseEntity<PaymentDto> getPayment(Integer paymentId);

    ResponseEntity<AccountDto> getAccountOfPayment(Integer paymentId);

    ResponseEntity<Integer> getOrderOfPayment(Integer paymentId);
}

