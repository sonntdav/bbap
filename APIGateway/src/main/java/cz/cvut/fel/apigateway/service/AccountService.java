package cz.cvut.fel.apigateway.service;

import cz.cvut.fel.apigateway.dto.AccountDto;
import org.springframework.http.ResponseEntity;

import java.util.List;

public interface AccountService {
    ResponseEntity<AccountDto> getAccountByName(String accountName);

    ResponseEntity<AccountDto> getAccount(Integer accountId);

    ResponseEntity<Void> createAccount(AccountDto account);
    ResponseEntity<Void> updateAccount(Integer accountId, AccountDto account);

    ResponseEntity<Double> getAccountBalance(Integer accountId);

    ResponseEntity<Void> addFunds(Integer accountId, Double amount);

    ResponseEntity<List<AccountDto>> getAllAccounts();
}
