package cz.cvut.fel.apigateway.service;

import cz.cvut.fel.apigateway.dto.ProductDto;
import cz.cvut.fel.apigateway.dto.WishlistDto;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;

import java.util.List;

public interface WishlistService {
    ResponseEntity<Void> addItemToWishlist(Integer wishlistId, Integer itemId);

    ResponseEntity<Void> removeItemFromWishlist(Integer wishlistId, Integer itemId);

    ResponseEntity<List<ProductDto>> getAllItemsFromWishList(Integer wishlistId);

    ResponseEntity<WishlistDto> getWishlist(Integer wishlistId);

}

