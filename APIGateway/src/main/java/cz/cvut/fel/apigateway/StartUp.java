//package cz.cvut.fel.apigateway;
//
//import cz.cvut.fel.apigateway.dto.AccountDto;
//import cz.cvut.fel.apigateway.dto.ProductDto;
//import cz.cvut.fel.apigateway.dto.ReviewDto;
//import cz.cvut.fel.apigateway.service.*;
//import jakarta.annotation.PostConstruct;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.stereotype.Component;
//
//@Component
//public class StartUp {
//    private final AccountService accountService;
//    private final CartService cartService;
//    private final OrderService orderService;
//    private final ProductService productService;
//    private final ReviewService reviewService;
//    private final WishlistService wishlistService;
//
//
//    @Autowired
//    public StartUp(AccountService accountService, ProductService productService, CartService cartService, OrderService orderService, ReviewService reviewService, WishlistService wishlistService) {
//        this.accountService = accountService;
//        this.productService = productService;
//        this.cartService = cartService;
//        this.orderService = orderService;
//        this.reviewService = reviewService;
//        this.wishlistService = wishlistService;
//    }
//
//    @PostConstruct
//    public void init() {
//        AccountDto accountDto = new AccountDto("spring_ name","Spring_mail","spring_city","spring_street",10000.00);
//        accountService.createAccount(accountDto);
//
//        ProductDto productDto = new ProductDto("spring_product","productDescription",99.99);
//        productService.createProduct(productDto);
//
//        cartService.addProductToCart(1,1);
//        cartService.checkout(1);
//
//        cartService.addProductToCart(1,1);
//
//        reviewService.addReview(1,1,"spring_review");
//
//        wishlistService.addItemToWishlist(1,1);
//
//        try {
//            Thread.sleep(10);
//        } catch (InterruptedException e) {
//            e.printStackTrace();
//        }
//        orderService.payForOrder(1);
//    }
//
//}
