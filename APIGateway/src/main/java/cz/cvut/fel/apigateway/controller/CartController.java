package cz.cvut.fel.apigateway.controller;

import cz.cvut.fel.apigateway.dto.AccountDto;
import cz.cvut.fel.apigateway.dto.CartDto;
import cz.cvut.fel.apigateway.dto.CartItemDto;
import cz.cvut.fel.apigateway.service.CartService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api/cart")
public class CartController {
    private final CartService cartService;

    @Autowired
    public CartController(CartService cartService) {
        this.cartService = cartService;
    }

    @GetMapping("/{cartId}")
    public ResponseEntity<CartDto> getCart(@PathVariable Integer cartId) {
        ResponseEntity<CartDto> response = cartService.getCart(cartId);
        return ResponseEntity.status(response.getStatusCode()).body(response.getBody());
    }

    @PostMapping("/{cartId}/products/{productId}")
    public ResponseEntity<?> addProductToCart(
            @PathVariable Integer cartId,
            @PathVariable Integer productId
    ) {
        ResponseEntity<Void> response = cartService.addProductToCart(cartId, productId);
        return ResponseEntity.status(response.getStatusCode()).build();
    }

    @GetMapping("/{cartId}/owner")
    public ResponseEntity<AccountDto> getOwnerOfCart(@PathVariable Integer cartId) {
        ResponseEntity<AccountDto> response = cartService.getOwnerOfCart(cartId);
        return ResponseEntity.status(response.getStatusCode()).body(response.getBody());
    }

    @GetMapping("/{cartId}/items")
    public ResponseEntity<List<CartItemDto>> getAllItemsInCart(@PathVariable Integer cartId) {
        ResponseEntity<List<CartItemDto>> response = cartService.getAllItemsInCart(cartId);
        return ResponseEntity.status(response.getStatusCode()).body(response.getBody());
    }

    @PostMapping("/{cartId}/checkout")
    public ResponseEntity<Void> checkout(@PathVariable Integer cartId){
        return cartService.checkout(cartId);
    }
}

