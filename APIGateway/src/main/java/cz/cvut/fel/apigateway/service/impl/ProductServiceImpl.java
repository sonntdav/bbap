package cz.cvut.fel.apigateway.service.impl;

import cz.cvut.fel.apigateway.aggregator.Aggregator;
import cz.cvut.fel.apigateway.dto.ProductDto;
import cz.cvut.fel.apigateway.service.ProductService;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@AllArgsConstructor
@Slf4j
public class ProductServiceImpl implements ProductService {
    private Aggregator aggregator;


    @Override
    public ResponseEntity<ProductDto> getProduct(Integer productID) {
        return aggregator.getProduct(productID);
    }

    @Override
    public ResponseEntity<ProductDto> getProduct(String productName) {
        return aggregator.getProduct(productName);
    }

    @Override
    public ResponseEntity<Void> createProduct(ProductDto product) {
        return aggregator.createProduct(product);
    }

    @Override
    public ResponseEntity<Void> updateProduct(Integer productID, ProductDto product) {
        return aggregator.updateProduct(productID,product);
    }

    @Override
    public ResponseEntity<List<ProductDto>> getListOfAllProducts() {
        return aggregator.getListOfAllProducts();
    }
}
