package cz.cvut.fel.apigateway.controller;

import cz.cvut.fel.apigateway.dto.ProductDto;
import cz.cvut.fel.apigateway.dto.WishlistDto;
import cz.cvut.fel.apigateway.service.WishlistService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api/wishlist")
public class WishlistController {
    private final WishlistService wishlistService;

    @Autowired
    public WishlistController(WishlistService wishlistService) {
        this.wishlistService = wishlistService;
    }

    @PostMapping("/{wishlistId}/items/{itemId}")
    public ResponseEntity<?> addItemToWishlist(
            @PathVariable Integer wishlistId,
            @PathVariable Integer itemId
    ) {
        ResponseEntity<Void> response = wishlistService.addItemToWishlist(wishlistId, itemId);
        if (response.getStatusCode() == HttpStatus.OK) {
            return ResponseEntity.ok().build();
        } else {
            return ResponseEntity.status(HttpStatus.NOT_FOUND).body("Item not added to the wishlist.");
        }
    }

    @DeleteMapping("/{wishlistId}/items/{itemId}")
    public ResponseEntity<?> removeItemFromWishlist(
            @PathVariable Integer wishlistId,
            @PathVariable Integer itemId
    ) {
        ResponseEntity<Void> response = wishlistService.removeItemFromWishlist(wishlistId, itemId);
        if (response.getStatusCode() == HttpStatus.OK) {
            return ResponseEntity.ok().build();
        } else {
            return ResponseEntity.status(HttpStatus.NOT_FOUND).body("Item not removed from the wishlist.");
        }
    }

    @GetMapping("/{wishlistId}/all")
    public ResponseEntity<?> getAllItemsFromWishlist(@PathVariable Integer wishlistId) {
        ResponseEntity<List<ProductDto>> items = wishlistService.getAllItemsFromWishList(wishlistId);
        if (items.getBody() != null) {
            return ResponseEntity.ok(items.getBody());
        } else {
            return ResponseEntity.status(HttpStatus.NOT_FOUND).body("No items found in the wishlist.");
        }
    }

    @GetMapping("/{wishlistId}")
    public ResponseEntity<?> getWishlist(@PathVariable Integer wishlistId) {
        ResponseEntity<WishlistDto> wishlist = wishlistService.getWishlist(wishlistId);
        if (wishlist.getBody() != null) {
            return ResponseEntity.ok(wishlist.getBody());
        } else {
            return ResponseEntity.status(HttpStatus.NOT_FOUND).body("Wishlist not found.");
        }
    }
}

