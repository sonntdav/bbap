package cz.cvut.fel.apigateway.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class OrderItemDto {
    private Integer orderItemID;
    private Integer amount;
    private Integer productEntity;
    private Integer order;
}

