package cz.cvut.fel.apigateway.service.impl;

import cz.cvut.fel.apigateway.aggregator.Aggregator;
import cz.cvut.fel.apigateway.dto.AccountDto;
import cz.cvut.fel.apigateway.dto.OrderDto;
import cz.cvut.fel.apigateway.dto.PaymentDto;
import cz.cvut.fel.apigateway.service.PaymentService;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

@Service
@AllArgsConstructor
@Slf4j
public class PaymentServiceImpl implements PaymentService {
    private Aggregator aggregator;


    @Override
    public ResponseEntity<PaymentDto> getPayment(Integer paymentId) {
        return aggregator.getPayment(paymentId);
    }

    @Override
    public ResponseEntity<AccountDto> getAccountOfPayment(Integer paymentId) {
        return aggregator.getAccountOfPayment(paymentId);
    }

    @Override
    public ResponseEntity<Integer> getOrderOfPayment(Integer paymentId) {
        return aggregator.getOrderOfPayment(paymentId);
    }
}
