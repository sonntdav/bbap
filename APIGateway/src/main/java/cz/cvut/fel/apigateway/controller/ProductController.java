package cz.cvut.fel.apigateway.controller;

import cz.cvut.fel.apigateway.dto.ProductDto;
import cz.cvut.fel.apigateway.service.ProductService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api/product")
public class ProductController {
    private final ProductService productService;

    @Autowired
    public ProductController(ProductService productService) {
        this.productService = productService;
    }

    @GetMapping("/{productID}")
    public ResponseEntity<ProductDto> getProduct(@PathVariable Integer productID) {
        ResponseEntity<ProductDto> product = productService.getProduct(productID);
        if (product.getBody() != null) {
            return ResponseEntity.ok(product.getBody());
        } else {
            return ResponseEntity.status(HttpStatus.NOT_FOUND).body(null);
        }
    }

    @GetMapping("/byName/{productName}")
    public ResponseEntity<ProductDto> getProductByName(@PathVariable String productName) {
        ResponseEntity<ProductDto> product = productService.getProduct(productName);
        if (product.getBody() != null) {
            return ResponseEntity.ok(product.getBody());
        } else {
            return ResponseEntity.status(HttpStatus.NOT_FOUND).body(null);
        }
    }

    @PostMapping
    public ResponseEntity<?> createProduct(@RequestBody ProductDto product) {
        ResponseEntity<Void> response = productService.createProduct(product);
        if (response.getStatusCode() == HttpStatus.CREATED) {
            return ResponseEntity.status(HttpStatus.CREATED).build();
        } else {
            return ResponseEntity.status(HttpStatus.CONFLICT).body("Product already exists.");
        }
    }

    @PutMapping("/{productID}")
    public ResponseEntity<?> updateProduct(@PathVariable Integer productID, @RequestBody ProductDto product) {
        ResponseEntity<Void> response = productService.updateProduct(productID, product);
        if (response.getStatusCode() == HttpStatus.OK) {
            return ResponseEntity.ok().build();
        } else {
            return ResponseEntity.status(HttpStatus.NOT_FOUND).body("Product not found.");
        }
    }

    @GetMapping
    public ResponseEntity<List<ProductDto>> getListOfAllProducts() {
        ResponseEntity<List<ProductDto>> products = productService.getListOfAllProducts();
        if (products.getBody() != null) {
            return ResponseEntity.ok(products.getBody());
        } else {
            return ResponseEntity.status(HttpStatus.NOT_FOUND).body(null);
        }
    }
}

