package cz.cvut.fel.apigateway.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class ReviewDto {
    private Integer reviewID;
    private String message;

    private Integer author;
    private Integer product;

    public ReviewDto(String message) {
        this.message = message;
    }
}

