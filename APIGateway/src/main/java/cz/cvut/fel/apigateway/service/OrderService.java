package cz.cvut.fel.apigateway.service;

import cz.cvut.fel.apigateway.dto.AccountDto;
import cz.cvut.fel.apigateway.dto.OrderDto;
import cz.cvut.fel.apigateway.dto.OrderItemDto;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;

import java.util.List;

public interface OrderService {
    ResponseEntity<OrderDto> getOrder(Integer orderId);

    ResponseEntity<Void> createOrder(OrderDto order);

    ResponseEntity<List<OrderDto>> getAllOrders();

    ResponseEntity<AccountDto> getOrderOwner(Integer orderId);

    ResponseEntity<List<OrderItemDto>> getAllItemsInOrder(Integer orderId);

    ResponseEntity<Void> payForOrder(Integer orderID);
}

