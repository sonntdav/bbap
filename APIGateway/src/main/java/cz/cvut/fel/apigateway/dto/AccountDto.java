package cz.cvut.fel.apigateway.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class AccountDto {
    private Integer id;
    private String username;
    private String email;
    private String cityName;
    private String street;
    private Double balance;

    private Integer cartEntity;
    private List<Integer> orders;
    private Integer wishlist;

    public AccountDto(String username, String email, String cityName, String street, Double balance) {
        this.username = username;
        this.email = email;
        this.cityName = cityName;
        this.street = street;
        this.balance = balance;
    }
}

