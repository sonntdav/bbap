package cz.cvut.fel.apigateway.aggregator;

import cz.cvut.fel.apigateway.dto.*;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

import java.util.Arrays;
import java.util.List;

@Component
@AllArgsConstructor
@Slf4j
public class Aggregator {
    private RestTemplate restTemplate;
//    private final String baseAccountUrl = "http://account-svc-service:8081/account";
//    private final String baseCartUrl = "http://cart-svc-service:8083/cart";
//    private final String baseOrderUrl = "http://order-service:8084/order";
//    private final String basePaymentUrl = "http://payment-service:8085/payment";
//    private final String baseProductUrl = "http://product-service-service:8082/product";
//    private final String baseReviewUrl = "http://review-service-service:8086/review";
//    private final String baseWishlistUrl = "http://wishlist-service-service:8087/wishlist";

    private final String baseAccountUrl = "http://localhost:8081/account";
    private final String baseCartUrl = "http://localhost:8083/cart";
    private final String baseOrderUrl = "http://localhost:8084/order";
    private final String basePaymentUrl = "http://localhost:8085/payment";
    private final String baseProductUrl = "http://localhost:8082/product";
    private final String baseReviewUrl = "http://localhost:8086/review";
    private final String baseWishlistUrl = "http://localhost:8087/wishlist";

    // --------------------------------------- Account service -------------------------------------------------------------------------

    public ResponseEntity<AccountDto> getAccountByName(String accountName) {
        String url = baseAccountUrl + "/byName/" + accountName;
        return restTemplate.getForEntity(url, AccountDto.class);
    }

    public ResponseEntity<AccountDto> getAccount(Integer accountId) {
        String url = baseAccountUrl + "/" + accountId;
        return restTemplate.getForEntity(url, AccountDto.class);
    }

    public ResponseEntity<List<AccountDto>> getAllAccounts() {
        ParameterizedTypeReference<List<AccountDto>> responseType = new ParameterizedTypeReference<>() {};
        return restTemplate.exchange(baseAccountUrl, HttpMethod.GET, null, responseType);
    }

    public ResponseEntity<Void> createAccount(AccountDto account) {
        return restTemplate.postForEntity(baseAccountUrl, account, Void.class);
    }

    public ResponseEntity<Void> updateAccount(Integer accountId, AccountDto account) {
        String url = baseAccountUrl + "/" + accountId;
        restTemplate.put(url, account);
        return ResponseEntity.ok().build();
    }

    public ResponseEntity<Double> getAccountBalance(Integer accountId) {
        String url = baseAccountUrl + "/" + accountId + "/balance";
        return restTemplate.getForEntity(url, Double.class);
    }

    public ResponseEntity<Void> addFunds(Integer accountId, Double amount) {
        String url = baseAccountUrl + "/" + accountId + "/addFunds/" + amount;
        restTemplate.postForEntity(url, null, Void.class);
        return ResponseEntity.ok().build();
    }

    // --------------------------------------- Cart service -------------------------------------------------------------------------

    public ResponseEntity<CartDto> getCart(Integer cartId) {
        String url = baseCartUrl + "/" + cartId;
        return restTemplate.getForEntity(url, CartDto.class);
    }

    public ResponseEntity<Void> addProductToCart(Integer cartId, Integer productId) {
        String url = baseCartUrl + "/" + cartId + "/products/" + productId;
        return restTemplate.postForEntity(url, null, Void.class);
    }

    public ResponseEntity<AccountDto> getOwnerOfCart(Integer cartId) {
        String url = baseCartUrl + "/" + cartId + "/owner";
        return restTemplate.getForEntity(url, AccountDto.class);
    }

    public ResponseEntity<List<CartItemDto>> getAllItemsInCart(Integer cartId) {
        String url = baseCartUrl + "/" + cartId + "/items";
        ParameterizedTypeReference<List<CartItemDto>> responseType = new ParameterizedTypeReference<>() {};
        return restTemplate.exchange(url, HttpMethod.GET, null, responseType);
    }

    public ResponseEntity<Void> checkout(Integer cartID){
        String url = baseCartUrl + "/" + cartID + "/checkout";
        return restTemplate.postForEntity(url,null,Void.class);
    }
    // --------------------------------------- Order service -------------------------------------------------------------------------

    public ResponseEntity<Void> payForOrder(Integer orderID){
        String url = baseOrderUrl + "/" + orderID + "/pay";
        return restTemplate.postForEntity(url,null,Void.class);
    }
    public ResponseEntity<OrderDto> getOrder(Integer orderId) {
        String url = baseOrderUrl + "/" + orderId;
        return restTemplate.exchange(url, HttpMethod.GET, null, OrderDto.class);
    }

    public ResponseEntity<Void> createOrder(OrderDto order) {
        return restTemplate.postForEntity(baseOrderUrl, order, Void.class);
    }

    public ResponseEntity<List<OrderDto>> getAllOrders() {
        ParameterizedTypeReference<List<OrderDto>> responseType = new ParameterizedTypeReference<>() {};
        return restTemplate.exchange(baseOrderUrl, HttpMethod.GET, null, responseType);
    }

    public ResponseEntity<AccountDto> getOrderOwner(Integer orderId) {
        String url = baseOrderUrl + "/" + orderId + "/owner";
        return restTemplate.exchange(url, HttpMethod.GET, null, AccountDto.class);
    }

    public ResponseEntity<List<OrderItemDto>> getAllItemsInOrder(Integer orderId) {
        String url = baseOrderUrl + "/" + orderId + "/items";
        ParameterizedTypeReference<List<OrderItemDto>> responseType = new ParameterizedTypeReference<>() {};
        return restTemplate.exchange(url, HttpMethod.GET, null, responseType);
    }

    // --------------------------------------- Payment service -------------------------------------------------------------------------

    public ResponseEntity<PaymentDto> getPayment(Integer paymentId) {
        String url = basePaymentUrl + "/" + paymentId;
        return restTemplate.exchange(url, HttpMethod.GET, null, PaymentDto.class);
    }

    public ResponseEntity<AccountDto> getAccountOfPayment(Integer paymentId) {
        String url = basePaymentUrl + "/" + paymentId + "/account";
        return restTemplate.exchange(url, HttpMethod.GET, null, AccountDto.class);
    }

    public ResponseEntity<Integer> getOrderOfPayment(Integer paymentId) {
        String url = basePaymentUrl + "/" + paymentId + "/order";
        return restTemplate.exchange(url, HttpMethod.GET, null, Integer.class);
    }

    // --------------------------------------- Product service -------------------------------------------------------------------------

    public ResponseEntity<ProductDto> getProduct(Integer productID) {
        String url = baseProductUrl + "/" + productID;
        return restTemplate.getForEntity(url, ProductDto.class);
    }

    public ResponseEntity<ProductDto> getProduct(String productName) {
        String url = baseProductUrl + "/byName/" + productName;
        return restTemplate.getForEntity(url, ProductDto.class);
    }

    public ResponseEntity<Void> createProduct(ProductDto product) {
        return restTemplate.postForEntity(baseProductUrl, product, Void.class);
    }

    public ResponseEntity<Void> updateProduct(Integer productID, ProductDto product) {
        String url = baseProductUrl + "/" + productID;
        restTemplate.put(url, product);
        return ResponseEntity.ok().build();
    }

    public ResponseEntity<List<ProductDto>> getListOfAllProducts() {
        ParameterizedTypeReference<List<ProductDto>> responseType = new ParameterizedTypeReference<>() {};
        return restTemplate.exchange(baseProductUrl, HttpMethod.GET, null, responseType);
    }

    // --------------------------------------- Review service -------------------------------------------------------------------------

    public ResponseEntity<ReviewDto> getReview(Integer reviewID) {
        String url = baseReviewUrl + "/" + reviewID;
        return restTemplate.getForEntity(url, ReviewDto.class);
    }

    public ResponseEntity<AccountDto> getAuthorOfReview(Integer reviewID) {
        String url = baseReviewUrl + "/" + reviewID + "/author";
        return restTemplate.getForEntity(url, AccountDto.class);
    }

    public ResponseEntity<List<ReviewDto>> getAllReviews(Integer productID) {
        String url = baseReviewUrl + "/product/" + productID;
        ParameterizedTypeReference<List<ReviewDto>> responseType = new ParameterizedTypeReference<>() {};
        return restTemplate.exchange(url, HttpMethod.GET, null, responseType);
    }

    public ResponseEntity<Void> addReview(Integer productID, Integer userID, String reviewMessage) {
        String url = baseReviewUrl + "/" + productID + "/" + userID;
        return restTemplate.postForEntity(url, reviewMessage, Void.class);
    }

    public ResponseEntity<Void> updateReview(ReviewDto reviewEntity) {
        restTemplate.put(baseReviewUrl, reviewEntity);
        return ResponseEntity.ok().build();
    }
    // --------------------------------------- Wishlist service -------------------------------------------------------------------------

    public ResponseEntity<Void> addItemToWishlist(Integer wishlistId, Integer itemId) {
        String url = baseWishlistUrl + "/" + wishlistId + "/items/" + itemId;
        return restTemplate.postForEntity(url, null, Void.class);
    }

    public ResponseEntity<Void> removeItemFromWishlist(Integer wishlistId, Integer itemId) {
        String url = baseWishlistUrl + "/" + wishlistId + "/items/" + itemId;
        restTemplate.delete(url);
        return ResponseEntity.ok().build();
    }

    public ResponseEntity<List<ProductDto>> getAllItemsFromWishList(Integer wishlistId) {
        String url = baseWishlistUrl + "/" + wishlistId + "/all";
        ParameterizedTypeReference<List<ProductDto>> responseType = new ParameterizedTypeReference<>() {};
        return restTemplate.exchange(url, HttpMethod.GET, null, responseType);
    }

    public ResponseEntity<WishlistDto> getWishlist(Integer wishlistId) {
        String url = baseWishlistUrl + "/" + wishlistId;
        return restTemplate.getForEntity(url, WishlistDto.class);
    }
}
