package cz.cvut.fel.apigateway.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;
import java.util.List;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class OrderDto {
    private Integer orderID;
    private Date orderDate;
    private OrderStatus orderStatus;

    private Integer account;
    private List<OrderItemDto> orderItems;
    private Integer payment;

}

