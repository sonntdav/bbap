package cz.cvut.fel.apigateway.dto;

public enum OrderStatus {
    PENDING,
    IN_PROGRESS,
    COMPLETED
}
