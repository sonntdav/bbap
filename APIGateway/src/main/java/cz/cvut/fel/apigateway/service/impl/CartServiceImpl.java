package cz.cvut.fel.apigateway.service.impl;

import cz.cvut.fel.apigateway.aggregator.Aggregator;
import cz.cvut.fel.apigateway.dto.AccountDto;
import cz.cvut.fel.apigateway.dto.CartDto;
import cz.cvut.fel.apigateway.dto.CartItemDto;
import cz.cvut.fel.apigateway.service.CartService;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@AllArgsConstructor
@Slf4j
public class CartServiceImpl implements CartService {
    private Aggregator aggregator;


    @Override
    public ResponseEntity<CartDto> getCart(Integer cartId) {
        return aggregator.getCart(cartId);
    }

    @Override
    public ResponseEntity<Void> addProductToCart(Integer cartId, Integer productId) {
        return aggregator.addProductToCart(cartId,productId);
    }

    @Override
    public ResponseEntity<AccountDto> getOwnerOfCart(Integer cartId) {
        return aggregator.getOwnerOfCart(cartId);
    }

    @Override
    public ResponseEntity<List<CartItemDto>> getAllItemsInCart(Integer cartId) {
        return aggregator.getAllItemsInCart(cartId);
    }

    @Override
    public ResponseEntity<Void> checkout(Integer cartID) {
        return aggregator.checkout(cartID);
    }
}
