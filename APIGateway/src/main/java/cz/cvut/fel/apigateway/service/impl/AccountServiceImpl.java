package cz.cvut.fel.apigateway.service.impl;

import cz.cvut.fel.apigateway.aggregator.Aggregator;
import cz.cvut.fel.apigateway.dto.AccountDto;
import cz.cvut.fel.apigateway.service.AccountService;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@AllArgsConstructor
@Slf4j
public class AccountServiceImpl implements AccountService {
    private Aggregator aggregator;

    @Override
    public ResponseEntity<AccountDto> getAccountByName(String accountName) {
        return aggregator.getAccountByName(accountName);
    }
    @Override
    public ResponseEntity<AccountDto> getAccount(Integer accountId) {
        return aggregator.getAccount(accountId);
    }
    @Override
    public ResponseEntity<Void> createAccount(AccountDto account) {
        return aggregator.createAccount(account);
    }
    @Override
    public ResponseEntity<Void> updateAccount(Integer accountId, AccountDto account) {
        return aggregator.updateAccount(accountId,account);
    }
    @Override
    public ResponseEntity<Double> getAccountBalance(Integer accountId) {
        return aggregator.getAccountBalance(accountId);
    }
    @Override
    public ResponseEntity<Void> addFunds(Integer accountId, Double amount) {
        return aggregator.addFunds(accountId,amount);
    }

    @Override
    public ResponseEntity<List<AccountDto>> getAllAccounts() {
        return aggregator.getAllAccounts();
    }

}
