package cz.cvut.fel.apigateway.service.impl;

import cz.cvut.fel.apigateway.aggregator.Aggregator;
import cz.cvut.fel.apigateway.dto.AccountDto;
import cz.cvut.fel.apigateway.dto.ReviewDto;
import cz.cvut.fel.apigateway.service.ReviewService;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@AllArgsConstructor
@Slf4j
public class ReviewServiceImpl implements ReviewService {
    private Aggregator aggregator;


    @Override
    public ResponseEntity<ReviewDto> getReview(Integer reviewID) {
        return aggregator.getReview(reviewID);
    }

    @Override
    public ResponseEntity<AccountDto> getAuthorOfReview(Integer reviewID) {
        return aggregator.getAuthorOfReview(reviewID);
    }

    @Override
    public ResponseEntity<List<ReviewDto>> getAllReviews(Integer productID) {
        return aggregator.getAllReviews(productID);
    }

    @Override
    public ResponseEntity<Void> addReview(Integer productID, Integer userID, String reviewMessage) {
        return aggregator.addReview(productID,userID,reviewMessage);
    }

    @Override
    public ResponseEntity<Void> updateReview(ReviewDto reviewEntity) {
        return aggregator.updateReview(reviewEntity);
    }
}
