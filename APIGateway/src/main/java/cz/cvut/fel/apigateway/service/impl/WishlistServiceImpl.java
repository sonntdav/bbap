package cz.cvut.fel.apigateway.service.impl;

import cz.cvut.fel.apigateway.aggregator.Aggregator;
import cz.cvut.fel.apigateway.dto.ProductDto;
import cz.cvut.fel.apigateway.dto.WishlistDto;
import cz.cvut.fel.apigateway.service.WishlistService;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@AllArgsConstructor
@Slf4j
public class WishlistServiceImpl implements WishlistService {
    private Aggregator aggregator;


    @Override
    public ResponseEntity<Void> addItemToWishlist(Integer wishlistId, Integer itemId) {
        return aggregator.addItemToWishlist(wishlistId,itemId);
    }

    @Override
    public ResponseEntity<Void> removeItemFromWishlist(Integer wishlistId, Integer itemId) {
        return aggregator.removeItemFromWishlist(wishlistId,itemId);
    }

    @Override
    public ResponseEntity<List<ProductDto>> getAllItemsFromWishList(Integer wishlistId) {
        return aggregator.getAllItemsFromWishList(wishlistId);
    }

    @Override
    public ResponseEntity<WishlistDto> getWishlist(Integer wishlistId) {
        return aggregator.getWishlist(wishlistId);
    }
}
