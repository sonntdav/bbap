package cz.cvut.fel.apigateway.controller;

import cz.cvut.fel.apigateway.dto.AccountDto;
import cz.cvut.fel.apigateway.dto.OrderDto;
import cz.cvut.fel.apigateway.dto.OrderItemDto;
import cz.cvut.fel.apigateway.service.OrderService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api/order")
public class OrderController {
    private final OrderService orderService;

    @Autowired
    public OrderController(OrderService orderService) {
        this.orderService = orderService;
    }

    @GetMapping("/{orderId}")
    public ResponseEntity<OrderDto> getOrder(@PathVariable Integer orderId) {
        return orderService.getOrder(orderId);
    }

    @PostMapping
    public ResponseEntity<?> createOrder(@RequestBody OrderDto order) {
        return orderService.createOrder(order);
    }

    @GetMapping
    public ResponseEntity<List<OrderDto>> getAllOrders() {
        return orderService.getAllOrders();
    }

    @GetMapping("/{orderId}/owner")
    public ResponseEntity<AccountDto> getOrderOwner(@PathVariable Integer orderId) {
        return orderService.getOrderOwner(orderId);
    }

    @GetMapping("/{orderId}/items")
    public ResponseEntity<List<OrderItemDto>> getAllItemsInOrder(@PathVariable Integer orderId) {
        return orderService.getAllItemsInOrder(orderId);
    }

    @PostMapping("/{orderId}/pay")
    public ResponseEntity<Void> payForOrder(@PathVariable Integer orderId) {
            orderService.payForOrder(orderId);
        return ResponseEntity.ok().build();
    }
}

