package cz.cvut.fel.apigateway.service;

import cz.cvut.fel.apigateway.dto.ProductDto;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;

import java.util.List;

public interface ProductService {
    ResponseEntity<ProductDto> getProduct(Integer productID);

    ResponseEntity<ProductDto> getProduct(String productName);

    ResponseEntity<Void> createProduct(ProductDto product);

    ResponseEntity<Void> updateProduct(Integer productID, ProductDto product);

    ResponseEntity<List<ProductDto>> getListOfAllProducts();
}
