package cz.cvut.fel.apigateway.controller;

import cz.cvut.fel.apigateway.dto.AccountDto;
import cz.cvut.fel.apigateway.dto.OrderDto;
import cz.cvut.fel.apigateway.dto.PaymentDto;
import cz.cvut.fel.apigateway.service.PaymentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api/payment")
public class PaymentController {
    private final PaymentService paymentService;

    @Autowired
    public PaymentController(PaymentService paymentService) {
        this.paymentService = paymentService;
    }

    @GetMapping("/{paymentID}")
    public ResponseEntity<PaymentDto> getPayment(@PathVariable Integer paymentID) {
        ResponseEntity<PaymentDto> payment = paymentService.getPayment(paymentID);
        if (payment.getBody() != null) {
            return ResponseEntity.ok(payment.getBody());
        } else {
            return ResponseEntity.status(HttpStatus.NOT_FOUND).body(null);
        }
    }

    @GetMapping("/{paymentID}/account")
    public ResponseEntity<AccountDto> getAccount(@PathVariable Integer paymentID) {
        ResponseEntity<AccountDto> account = paymentService.getAccountOfPayment(paymentID);
        if (account.getBody() != null) {
            return ResponseEntity.ok(account.getBody());
        } else {
            return ResponseEntity.status(HttpStatus.NOT_FOUND).body(null);
        }
    }

    @GetMapping("/{paymentID}/order")
    public ResponseEntity<Integer> getOrder(@PathVariable Integer paymentID) {
        ResponseEntity<Integer> order = paymentService.getOrderOfPayment(paymentID);
        if (order.getBody() != null) {
            return ResponseEntity.ok(order.getBody());
        } else {
            return ResponseEntity.status(HttpStatus.NOT_FOUND).body(null);
        }
    }
}

