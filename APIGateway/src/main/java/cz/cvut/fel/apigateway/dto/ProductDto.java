package cz.cvut.fel.apigateway.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class ProductDto {
    private Integer productID;
    private List<Integer> reviewsIds;
    private String name;
    private String description;
    private Double price;

    public ProductDto(String name, String description, Double price) {
        this.name = name;
        this.description = description;
        this.price = price;
    }
}

