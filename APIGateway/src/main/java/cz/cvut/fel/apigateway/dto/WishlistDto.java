package cz.cvut.fel.apigateway.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class WishlistDto {
    private Integer wishlistID;
    private Integer accountID;
    private List<Integer> productsInWishlistIds;
}

