package cz.cvut.fel.productsvc.kafka;

import com.fasterxml.jackson.core.JsonProcessingException;
import cz.cvut.fel.productsvc.kafka.transfer.ReviewCreatedResponse;
import cz.cvut.fel.productsvc.model.ProductEntity;
import cz.cvut.fel.productsvc.service.ProductService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.stereotype.Component;

@Component
public class MyKafkaListener {
    private final ProductService productService;
    private final JsonSerializer jsonSerializer = new JsonSerializer();
    @Autowired
    public MyKafkaListener(ProductService productService) {
        this.productService = productService;
    }

    @KafkaListener(topics = "reviewCreated", groupId = "account_group_id")
    public void cartWasCreated(String  jsonCartCreatedResponse){
        try {
            ReviewCreatedResponse  reviewCreatedResponse = jsonSerializer.departmentFromJson(jsonCartCreatedResponse);

            Integer productID = reviewCreatedResponse.getProductID();
            Integer reviewID = reviewCreatedResponse.getReviewID();

            ProductEntity product = productService.getProduct(productID);
            product.addReview(reviewID);
            productService.updateProduct(product);
        } catch (JsonProcessingException e) {
            e.printStackTrace();
        }
    }
}
