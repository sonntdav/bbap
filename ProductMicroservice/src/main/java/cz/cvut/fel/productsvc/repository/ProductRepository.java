package cz.cvut.fel.productsvc.repository;

import cz.cvut.fel.productsvc.model.ProductEntity;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ProductRepository extends JpaRepository<ProductEntity,Integer> {
    ProductEntity findByName(String name);
}
