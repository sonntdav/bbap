package cz.cvut.fel.productsvc.controller;

import cz.cvut.fel.productsvc.exception.AccountAlreadyExistsException;
import cz.cvut.fel.productsvc.model.ProductEntity;
import cz.cvut.fel.productsvc.service.ProductService;
import jakarta.persistence.EntityNotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/product")
public class ProductController {
    private final ProductService productService;

    @Autowired
    public ProductController(ProductService productService) {
        this.productService = productService;
    }

    @GetMapping("/{productID}")
    public ResponseEntity<ProductEntity> getProduct(@PathVariable Integer productID) {
        try {
            ProductEntity product = productService.getProduct(productID);
            return ResponseEntity.ok(product);
        } catch (EntityNotFoundException e) {
            return ResponseEntity.status(HttpStatus.NOT_FOUND).body(null);
        }
    }

    @GetMapping("/byName/{productName}")
    public ResponseEntity<ProductEntity> getProduct(@PathVariable String productName) {
        try {
            ProductEntity product = productService.getProduct(productName);
            return ResponseEntity.ok(product);
        } catch (EntityNotFoundException e) {
            return ResponseEntity.status(HttpStatus.NOT_FOUND).body(null);
        }
    }

    @PostMapping
    public ResponseEntity<?> createProduct(@RequestBody ProductEntity product) {
        try {
            productService.createProduct(product);
            return ResponseEntity.status(HttpStatus.CREATED).build();
        } catch (AccountAlreadyExistsException e) {
            return ResponseEntity.status(HttpStatus.CONFLICT).body(e.getMessage());
        }
    }

    @PutMapping("/{productID}")
    public ResponseEntity<?> updateProduct(@PathVariable Integer productID, @RequestBody ProductEntity product) {
        product.setProductID(productID);
        try {
            productService.updateProduct(product);
            return ResponseEntity.ok().build();
        } catch (EntityNotFoundException e) {
            return ResponseEntity.status(HttpStatus.NOT_FOUND).body(e.getMessage());
        }
    }

    @DeleteMapping("/{productID}")
    public ResponseEntity<?> deleteProduct(@PathVariable Integer productID) {
        try {
            productService.deleteProduct(productID);
            return ResponseEntity.ok().build();
        } catch (EntityNotFoundException e) {
            return ResponseEntity.status(HttpStatus.NOT_FOUND).body(e.getMessage());
        }
    }

    @PostMapping("/valueOfProducts")
    public ResponseEntity<Double> getValueOfProducts(@RequestBody List<Integer> products) {
        try {
            double valueOfAllProducts = productService.getValueOfAllProducts(products);
            return ResponseEntity.ok(valueOfAllProducts);
        } catch (EntityNotFoundException e) {
            return ResponseEntity.status(HttpStatus.NOT_FOUND).body(null);
        }
    }

    @GetMapping
    public ResponseEntity<List<ProductEntity>> getListOfAllProducts() {
        List<ProductEntity> products = productService.getListOfAllProducts();
        return ResponseEntity.ok(products);
    }
}
