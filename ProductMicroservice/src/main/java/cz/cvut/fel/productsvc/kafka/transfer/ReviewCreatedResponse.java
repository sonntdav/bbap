package cz.cvut.fel.productsvc.kafka.transfer;

import lombok.NoArgsConstructor;

public class ReviewCreatedResponse {
    private Integer productID;
    private Integer reviewID;

    public ReviewCreatedResponse(Integer productID, Integer reviewID) {
        this.productID = productID;
        this.reviewID = reviewID;
    }

    public ReviewCreatedResponse(){
    }
    public Integer getProductID() {
        return productID;
    }

    public void setProductID(Integer productID) {
        this.productID = productID;
    }

    public Integer getReviewID() {
        return reviewID;
    }

    public void setReviewID(Integer reviewID) {
        this.reviewID = reviewID;
    }
}
