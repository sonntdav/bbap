package cz.cvut.fel.productsvc.model;

import jakarta.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Data;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

@Entity
@Data
@Table(name = "ProductMC")
@AllArgsConstructor
public class ProductEntity {
    public ProductEntity(){
        this.reviews = new ArrayList<>();
    }

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Integer productID;

    @ElementCollection(fetch = FetchType.EAGER)
    @CollectionTable(name = "product_reviews")
    private List<Integer> reviews;

    @Column(unique = true)
    private String name;
    private String description;
    private Double price;

    public void addReview(Integer review) {
        reviews.add(review);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof ProductEntity product)) return false;
        return productID.equals(product.productID) && name.equals(product.name) && description.equals(product.description) && price.equals(product.price);
    }

    @Override
    public int hashCode() {
        return Objects.hash(productID, name, description, price);
    }

    @Override
    public String toString() {
        return "ProductEntity{" +
                "productID=" + productID +
                ", name='" + name + '\'' +
                ", description='" + description + '\'' +
                ", price=" + price +
                '}';
    }
}
