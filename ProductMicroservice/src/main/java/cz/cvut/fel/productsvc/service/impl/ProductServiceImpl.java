package cz.cvut.fel.productsvc.service.impl;

import cz.cvut.fel.productsvc.exception.AccountAlreadyExistsException;
import cz.cvut.fel.productsvc.model.ProductEntity;
import cz.cvut.fel.productsvc.repository.ProductRepository;
import cz.cvut.fel.productsvc.service.ProductService;
import jakarta.persistence.EntityNotFoundException;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * Implementation of the {@link ProductService} interface that provides methods
 * to manage products.
 */
@Service
@AllArgsConstructor
@Slf4j
public class ProductServiceImpl implements ProductService {
    private ProductRepository productRepository;

    /**
     * Retrieves the product with the specified ID.
     *
     * @param productID the ID of the product to retrieve
     * @return the {@link ProductEntity} object with the specified ID
     * @throws EntityNotFoundException if the product with the specified ID does not exist
     */
    @Override
    public ProductEntity getProduct(Integer productID) throws EntityNotFoundException {
        return productRepository.findById(productID).orElseThrow(() -> new EntityNotFoundException("Product was not found. ID of product: " + productID));
    }

    /**
     * Retrieves the product with the specified ID.
     *
     * @param productName the name of the product
     * @return the {@link ProductEntity} object with the specified ID
     */
    @Override
    public ProductEntity getProduct(String productName) {
        return productRepository.findByName(productName);
    }

    /**
     * Creates a new product.
     *
     * @param product the {@link ProductEntity} object representing the product to create
     * @throws AccountAlreadyExistsException if a product with the same ID already exists
     */
    @Override
    public void createProduct(ProductEntity product) throws AccountAlreadyExistsException {
        if (product.getProductID() == null || productRepository.findById(product.getProductID()).isEmpty()) {
            productRepository.save(product);
        } else {
            throw new AccountAlreadyExistsException("Product with this ID already exists.");
        }
    }

    /**
     * Updates an existing product.
     *
     * @param product the {@link ProductEntity} object representing the updated product
     * @throws EntityNotFoundException if the product with the specified ID does not exist
     */
    @Override
    public void updateProduct(ProductEntity product) throws EntityNotFoundException {
        final ProductEntity originalProduct = getProduct(product.getProductID());
        product.setProductID(originalProduct.getProductID());
        productRepository.save(product);
    }

    /**
     * Deletes the product with the specified ID.
     *
     * @param productID the ID of the product to delete
     * @throws EntityNotFoundException if the product with the specified ID does not exist
     */
    @Override
    public void deleteProduct(Integer productID) throws EntityNotFoundException {
        final ProductEntity originalProduct = getProduct(productID);
        productRepository.delete(originalProduct);
    }

    /**
     * Retrieves a list of all products.
     *
     * @return a list of all {@link ProductEntity} objects
     */
    @Override
    public List<ProductEntity> getListOfAllProducts() {
        return productRepository.findAll();
    }

    @Override
    public double getValueOfAllProducts(List<Integer> productIDs) {
        double result = 0;
        for (Integer productID : productIDs) {
            result += getProduct(productID).getPrice();
        }
        return result;
    }
}
