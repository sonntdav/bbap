package cz.cvut.fel.productsvc;

import cz.cvut.fel.productsvc.model.ProductEntity;
import cz.cvut.fel.productsvc.service.ProductService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.event.ApplicationReadyEvent;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

import java.util.ArrayList;
import java.util.List;

@Component
public class StartUp {
    private final ProductService paymentService;

    @Autowired
    public StartUp(ProductService reviewService) {
        this.paymentService = reviewService;
    }

    @EventListener(ApplicationReadyEvent.class)
    public void init() {
        ProductEntity product = new ProductEntity(1, List.of(1),"string_product_name","string_description",20.00);
        paymentService.createProduct(product);
    }

}
