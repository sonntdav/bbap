package cz.cvut.fel.productsvc.service;


import cz.cvut.fel.productsvc.model.ProductEntity;

import java.util.List;

public interface ProductService {
    ProductEntity getProduct(Integer productID);

    ProductEntity getProduct(String productName);

    void createProduct(ProductEntity product);

    void updateProduct(ProductEntity product);

    void deleteProduct(Integer productID);

    List<ProductEntity> getListOfAllProducts();

    double getValueOfAllProducts(List<Integer> productIDs);
}
