package cz.cvut.fel.productsvc.exception;

public class ItemNotInCartException extends RuntimeException{
    public ItemNotInCartException(String message) {
        super(message);
    }
}