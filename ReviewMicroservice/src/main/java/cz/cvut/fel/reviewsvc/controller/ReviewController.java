package cz.cvut.fel.reviewsvc.controller;

import cz.cvut.fel.reviewsvc.model.ReviewEntity;
import cz.cvut.fel.reviewsvc.service.ReviewService;
import jakarta.persistence.EntityNotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/review")
public class ReviewController {
    private final ReviewService reviewService;

    @Autowired
    public ReviewController(ReviewService reviewService) {
        this.reviewService = reviewService;
    }

    @GetMapping("/{reviewID}")
    public ResponseEntity<ReviewEntity> getReview(@PathVariable Integer reviewID) {
        try {
            ReviewEntity review = reviewService.getReview(reviewID);
            return ResponseEntity.ok(review);
        } catch (EntityNotFoundException e) {
            return ResponseEntity.status(HttpStatus.NOT_FOUND).body(null);
        }
    }

    @GetMapping("/{reviewID}/author")
    public ResponseEntity<Integer> getAuthorOfReview(@PathVariable Integer reviewID) {
        try {
            Integer account = reviewService.getAuthorOfReview(reviewID);
            return ResponseEntity.ok(account);
        } catch (EntityNotFoundException e) {
            return ResponseEntity.status(HttpStatus.NOT_FOUND).body(null);
        }
    }

    @GetMapping("/product/{productID}")
    public ResponseEntity<List<ReviewEntity>> getAllReviews(@PathVariable Integer productID) {
        try {
            List<ReviewEntity> reviews = reviewService.getAllReviews(productID);
            return ResponseEntity.ok(reviews);
        } catch (EntityNotFoundException e) {
            return ResponseEntity.status(HttpStatus.NOT_FOUND).body(null);
        }
    }

    @PostMapping("/{productID}/{userID}")
    public ResponseEntity<?> addReview(
            @PathVariable Integer productID,
            @PathVariable Integer userID,
            @RequestBody String reviewMessage
    ) {
        try {
            reviewService.addReview(productID, userID, reviewMessage);
            return ResponseEntity.status(HttpStatus.CREATED).build();
        } catch (EntityNotFoundException e) {
            return ResponseEntity.status(HttpStatus.NOT_FOUND).body(e.getMessage());
        }
    }

    @PutMapping
    public ResponseEntity<?> updateReview(@RequestBody ReviewEntity reviewEntity) {
        try {
            reviewService.updateReview(reviewEntity);
            return ResponseEntity.ok().build();
        } catch (EntityNotFoundException e) {
            return ResponseEntity.status(HttpStatus.NOT_FOUND).body(e.getMessage());
        }
    }

    @DeleteMapping("/{reviewID}")
    public ResponseEntity<?> removeReview(@PathVariable Integer reviewID) {
        try {
            reviewService.removeReview(reviewID);
            return ResponseEntity.ok().build();
        } catch (EntityNotFoundException e) {
            return ResponseEntity.status(HttpStatus.NOT_FOUND).body(e.getMessage());
        }
    }
}
