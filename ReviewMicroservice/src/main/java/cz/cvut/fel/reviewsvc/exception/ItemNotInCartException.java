package cz.cvut.fel.reviewsvc.exception;

public class ItemNotInCartException extends RuntimeException{
    public ItemNotInCartException(String message) {
        super(message);
    }
}