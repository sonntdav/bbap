package cz.cvut.fel.reviewsvc.service.impl;


import cz.cvut.fel.reviewsvc.kafka.MyKafkaProducer;
import cz.cvut.fel.reviewsvc.model.ReviewEntity;
import cz.cvut.fel.reviewsvc.repository.ReviewRepository;
import cz.cvut.fel.reviewsvc.service.ReviewService;
import jakarta.persistence.EntityNotFoundException;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * Implementation of the {@link ReviewService} interface that provides methods
 * to manage product reviews.
 */
@Service
@Slf4j
public class ReviewServiceImpl implements ReviewService {
    private final ReviewRepository reviewRepository;
    private final MyKafkaProducer kafkaProducer;

    @Autowired
    public ReviewServiceImpl(ReviewRepository reviewRepository, MyKafkaProducer kafkaProducer) {
        this.reviewRepository = reviewRepository;
        this.kafkaProducer = kafkaProducer;
    }

    /**
     * Retrieves the review with the specified ID.
     *
     * @param reviewID the ID of the review to retrieve
     * @return the {@link ReviewEntity} object with the specified ID
     * @throws EntityNotFoundException if the review with the specified ID does not exist
     */
    @Override
    public ReviewEntity getReview(Integer reviewID) throws EntityNotFoundException {
        return reviewRepository.findById(reviewID).orElseThrow(() -> new EntityNotFoundException("Review with this ID not found."));
    }

    /**
     * Retrieves all reviews for the specified product.
     *
     * @param productReview the ID of the product to retrieve reviews for
     * @return a list of {@link ReviewEntity} objects for the specified product
     * @throws EntityNotFoundException if the product with the specified ID does not exist
     */
    @Override
    public List<ReviewEntity> getAllReviews(Integer productReview) throws EntityNotFoundException {
        return reviewRepository.findAllByProduct(productReview);
    }

    /**
     * Adds a new review for the specified product and user.
     *
     * @param productID    the ID of the product
     * @param userID       the ID of the user
     * @param reviewMessage the review message
     * @throws EntityNotFoundException if the product or user with the specified IDs does not exist
     */
    @Override
    public void addReview(Integer productID, Integer userID, String reviewMessage) throws EntityNotFoundException {
        ReviewEntity reviewEntity = new ReviewEntity(userID, productID, reviewMessage);
        reviewRepository.save(reviewEntity);
        kafkaProducer.reviewCreated(productID,reviewEntity.getReviewID());
    }

    /**
     * Updates an existing review.
     *
     * @param reviewEntity the {@link ReviewEntity} object representing the updated review
     */
    @Override
    public void updateReview(ReviewEntity reviewEntity) {
        reviewRepository.save(reviewEntity);
    }

    /**
     * Removes the review with the specified ID.
     *
     * @param reviewID the ID of the review to remove
     * @throws EntityNotFoundException if the review with the specified ID does not exist
     */
    @Override
    public void removeReview(Integer reviewID) throws EntityNotFoundException {
        reviewRepository.delete(getReview(reviewID));
    }

    @Override
    public Integer getAuthorOfReview(Integer reviewID) {
        ReviewEntity review = getReview(reviewID);
        return review.getAuthor();
    }
}
