package cz.cvut.fel.reviewsvc;

import cz.cvut.fel.reviewsvc.service.ReviewService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.event.ApplicationReadyEvent;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

@Component
public class StartUp {
    private final ReviewService reviewService;
    private final RestTemplate restTemplate;

    @Autowired
    public StartUp(ReviewService reviewService, RestTemplate restTemplate) {
        this.reviewService = reviewService;
        this.restTemplate = restTemplate;
    }

    @EventListener(ApplicationReadyEvent.class)
    public void init() {
        reviewService.addReview(1,1,"spring_review");
    }

}
