package cz.cvut.fel.reviewsvc.kafka;

import com.fasterxml.jackson.core.JsonProcessingException;
import cz.cvut.fel.reviewsvc.kafka.transfer.ReviewCreatedResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.stereotype.Component;

import java.io.Serializable;

@Component
public class MyKafkaProducer implements Serializable {
    private final KafkaTemplate<String, String> kafkaTemplate;
    private final JsonSerializer jsonSerializer = new JsonSerializer();
    @Autowired
    public MyKafkaProducer(KafkaTemplate<String, String> kafkaTemplate) {
        this.kafkaTemplate = kafkaTemplate;
    }

    public void reviewCreated(Integer productID, Integer reviewID){
        ReviewCreatedResponse reviewCreatedResponse = new ReviewCreatedResponse();
        reviewCreatedResponse.setReviewID(reviewID);
        reviewCreatedResponse.setProductID(productID);
        try {
            kafkaTemplate.send("reviewCreated",jsonSerializer.transferClassToJson(reviewCreatedResponse));
        } catch (JsonProcessingException e) {
            e.printStackTrace();
        }
    }
}
