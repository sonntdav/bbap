package cz.cvut.fel.reviewsvc.exception;

public class OrderAlreadyPayedException extends RuntimeException{
    public OrderAlreadyPayedException(String message) {
        super(message);
    }
}
