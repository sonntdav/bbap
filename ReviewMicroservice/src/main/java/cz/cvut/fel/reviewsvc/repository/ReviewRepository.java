package cz.cvut.fel.reviewsvc.repository;

import cz.cvut.fel.reviewsvc.model.ReviewEntity;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface ReviewRepository extends JpaRepository<ReviewEntity, Integer> {

    List<ReviewEntity> findAllByProduct(Integer id);
}
