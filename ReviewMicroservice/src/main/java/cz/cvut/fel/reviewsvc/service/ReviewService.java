package cz.cvut.fel.reviewsvc.service;

import cz.cvut.fel.reviewsvc.model.ReviewEntity;

import java.util.List;

public interface ReviewService {
    ReviewEntity getReview(Integer reviewID);

    List<ReviewEntity> getAllReviews(Integer productReview);

    void addReview(Integer productID,Integer userID ,String reviewMessage);

    void updateReview(ReviewEntity reviewEntity);

    void removeReview(Integer reviewID);

    Integer getAuthorOfReview(Integer reviewID);

}

