package cz.cvut.fel.reviewsvc.kafka;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import cz.cvut.fel.reviewsvc.kafka.transfer.ReviewCreatedResponse;


public class JsonSerializer {
    private final ObjectMapper obj = new ObjectMapper();

    public String transferClassToJson(Object o) throws JsonProcessingException {
        return obj.writeValueAsString(o);
    }

    public ReviewCreatedResponse departmentFromJson(String json) throws JsonProcessingException {
        return obj.readValue(json, ReviewCreatedResponse.class);
    }

}


