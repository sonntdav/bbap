package cz.cvut.fel.reviewsvc.model;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonIgnore;
import jakarta.persistence.*;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Objects;

@Entity
@Data
@NoArgsConstructor
@Table(name = "Review_MC")
public class ReviewEntity {
    public ReviewEntity(Integer author, Integer product, String message) {
        this.author = author;
        this.product = product;
        this.message = message;
    }

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Integer reviewID;
    private String message;

    private Integer author;
    private Integer product;

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof ReviewEntity that)) return false;
        return Objects.equals(reviewID, that.reviewID);
    }

    @Override
    public int hashCode() {
        return Objects.hash(reviewID);
    }


}
