package cz.cvut.fel.monolith.service.impl;

import cz.cvut.fel.monolith.model.AccountEntity;
import cz.cvut.fel.monolith.model.OrderEntity;
import cz.cvut.fel.monolith.model.PaymentEntity;
import cz.cvut.fel.monolith.repository.PaymentRepository;
import cz.cvut.fel.monolith.service.PaymentService;
import jakarta.persistence.EntityNotFoundException;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

/**
 * Implementation of the {@link PaymentService} interface that provides methods
 * to manage payments.
 */
@Service
@AllArgsConstructor
@Slf4j
public class PaymentServiceImpl implements PaymentService {
    private PaymentRepository paymentRepository;

    /**
     * Retrieves the payment with the specified ID.
     *
     * @param paymentID the ID of the payment to retrieve
     * @return the {@link PaymentEntity} object with the specified ID
     * @throws EntityNotFoundException if the payment with the specified ID does not exist
     */
    @Override
    public PaymentEntity getPayment(Integer paymentID) throws EntityNotFoundException {
        return paymentRepository.findById(paymentID).orElseThrow(() -> new EntityNotFoundException("Payment with this ID not found."));
    }

    /**
     * Retrieves the account associated with the payment with the specified ID.
     *
     * @param paymentID the ID of the payment
     * @return the {@link AccountEntity} object associated with the payment
     * @throws EntityNotFoundException if the payment with the specified ID does not exist
     */
    @Override
    public AccountEntity getAccount(Integer paymentID) throws EntityNotFoundException {
        PaymentEntity payment = getPayment(paymentID);
        return payment.getOrderEntity().getAccount();
    }

    /**
     * Retrieves the order associated with the payment with the specified ID.
     *
     * @param paymentID the ID of the payment
     * @return the {@link OrderEntity} object associated with the payment
     * @throws EntityNotFoundException if the payment with the specified ID does not exist
     */
    @Override
    public OrderEntity getOrder(Integer paymentID) throws EntityNotFoundException {
        PaymentEntity payment = getPayment(paymentID);
        return payment.getOrderEntity();
    }
}
