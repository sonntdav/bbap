package cz.cvut.fel.monolith.service.impl;

import cz.cvut.fel.monolith.model.AccountEntity;
import cz.cvut.fel.monolith.model.ProductEntity;
import cz.cvut.fel.monolith.model.ReviewEntity;
import cz.cvut.fel.monolith.repository.ReviewRepository;
import cz.cvut.fel.monolith.service.AccountService;
import cz.cvut.fel.monolith.service.ProductService;
import cz.cvut.fel.monolith.service.ReviewService;
import jakarta.persistence.EntityNotFoundException;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * Implementation of the {@link ReviewService} interface that provides methods
 * to manage product reviews.
 */
@Service
@Slf4j
public class ReviewServiceImpl implements ReviewService {
    private final ReviewRepository reviewRepository;
    private final ProductService productService;
    private final AccountService accountService;

    @Autowired
    public ReviewServiceImpl(ReviewRepository reviewRepository, ProductService productService, AccountService accountService) {
        this.reviewRepository = reviewRepository;
        this.productService = productService;
        this.accountService = accountService;
    }

    /**
     * Retrieves the review with the specified ID.
     *
     * @param reviewID the ID of the review to retrieve
     * @return the {@link ReviewEntity} object with the specified ID
     * @throws EntityNotFoundException if the review with the specified ID does not exist
     */
    @Override
    public ReviewEntity getReview(Integer reviewID) throws EntityNotFoundException {
        return reviewRepository.findById(reviewID).orElseThrow(() -> new EntityNotFoundException("Review with this ID not found."));
    }

    /**
     * Retrieves all reviews for the specified product.
     *
     * @param productReview the ID of the product to retrieve reviews for
     * @return a list of {@link ReviewEntity} objects for the specified product
     * @throws EntityNotFoundException if the product with the specified ID does not exist
     */
    @Override
    public List<ReviewEntity> getAllReviews(Integer productReview) throws EntityNotFoundException {
        ProductEntity product = productService.getProduct(productReview);
        return product.getReviews();
    }

    /**
     * Adds a new review for the specified product and user.
     *
     * @param productID    the ID of the product
     * @param userID       the ID of the user
     * @param reviewMessage the review message
     * @throws EntityNotFoundException if the product or user with the specified IDs does not exist
     */
    @Override
    public void addReview(Integer productID, Integer userID, String reviewMessage) throws EntityNotFoundException {
        AccountEntity account = accountService.getAccount(userID);
        ProductEntity product = productService.getProduct(productID);
        ReviewEntity reviewEntity = new ReviewEntity(account, product, reviewMessage);
        product.addReview(reviewEntity);
        productService.updateProduct(product);
    }

    /**
     * Updates an existing review.
     *
     * @param reviewEntity the {@link ReviewEntity} object representing the updated review
     */
    @Override
    public void updateReview(ReviewEntity reviewEntity) {
        reviewRepository.save(reviewEntity);
    }

    /**
     * Removes the review with the specified ID.
     *
     * @param reviewID the ID of the review to remove
     * @throws EntityNotFoundException if the review with the specified ID does not exist
     */
    @Override
    public void removeReview(Integer reviewID) throws EntityNotFoundException {
        reviewRepository.delete(getReview(reviewID));
    }

    @Override
    public AccountEntity getAuthorOfReview(Integer reviewID) {
        ReviewEntity review = getReview(reviewID);
        return review.getAuthor();
    }
}
