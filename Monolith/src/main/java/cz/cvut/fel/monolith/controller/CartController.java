package cz.cvut.fel.monolith.controller;

import cz.cvut.fel.monolith.exception.ItemNotInCartException;
import cz.cvut.fel.monolith.model.CartEntity;
import cz.cvut.fel.monolith.model.CartItemEntity;
import cz.cvut.fel.monolith.service.CartService;
import jakarta.persistence.EntityNotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/cart")
public class CartController {
    private final CartService cartService;

    @Autowired
    public CartController(CartService cartService) {
        this.cartService = cartService;
    }

    @GetMapping("/{cartId}")
    public ResponseEntity<CartEntity> getCart(@PathVariable Integer cartId) {
        try {
            CartEntity cart = cartService.getCart(cartId);
            return ResponseEntity.ok(cart);
        } catch (EntityNotFoundException e) {
            return ResponseEntity.status(HttpStatus.NOT_FOUND).body(null);
        }
    }

    @PostMapping("/{cartId}/products/{productId}")
    public ResponseEntity<?> addProductToCart(
            @PathVariable Integer cartId,
            @PathVariable Integer productId
    ) {
        try {
                cartService.addProduct(cartId, productId);
            return ResponseEntity.ok().build();
        } catch (EntityNotFoundException e) {
            return ResponseEntity.status(HttpStatus.NOT_FOUND).body(e.getMessage());
        }
    }

    @DeleteMapping("/{cartId}/products/{productId}")
    public ResponseEntity<?> removeProductFromCart(
            @PathVariable Integer cartId,
            @PathVariable Integer productId
    ) {
        try {
            cartService.removeItem(cartId, productId);
            return ResponseEntity.ok().build();
        } catch (EntityNotFoundException | ItemNotInCartException e) {
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(e.getMessage());
        }
    }

    @GetMapping("/{cartId}/owner")
    public ResponseEntity<?> getOwnerOfCart(@PathVariable Integer cartId) {
        try {
            return ResponseEntity.ok(cartService.getOwnerOfCart(cartId));
        } catch (EntityNotFoundException e) {
            return ResponseEntity.status(HttpStatus.NOT_FOUND).body(e.getMessage());
        }
    }

    @GetMapping("/{cartId}/items")
    public ResponseEntity<List<CartItemEntity>> getAllItemsInCart(@PathVariable Integer cartId) {
        try {
            return ResponseEntity.ok(cartService.getAllItemsInCart(cartId));
        } catch (EntityNotFoundException e) {
            return ResponseEntity.status(HttpStatus.NOT_FOUND).body(null);
        }
    }

    @PostMapping("/{cartId}/checkout")
    public ResponseEntity<?> checkout(@PathVariable Integer cartId) {
        try {
            cartService.checkout(cartId);
            return ResponseEntity.ok().build();
        } catch (EntityNotFoundException e) {
            return ResponseEntity.status(HttpStatus.NOT_FOUND).body(e.getMessage());
        }
    }
}
