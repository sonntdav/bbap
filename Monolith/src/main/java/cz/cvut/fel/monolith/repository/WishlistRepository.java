package cz.cvut.fel.monolith.repository;

import cz.cvut.fel.monolith.model.WishlistEntity;
import org.springframework.data.jpa.repository.JpaRepository;

public interface WishlistRepository extends JpaRepository<WishlistEntity, Integer> {
}
