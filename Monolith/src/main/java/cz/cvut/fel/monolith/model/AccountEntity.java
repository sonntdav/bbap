package cz.cvut.fel.monolith.model;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonManagedReference;
import jakarta.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;


@Entity
@Data
@AllArgsConstructor
public class AccountEntity {
    public AccountEntity(){
        this.cartEntity = new CartEntity();
        this.wishlist = new WishlistEntity();
        this.orders = new ArrayList<>();
        this.balance = 0.0;
    }
    public AccountEntity(String username, String password, String email, String cityName, String street, Double balance, CartEntity cartEntity) {
        this.username = username;
        this.password = password;
        this.email = email;
        this.cityName = cityName;
        this.street = street;
        this.balance = balance;
        this.cartEntity = cartEntity;
        this.orders = new ArrayList<>();
        this.wishlist = new WishlistEntity();
    }

    public AccountEntity(String username, String password, String email, String cityName, String street, Double balance, CartEntity cartEntity, List<OrderEntity> orders, WishlistEntity wishlist) {
        this.username = username;
        this.password = password;
        this.email = email;
        this.cityName = cityName;
        this.street = street;
        this.balance = balance;
        this.cartEntity = cartEntity;
        this.orders = orders;
        this.wishlist = wishlist;
    }

    public AccountEntity(String username, String password, String email, String cityName, String street, Double balance) {
        this.username = username;
        this.password = password;
        this.email = email;
        this.cityName = cityName;
        this.street = street;
        this.balance = balance;
        this.cartEntity = new CartEntity(this);
        this.orders = new ArrayList<>();
        this.wishlist = new WishlistEntity();
    }

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Integer id;
    @Column(unique = true)
    private String username;
    private String password;
    private String email;
    private String cityName;
    private String street;
    private Double balance;

    @JsonManagedReference
    @OneToOne(cascade = CascadeType.ALL)
    private CartEntity cartEntity;

    @JsonManagedReference
    @OneToMany(cascade = CascadeType.ALL,mappedBy = "account", fetch = FetchType.EAGER)
    private List<OrderEntity> orders;

    @JsonManagedReference
    @OneToOne(cascade = CascadeType.ALL, fetch = FetchType.EAGER)
    private WishlistEntity wishlist;

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof AccountEntity)) return false;
        AccountEntity account = (AccountEntity) o;
        return id.equals(account.id) && username.equals(account.username);
    }

    @Override
    public String toString() {
        return "AccountEntity{" +
                "id=" + id +
                ", username='" + username + '\'' +
                ", password='" + password + '\'' +
                ", email='" + email + '\'' +
                ", cityName='" + cityName + '\'' +
                ", street='" + street + '\'' +
                ", balance=" + balance +
                '}';
    }
}


