package cz.cvut.fel.monolith.model;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonIgnore;
import jakarta.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Entity
@Data
@NoArgsConstructor
public class WishlistEntity {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Integer wishlistID;
    @JsonBackReference
    @OneToOne(mappedBy = "wishlist",cascade = CascadeType.ALL)
    private AccountEntity account;

    @ManyToMany(cascade = CascadeType.ALL)
    private List<ProductEntity> productsInWishlist;
}
