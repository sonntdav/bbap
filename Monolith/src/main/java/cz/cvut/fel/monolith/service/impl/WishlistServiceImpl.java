package cz.cvut.fel.monolith.service.impl;

import cz.cvut.fel.monolith.model.ProductEntity;
import cz.cvut.fel.monolith.model.WishlistEntity;
import cz.cvut.fel.monolith.repository.WishlistRepository;
import cz.cvut.fel.monolith.service.ProductService;
import cz.cvut.fel.monolith.service.WishlistService;
import jakarta.persistence.EntityNotFoundException;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * Implementation of the {@link WishlistService} interface that provides methods
 * to manage wishlists.
 */
@Service
@Slf4j
public class WishlistServiceImpl implements WishlistService {
    private final WishlistRepository wishlistRepository;
    private final ProductService productService;

    @Autowired
    public WishlistServiceImpl(WishlistRepository wishlistRepository, ProductService productService) {
        this.wishlistRepository = wishlistRepository;
        this.productService = productService;
    }

    /**
     * Adds an item to the wishlist.
     *
     * @param wishlistId the ID of the wishlist
     * @param itemId     the ID of the item to add
     * @throws EntityNotFoundException if the wishlist or item with the specified IDs does not exist
     */
    @Override
    public void addItemToWishlist(Integer wishlistId, Integer itemId) throws EntityNotFoundException {
        WishlistEntity wishlist = getWishlist(wishlistId);
        ProductEntity product = productService.getProduct(itemId);
        wishlist.getProductsInWishlist().add(product);
        wishlistRepository.save(wishlist);
    }

    /**
     * Removes an item from the wishlist.
     *
     * @param wishlistId the ID of the wishlist
     * @param itemId     the ID of the item to remove
     * @throws EntityNotFoundException if the wishlist or item with the specified IDs does not exist
     */
    @Override
    public void removeItemFromWishlist(Integer wishlistId, Integer itemId) throws EntityNotFoundException {
        WishlistEntity wishlist = getWishlist(wishlistId);
        ProductEntity product = productService.getProduct(itemId);
        wishlist.getProductsInWishlist().remove(product);
        wishlistRepository.save(wishlist);
    }

    /**
     * Retrieves the wishlist with the specified ID.
     *
     * @param id the ID of the wishlist to retrieve
     * @return the {@link WishlistEntity} object with the specified ID
     * @throws EntityNotFoundException if the wishlist with the specified ID does not exist
     */
    @Override
    public WishlistEntity getWishlist(Integer id) throws EntityNotFoundException {
        return wishlistRepository.findById(id).orElseThrow(() -> new EntityNotFoundException("Wishlist with this ID not found."));
    }

    /**
     * Retrieves all items from the wishlist with the specified ID.
     *
     * @param id the ID of the wishlist
     * @return a list of {@link ProductEntity} objects representing the items in the wishlist
     * @throws EntityNotFoundException if the wishlist with the specified ID does not exist
     */
    @Override
    public List<ProductEntity> getAllItemsFromWishList(Integer id) throws EntityNotFoundException {
        WishlistEntity wishlist = getWishlist(id);
        return wishlist.getProductsInWishlist();
    }   
}
