package cz.cvut.fel.monolith.repository;

import cz.cvut.fel.monolith.model.PaymentEntity;
import org.springframework.data.jpa.repository.JpaRepository;

public interface PaymentRepository extends JpaRepository<PaymentEntity, Integer> {
}
