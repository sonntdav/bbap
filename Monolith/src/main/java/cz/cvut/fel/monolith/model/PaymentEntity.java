package cz.cvut.fel.monolith.model;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonManagedReference;
import jakarta.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;

@Entity
@Data
@NoArgsConstructor
public class PaymentEntity {
    public PaymentEntity(OrderEntity order){
        this.paymentDate = new Date();
    }
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Integer paymentID;
    private Date paymentDate;

    @JsonBackReference
    @OneToOne(mappedBy = "payment",cascade = CascadeType.ALL)
    private OrderEntity orderEntity;

}
