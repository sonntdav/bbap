package cz.cvut.fel.monolith.service.impl;

import cz.cvut.fel.monolith.exception.ItemNotInCartException;
import cz.cvut.fel.monolith.model.*;
import cz.cvut.fel.monolith.repository.CartRepository;
import cz.cvut.fel.monolith.service.CartService;
import cz.cvut.fel.monolith.service.OrderService;
import cz.cvut.fel.monolith.service.ProductService;
import jakarta.persistence.EntityNotFoundException;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
@AllArgsConstructor
@Slf4j
public class CartServiceImpl implements CartService {
    private CartRepository cartRepository;
    private ProductService productService;
    private OrderService orderService;

    /**
     * Retrieves the {@link CartEntity} with the specified ID.
     *
     * @param cartId the ID of the cart to retrieve
     * @return the {@link CartEntity} with the specified ID
     * @throws EntityNotFoundException if the cart is not found
     */
    @Override
    public CartEntity getCart(Integer cartId) {
        return cartRepository.findById(cartId).orElseThrow(() -> new EntityNotFoundException("cart was not found. ID of cart: " + cartId));
    }

    /**
     * Adds a product to the specified cart.
     *
     * @param cartID    the ID of the cart
     * @param productID the ID of the product to add
     * @throws EntityNotFoundException if the cart or product is not found
     */
    @Override
    public void addProduct(Integer cartID, Integer productID) {
        CartEntity cartEntity = cartRepository.findById(cartID).orElseThrow(() -> new EntityNotFoundException("cart was not found. ID of cart: " + cartID));
        ProductEntity product = productService.getProduct(productID);
        for (CartItemEntity cartItemEntity : cartEntity.getCartItemEntities()) {
            if(cartItemEntity.getProductEntity().equals(product)){
                increaseQuantity(cartID,productID);
                return;
            }
        }
        CartItemEntity newCartItemEntity = new CartItemEntity(product,cartEntity);
        cartEntity.getCartItemEntities().add(newCartItemEntity);
        cartRepository.save(cartEntity);
    }

    /**
     * Increases the quantity of a product in the specified cart.
     *
     * @param cartID    the ID of the cart
     * @param productID the ID of the product
     * @throws EntityNotFoundException    if the cart or product is not found
     * @throws ItemNotInCartException     if the product is not in the cart
     */
    @Override
    public void increaseQuantity(Integer cartID, Integer productID) {
        CartEntity cartEntity = cartRepository.findById(cartID).orElseThrow(() -> new EntityNotFoundException("cart was not found. ID of cart: " + cartID));
        ProductEntity product = productService.getProduct(productID);
        for (CartItemEntity cartItemEntity : cartEntity.getCartItemEntities()) {
            if(cartItemEntity.getProductEntity().equals(product)){
                cartItemEntity.increaseAmount();
                cartRepository.save(cartEntity);
                return;
            }
        }
        throw new ItemNotInCartException("product with this ID was not in the cart!");
    }

    /**
     * Decreases the quantity of a product in the specified cart.
     * If the quantity becomes less than 1, the item is removed from the cart.
     *
     * @param cartID    the ID of the cart
     * @param productID the ID of the product
     * @throws EntityNotFoundException    if the cart or product is not found
     * @throws ItemNotInCartException     if the product is not in the cart
     */
    @Override
    public void decreaseQuantity(Integer cartID, Integer productID) {
        CartEntity cartEntity = cartRepository.findById(cartID).orElseThrow(() -> new EntityNotFoundException("cart was not found. ID of cart: " + cartID));
        ProductEntity product = productService.getProduct(productID);
        for (CartItemEntity cartItemEntity : cartEntity.getCartItemEntities()) {
            if(cartItemEntity.getProductEntity().equals(product)){
                cartItemEntity.decreaseAmount();
                if (cartItemEntity.getAmount() < 1){
                    removeItem(cartID,productID);
                }
                else {
                    cartRepository.save(cartEntity);
                }
                return;
            }
        }
        throw new ItemNotInCartException("product with this ID was not in the cart!");
    }


    /**
     * Removes an item from the specified cart.
     *
     * @param cartID    the ID of the cart
     * @param productID the ID of the product to remove
     * @throws EntityNotFoundException if the cart or product is not found
     * @throws ItemNotInCartException  if the product is not in the cart
     */
    @Override
    public void removeItem(Integer cartID, Integer productID) {
        CartEntity cartEntity = cartRepository.findById(cartID).orElseThrow(() -> new EntityNotFoundException("cart was not found. ID of cart: " + cartID));
        ProductEntity product = productService.getProduct(productID);

        for (CartItemEntity cartItemEntity : cartEntity.getCartItemEntities()) {
            if(cartItemEntity.getProductEntity().equals(product)){
                cartEntity.getCartItemEntities().remove(cartItemEntity);
                cartRepository.save(cartEntity);
                return;
            }
        }
        throw new ItemNotInCartException("product with this ID was not in the cart!");
    }

    /**
     * Retrieves the owner of the specified cart.
     *
     * @param cartID the ID of the cart
     * @return the {@link AccountEntity} that owns the cart
     * @throws EntityNotFoundException if the cart is not found
     */
    @Override
    public AccountEntity getOwnerOfCart(Integer cartID) {
        CartEntity cartEntity = cartRepository.findById(cartID).orElseThrow(() -> new EntityNotFoundException("cart was not found. ID of cart: " + cartID));
        return cartEntity.getAccount();
    }

    /**
     * Retrieves all items in the specified cart.
     *
     * @param cartID the ID of the cart
     * @return a list of {@link CartItemEntity} representing the items in the cart
     * @throws EntityNotFoundException if the cart is not found
     */
    @Override
    public List<CartItemEntity> getAllItemsInCart(Integer cartID) {
        CartEntity cartEntity = cartRepository.findById(cartID).orElseThrow(() -> new EntityNotFoundException("cart was not found. ID of cart: " + cartID));
        return cartEntity.getCartItemEntities();
    }

    /**
     * Performs the checkout process for the specified cart,
     * creating an order with the cart items and associating it with the account.
     *
     * @param cartID the ID of the cart
     * @throws EntityNotFoundException if the cart is not found
     */
    @Override
    public void checkout(Integer cartID) {
        CartEntity cartEntity = cartRepository.findById(cartID).orElseThrow(() -> new EntityNotFoundException("cart was not found. ID of cart: " + cartID));
        OrderEntity order = new OrderEntity(cartEntity.getAccount());
        List<OrderItemEntity> orderItems = new ArrayList<>();
        for (CartItemEntity cartItemEntity : cartEntity.getCartItemEntities()) {
            orderItems.add(new OrderItemEntity(cartItemEntity.getAmount(),cartItemEntity.getProductEntity(),order));
            cartItemEntity.setCartEntity(null);
        }
        cartRepository.save(cartEntity);
        order.setOrderItems(orderItems);
        orderService.createOrder(order);
    }

}
