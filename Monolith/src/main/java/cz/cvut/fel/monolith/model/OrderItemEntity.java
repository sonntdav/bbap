package cz.cvut.fel.monolith.model;

import com.fasterxml.jackson.annotation.JsonBackReference;
import jakarta.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Entity
@Data
@NoArgsConstructor
@AllArgsConstructor
public class OrderItemEntity {
    public OrderItemEntity(Integer amount, ProductEntity productEntity, OrderEntity order) {
        this.amount = amount;
        this.productEntity = productEntity;
        this.order = order;
    }

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Integer orderItemID;
    private Integer amount;
    @ManyToOne(cascade = CascadeType.ALL)
    private ProductEntity productEntity;

    @JsonBackReference
    @ManyToOne(cascade = CascadeType.ALL)
    private OrderEntity order;


}
