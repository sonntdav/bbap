package cz.cvut.fel.monolith.service;

import cz.cvut.fel.monolith.model.AccountEntity;
import cz.cvut.fel.monolith.model.OrderEntity;
import cz.cvut.fel.monolith.model.PaymentEntity;

public interface PaymentService {
    PaymentEntity getPayment(Integer paymentID);

    AccountEntity getAccount(Integer paymentID);

    OrderEntity getOrder(Integer paymentID);
}

