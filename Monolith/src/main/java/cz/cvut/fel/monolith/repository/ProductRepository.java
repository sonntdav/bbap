package cz.cvut.fel.monolith.repository;

import cz.cvut.fel.monolith.model.ProductEntity;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ProductRepository extends JpaRepository<ProductEntity,Integer> {
    ProductEntity findByName(String name);
}
