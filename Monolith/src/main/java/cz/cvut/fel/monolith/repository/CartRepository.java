package cz.cvut.fel.monolith.repository;

import cz.cvut.fel.monolith.model.CartEntity;
import org.springframework.data.jpa.repository.JpaRepository;

public interface CartRepository extends JpaRepository<CartEntity,Integer> {
}
