package cz.cvut.fel.monolith.exception;

public class OrderAlreadyPayedException extends RuntimeException{
    public OrderAlreadyPayedException(String message) {
        super(message);
    }
}
