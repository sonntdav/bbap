package cz.cvut.fel.monolith.service.impl;

import cz.cvut.fel.monolith.exception.AccountAlreadyExistsException;
import cz.cvut.fel.monolith.exception.BalanceException;
import cz.cvut.fel.monolith.exception.OrderAlreadyPayedException;
import cz.cvut.fel.monolith.model.*;
import cz.cvut.fel.monolith.repository.OrderRepository;
import cz.cvut.fel.monolith.service.AccountService;
import cz.cvut.fel.monolith.service.OrderService;
import jakarta.persistence.EntityNotFoundException;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import java.util.List;

/**
 * Implementation of the {@link OrderService} interface that provides methods
 * to manage orders.
 */
@Service
@AllArgsConstructor
@Slf4j
public class OrderServiceImpl implements OrderService {
    private OrderRepository orderRepository;
    private AccountService accountService;
    /**
     * Retrieves the order with the specified ID.
     *
     * @param id the ID of the order to retrieve
     * @return the {@link OrderEntity} object with the specified ID
     * @throws EntityNotFoundException if the order with the specified ID does not exist
     */
    @Override
    public OrderEntity getOrder(Integer id) throws EntityNotFoundException {
        return orderRepository.findById(id).orElseThrow(() -> new EntityNotFoundException("Order with this ID does not exist. Order ID: " + id));
    }

    /**
     * Creates a new order.
     *
     * @param order the {@link OrderEntity} object representing the order to create
     * @throws AccountAlreadyExistsException if an order with the same ID already exists
     */
    @Override
    public void createOrder(OrderEntity order) throws AccountAlreadyExistsException {
        if (order.getOrderID() == null || orderRepository.findById(order.getOrderID()).isEmpty()) {
            orderRepository.save(order);
        } else {
            throw new AccountAlreadyExistsException("Order with this ID already exists.");
        }
    }

    /**
     * Retrieves all orders.
     *
     * @return a list of all {@link OrderEntity} objects
     */
    @Override
    public List<OrderEntity> getAllOrders() {
        return orderRepository.findAll();
    }

    /**
     * Deletes the order with the specified ID.
     *
     * @param id the ID of the order to delete
     * @throws EntityNotFoundException if the order with the specified ID does not exist
     */
    @Override
    public void deleteOrder(Integer id) throws EntityNotFoundException {
        final OrderEntity order = getOrder(id);
        orderRepository.delete(order);
    }

    /**
     * Pays for the order with the specified ID.
     *
     * @param id the ID of the order to pay for
     * @throws OrderAlreadyPayedException if the order was already paid for
     * @throws BalanceException if the account does not have enough balance to pay for the order
     */
    @Override
    public void payForOrder(Integer id) throws OrderAlreadyPayedException {
        final OrderEntity order = getOrder(id);
        if (order.getOrderStatus() == OrderStatus.PENDING) {
            AccountEntity account = order.getAccount();
            double priceOfOrder = getTotalPriceOfOrder(order);
            if (account.getBalance() >= priceOfOrder){
                accountService.withdrawBalance(account.getId(),priceOfOrder);
                order.setPaymentEntity(new PaymentEntity(order));
                orderRepository.save(order);
            }
            else {
                throw new BalanceException("Not enough money to pay for order!");
            }
            return;
        }
        throw new OrderAlreadyPayedException("Order was already paid for!");
    }

    /**
     * Marks the order with the specified ID as completed.
     *
     * @param id the ID of the order to mark as completed
     * @throws OrderAlreadyPayedException if the order is already completed or not paid for at all
     */
    @Override
    public void finishOrder(Integer id) throws OrderAlreadyPayedException {
        final OrderEntity order = getOrder(id);
        if (order.getOrderStatus() == OrderStatus.IN_PROGRESS) {
            order.markAsCompleted();
            orderRepository.save(order);
            return;
        }
        throw new OrderAlreadyPayedException("Order is already completed or not paid for at all");
    }

    /**
     * Retrieves the owner account of the order with the specified ID.
     *
     * @param id the ID of the order
     * @return the {@link AccountEntity} object representing the order owner
     * @throws EntityNotFoundException if the order with the specified ID does not exist
     */
    @Override
    public AccountEntity getOrderOwner(Integer id) throws EntityNotFoundException {
        return getOrder(id).getAccount();
    }

    /**
     * Retrieves all items in the order with the specified ID.
     *
     * @param id the ID of the order
     * @return a list of {@link OrderItemEntity} objects representing the items in the order
     * @throws EntityNotFoundException if the order with the specified ID does not exist
     */
    @Override
    public List<OrderItemEntity> getAllItemsInOrder(Integer id) throws EntityNotFoundException {
        return getOrder(id).getOrderItems();
    }

    private double getTotalPriceOfOrder(OrderEntity order){
        double result = 0.0;
        for (OrderItemEntity orderItem : order.getOrderItems()) {
            result += orderItem.getProductEntity().getPrice() * orderItem.getAmount();
        }
        return result;
    }
}
