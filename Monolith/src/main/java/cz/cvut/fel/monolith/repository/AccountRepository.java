package cz.cvut.fel.monolith.repository;

import cz.cvut.fel.monolith.model.AccountEntity;
import org.springframework.data.jpa.repository.JpaRepository;

public interface AccountRepository extends JpaRepository<AccountEntity, Integer> {

    AccountEntity findByUsername(String username);
}
