package cz.cvut.fel.monolith.service;

import cz.cvut.fel.monolith.model.*;

import java.util.List;

public interface CartService {
    CartEntity getCart(Integer cartId);

    void addProduct(Integer cartID, Integer productID);

    void increaseQuantity(Integer cartID, Integer productID);

    void decreaseQuantity(Integer cartID, Integer productID);

    void removeItem(Integer cartID, Integer productID);

    AccountEntity getOwnerOfCart(Integer cartID);

    List<CartItemEntity> getAllItemsInCart(Integer cartID);

    void checkout(Integer cartID);

}

