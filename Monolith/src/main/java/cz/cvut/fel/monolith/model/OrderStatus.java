package cz.cvut.fel.monolith.model;

public enum OrderStatus {
    PENDING,
    IN_PROGRESS,
    COMPLETED
}
