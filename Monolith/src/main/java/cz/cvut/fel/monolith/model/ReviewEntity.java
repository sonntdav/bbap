package cz.cvut.fel.monolith.model;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonManagedReference;
import jakarta.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Objects;

@Entity
@Data
@NoArgsConstructor
public class ReviewEntity {
    public ReviewEntity(AccountEntity author, ProductEntity product, String message) {
        this.author = author;
        this.product = product;
        this.message = message;
    }

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Integer reviewID;
    private String message;


    @JsonIgnore
    @ManyToOne(cascade = CascadeType.ALL)
    private AccountEntity author;

    @JsonBackReference
    @ManyToOne(cascade = CascadeType.ALL)
    private ProductEntity product;

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof ReviewEntity)) return false;
        ReviewEntity that = (ReviewEntity) o;
        return Objects.equals(reviewID, that.reviewID);
    }

    @Override
    public int hashCode() {
        return Objects.hash(reviewID);
    }


}
