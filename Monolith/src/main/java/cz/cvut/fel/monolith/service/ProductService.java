package cz.cvut.fel.monolith.service;

import cz.cvut.fel.monolith.model.ProductEntity;

import java.util.List;

public interface ProductService {
    ProductEntity getProduct(Integer productID);

    ProductEntity getProduct(String productName);

    void createProduct(ProductEntity product);

    void updateProduct(ProductEntity product);

    void deleteProduct(Integer productID);

    List<ProductEntity> getListOfAllProducts();
}
