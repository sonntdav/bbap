package cz.cvut.fel.monolith.repository;

import cz.cvut.fel.monolith.model.ReviewEntity;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ReviewRepository extends JpaRepository<ReviewEntity, Integer> {
}
