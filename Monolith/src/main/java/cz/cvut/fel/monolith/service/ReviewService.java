package cz.cvut.fel.monolith.service;

import cz.cvut.fel.monolith.model.AccountEntity;
import cz.cvut.fel.monolith.model.ReviewEntity;

import java.util.List;

public interface ReviewService {
    ReviewEntity getReview(Integer reviewID);

    List<ReviewEntity> getAllReviews(Integer productReview);

    void addReview(Integer productID,Integer userID ,String reviewMessage);

    void updateReview(ReviewEntity reviewEntity);

    void removeReview(Integer reviewID);

    AccountEntity getAuthorOfReview(Integer reviewID);

}

