package cz.cvut.fel.monolith.service;

import cz.cvut.fel.monolith.model.ProductEntity;
import cz.cvut.fel.monolith.model.WishlistEntity;

import java.util.List;

public interface WishlistService {
    void addItemToWishlist(Integer wishlistId,Integer itemId);

    void removeItemFromWishlist(Integer wishlistId,Integer itemId);

    WishlistEntity getWishlist(Integer id);

    List<ProductEntity> getAllItemsFromWishList(Integer id);

}

