package cz.cvut.fel.monolith.model;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonManagedReference;
import jakarta.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Entity
@Data
@NoArgsConstructor
public class OrderEntity {
    public OrderEntity(AccountEntity account, List<OrderItemEntity> orderItems) {
        this.account = account;
        this.orderItems = orderItems;
        this.orderStatus = OrderStatus.PENDING;
        this.orderDate = new Date();
        this.orderItems = new ArrayList<>();
    }
    public OrderEntity(AccountEntity account){
        this.account = account;
        this.orderStatus = OrderStatus.PENDING;
        this.orderDate = new Date();
        this.orderItems = new ArrayList<>();
    }

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Integer orderID;
    private Date orderDate;
    @Enumerated
    private OrderStatus orderStatus;

    @JsonBackReference
    @ManyToOne(cascade = CascadeType.ALL)
    private AccountEntity account;

    @JsonManagedReference
    @OneToMany(mappedBy = "order",cascade = CascadeType.ALL, fetch = FetchType.EAGER)
    private List<OrderItemEntity> orderItems;


    @JsonManagedReference
    @OneToOne(cascade = CascadeType.ALL)
    private PaymentEntity payment;

    public void setPaymentEntity(PaymentEntity payment){
        this.payment = payment;
        this.orderStatus = OrderStatus.IN_PROGRESS;
    }

    public void markAsCompleted(){
        this.orderStatus = OrderStatus.COMPLETED;
    }

    @Override
    public String toString() {
        return "OrderEntity{" +
                "orderID=" + orderID +
                '}';
    }

}
