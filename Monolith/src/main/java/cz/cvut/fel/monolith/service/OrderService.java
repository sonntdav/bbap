package cz.cvut.fel.monolith.service;

import cz.cvut.fel.monolith.model.AccountEntity;
import cz.cvut.fel.monolith.model.OrderEntity;
import cz.cvut.fel.monolith.model.OrderItemEntity;

import java.util.List;

public interface OrderService {
    OrderEntity getOrder(Integer id);

    void createOrder(OrderEntity order);

    List<OrderEntity> getAllOrders();

    void deleteOrder(Integer id);

    void payForOrder(Integer id);

    void finishOrder(Integer id);

    AccountEntity getOrderOwner(Integer id);

    List<OrderItemEntity> getAllItemsInOrder(Integer id);
}

