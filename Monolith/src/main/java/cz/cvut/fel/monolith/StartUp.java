//package cz.cvut.fel.monolith;
//
//import cz.cvut.fel.monolith.model.AccountEntity;
//import cz.cvut.fel.monolith.model.ProductEntity;
//import cz.cvut.fel.monolith.service.*;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.boot.context.event.ApplicationReadyEvent;
//import org.springframework.context.event.EventListener;
//import org.springframework.http.ResponseEntity;
//import org.springframework.stereotype.Component;
//import org.springframework.web.client.RestTemplate;
//
//@Component
//public class StartUp {
//    private final AccountService accountService;
//    private final CartService cartService;
//    private final OrderService orderService;
//    private final ProductService productService;
//    private final ReviewService reviewService;
//    private final WishlistService wishlistService;
//    private final RestTemplate restTemplate;
//
//    @Autowired
//    public StartUp(AccountService accountService, ProductService productService, CartService cartService, OrderService orderService, PaymentService paymentService, ReviewService reviewService, WishlistService wishlistService, RestTemplate restTemplate) {
//        this.accountService = accountService;
//        this.productService = productService;
//        this.cartService = cartService;
//        this.orderService = orderService;
//        this.reviewService = reviewService;
//        this.wishlistService = wishlistService;
//
//        this.restTemplate = restTemplate;
//    }
//
//    @EventListener(ApplicationReadyEvent.class)
//    public void init() {
//        String url = "http://localhost:8080";
//        AccountEntity accountDto = new AccountEntity("spring_ name","pass","Spring_mail","spring_city","spring_street",10000.00);
//        restTemplate.postForEntity(url + "/account", accountDto, Void.class);
//
//        ProductEntity productDto = new ProductEntity("spring_product","productDescription",99.99);
//        restTemplate.postForEntity(url + "/product",productDto,Void.class);
//
////        cartService.addProduct(accountDto.getCartEntity().getCartId(),productDto.getProductID());
//        restTemplate.postForEntity(url + "/cart/1/products/1",null,Void.class);
//
//        restTemplate.postForEntity(url + "/cart/1/checkout",null,Void.class);
//       // cartService.checkout(accountDto.getCartEntity().getCartId());
//
//        restTemplate.postForEntity(url + "/cart/1/products/1",null,Void.class);
////        cartService.addProduct(1,1);
//
//        restTemplate.postForEntity(url + "/review/1/1","review message",Void.class);
//        //reviewService.addReview(1,1,"spring_review");
//
//        restTemplate.postForEntity(url + "/wishlist/1/items/1",null ,Void.class);
//        //wishlistService.addItemToWishlist(1,1);
//
//        restTemplate.postForEntity(url + "/order/1/pay",null,Void.class);
//    }
//
//}
