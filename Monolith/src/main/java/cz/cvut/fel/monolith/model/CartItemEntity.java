package cz.cvut.fel.monolith.model;

import com.fasterxml.jackson.annotation.JsonBackReference;
import jakarta.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Entity
@Data
@NoArgsConstructor
public class CartItemEntity {
    public CartItemEntity(ProductEntity product, CartEntity cartEntity){
        this.setAmount(1);
        this.setCartEntity(cartEntity);
        this.setProductEntity(product);
    }

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Integer CartItemId;
    private Integer amount;

    @ManyToOne(cascade = CascadeType.ALL)
    private ProductEntity productEntity;

    @JsonBackReference
    @ManyToOne(cascade = CascadeType.ALL)
    private CartEntity cartEntity;

    public void increaseAmount(){
        this.amount++;
    }
    public void decreaseAmount(){
        this.amount--;
    }
}
