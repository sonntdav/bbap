package cz.cvut.fel.monolith.controller;

import cz.cvut.fel.monolith.exception.AccountAlreadyExistsException;
import cz.cvut.fel.monolith.model.ProductEntity;
import cz.cvut.fel.monolith.service.ProductService;
import jakarta.persistence.EntityNotFoundException;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.*;

class ProductControllerMockitoTest {
    @Mock
    private ProductService productService;

    @InjectMocks
    private ProductController productController;

    @BeforeEach
    void setUp() {
        MockitoAnnotations.openMocks(this);
    }

    @Test
    void testGetProduct_Success() {
        ProductEntity productEntity = new ProductEntity();
        when(productService.getProduct(1)).thenReturn(productEntity);

        ResponseEntity<ProductEntity> response = productController.getProduct(1);

        assertEquals(HttpStatus.OK, response.getStatusCode());
        assertEquals(productEntity, response.getBody());
        verify(productService, times(1)).getProduct(1);
    }

    @Test
    void testGetProduct_NotFound() {
        when(productService.getProduct(1)).thenThrow(new EntityNotFoundException("Product not found"));

        ResponseEntity<ProductEntity> response = productController.getProduct(1);

        assertEquals(HttpStatus.NOT_FOUND, response.getStatusCode());
        assertEquals(null, response.getBody());
        verify(productService, times(1)).getProduct(1);
    }

}
