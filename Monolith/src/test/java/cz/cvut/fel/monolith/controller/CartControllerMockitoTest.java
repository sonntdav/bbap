package cz.cvut.fel.monolith.controller;

import cz.cvut.fel.monolith.controller.CartController;
import cz.cvut.fel.monolith.exception.ItemNotInCartException;
import cz.cvut.fel.monolith.model.AccountEntity;
import cz.cvut.fel.monolith.model.CartEntity;
import cz.cvut.fel.monolith.model.CartItemEntity;
import cz.cvut.fel.monolith.service.CartService;
import jakarta.persistence.EntityNotFoundException;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

class CartControllerMockitoTest {
    @Mock
    private CartService cartService;

    @InjectMocks
    private CartController cartController;

    @BeforeEach
    void setUp() {
        MockitoAnnotations.openMocks(this);
    }

    @Test
    void testGetCart_Success() {
        // Arrange
        Integer cartId = 1;
        CartEntity cart = new CartEntity();
        when(cartService.getCart(cartId)).thenReturn(cart);

        // Act
        ResponseEntity<CartEntity> response = cartController.getCart(cartId);

        // Assert
        assertEquals(HttpStatus.OK, response.getStatusCode());
        assertEquals(cart, response.getBody());
        verify(cartService, times(1)).getCart(cartId);
    }

    @Test
    void testGetCart_NotFound() {
        // Arrange
        Integer cartId = 1;
        when(cartService.getCart(cartId)).thenThrow(new EntityNotFoundException("Cart not found"));

        // Act
        ResponseEntity<CartEntity> response = cartController.getCart(cartId);

        // Assert
        assertEquals(HttpStatus.NOT_FOUND, response.getStatusCode());
        assertNull(response.getBody());
        verify(cartService, times(1)).getCart(cartId);
    }

    @Test
    void testAddProductToCart_Success() {
        // Arrange
        Integer cartId = 1;
        Integer productId = 1;

        // Act
        ResponseEntity<?> response = cartController.addProductToCart(cartId, productId);

        // Assert
        assertEquals(HttpStatus.OK, response.getStatusCode());
        verify(cartService, times(1)).addProduct(cartId, productId);
    }

    @Test
    void testAddProductToCart_NotFound() {
        // Arrange
        Integer cartId = 1;
        Integer productId = 1;
        doThrow(new EntityNotFoundException("Cart not found"))
                .when(cartService).addProduct(cartId, productId);

        // Act
        ResponseEntity<?> response = cartController.addProductToCart(cartId, productId);

        // Assert
        assertEquals(HttpStatus.NOT_FOUND, response.getStatusCode());
        assertEquals("Cart not found", response.getBody());
        verify(cartService, times(1)).addProduct(cartId, productId);
    }


    @Test
    void testRemoveProductFromCart_Success() {
        // Arrange
        Integer cartId = 1;
        Integer productId = 1;

        // Act
        ResponseEntity<?> response = cartController.removeProductFromCart(cartId, productId);

        // Assert
        assertEquals(HttpStatus.OK, response.getStatusCode());
        verify(cartService, times(1)).removeItem(cartId, productId);
    }

    @Test
    void testRemoveProductFromCart_NotFound() {
        // Arrange
        Integer cartId = 1;
        Integer productId = 1;
        doThrow(new EntityNotFoundException("Cart not found"))
                .when(cartService).removeItem(cartId, productId);

        // Act
        ResponseEntity<?> response = cartController.removeProductFromCart(cartId, productId);

        // Assert
        assertEquals(HttpStatus.BAD_REQUEST, response.getStatusCode());
        assertEquals("Cart not found", response.getBody());
        verify(cartService, times(1)).removeItem(cartId, productId);
    }

    @Test
    void testRemoveProductFromCart_ItemNotInCart() {
        // Arrange
        Integer cartId = 1;
        Integer productId = 1;
        doThrow(new ItemNotInCartException("Product not in cart"))
                .when(cartService).removeItem(cartId, productId);

        // Act
        ResponseEntity<?> response = cartController.removeProductFromCart(cartId, productId);

        // Assert
        assertEquals(HttpStatus.BAD_REQUEST, response.getStatusCode());
        assertEquals("Product not in cart", response.getBody());
        verify(cartService, times(1)).removeItem(cartId, productId);
    }

    @Test
    void testGetOwnerOfCart_Success() {
        // Arrange
        Integer cartId = 1;
        AccountEntity owner = new AccountEntity();
        when(cartService.getOwnerOfCart(cartId)).thenReturn(owner);

        // Act
        ResponseEntity<?> response = cartController.getOwnerOfCart(cartId);

        // Assert
        assertEquals(HttpStatus.OK, response.getStatusCode());
        assertEquals(owner, response.getBody());
        verify(cartService, times(1)).getOwnerOfCart(cartId);
    }

    @Test
    void testGetOwnerOfCart_NotFound() {
        // Arrange
        Integer cartId = 1;
        when(cartService.getOwnerOfCart(cartId)).thenThrow(new EntityNotFoundException("Cart not found"));

        // Act
        ResponseEntity<?> response = cartController.getOwnerOfCart(cartId);

        // Assert
        assertEquals(HttpStatus.NOT_FOUND, response.getStatusCode());
        assertEquals("Cart not found", response.getBody());
        verify(cartService, times(1)).getOwnerOfCart(cartId);
    }

    @Test
    void testGetAllItemsInCart_Success() {
        // Arrange
        Integer cartId = 1;
        List<CartItemEntity> items = new ArrayList<>();
        when(cartService.getAllItemsInCart(cartId)).thenReturn(items);

        // Act
        ResponseEntity<List<CartItemEntity>> response = cartController.getAllItemsInCart(cartId);

        // Assert
        assertEquals(HttpStatus.OK, response.getStatusCode());
        assertEquals(items, response.getBody());
        verify(cartService, times(1)).getAllItemsInCart(cartId);
    }

    @Test
    void testGetAllItemsInCart_NotFound() {
        // Arrange
        Integer cartId = 1;
        when(cartService.getAllItemsInCart(cartId)).thenThrow(new EntityNotFoundException("Cart not found"));

        // Act
        ResponseEntity<List<CartItemEntity>> response = cartController.getAllItemsInCart(cartId);

        // Assert
        assertEquals(HttpStatus.NOT_FOUND, response.getStatusCode());
        assertNull(response.getBody());
        verify(cartService, times(1)).getAllItemsInCart(cartId);
    }

    @Test
    void testCheckout_Success() {
        // Arrange
        Integer cartId = 1;

        // Act
        ResponseEntity<?> response = cartController.checkout(cartId);

        // Assert
        assertEquals(HttpStatus.OK, response.getStatusCode());
        verify(cartService, times(1)).checkout(cartId);
    }

    @Test
    void testCheckout_NotFound() {
        // Arrange
        Integer cartId = 1;
        doThrow(new EntityNotFoundException("Cart not found"))
                .when(cartService).checkout(cartId);

        // Act
        ResponseEntity<?> response = cartController.checkout(cartId);

        // Assert
        assertEquals(HttpStatus.NOT_FOUND, response.getStatusCode());
        assertEquals("Cart not found", response.getBody());
        verify(cartService, times(1)).checkout(cartId);
    }
}
