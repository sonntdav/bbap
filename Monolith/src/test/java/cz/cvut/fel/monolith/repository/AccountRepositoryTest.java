package cz.cvut.fel.monolith.repository;

import cz.cvut.fel.monolith.model.AccountEntity;
import org.junit.jupiter.api.Test;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;

import java.util.Optional;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.*;

@DataJpaTest
public class AccountRepositoryTest {
    @Autowired
    AccountRepository accountRepository;

    @Test
    public void savingAccountInDatabaseIsSameAsTheOneWeLoadFromDatabase(){
        AccountEntity account = getAccount();
        accountRepository.save(account);
        assertTrue(accountRepository.findById(account.getId()).isPresent());
        AccountEntity accountEntity = accountRepository.findById(account.getId()).get();
        assertThat(account)
                .usingRecursiveComparison()
                .isEqualTo(accountEntity);
    }

    @Test
    public void gettingAccountThatDoesNotExistReturnsNothing(){
        AccountEntity account = getAccount();
        account.setId(1);
        assertFalse(accountRepository.findById(account.getId()).isPresent());
    }

    @Test
    public void updatingAccountChangesItInDatabase(){
        final String newName = "New John Doe";
        AccountEntity account = getAccount();
        accountRepository.save(account);
        AccountEntity entityFromDB = accountRepository.findByUsername(account.getUsername());
        entityFromDB.setUsername(newName);
        accountRepository.save(entityFromDB);
        assertTrue(accountRepository.findById(entityFromDB.getId()).isPresent());
        assertEquals(entityFromDB.getUsername(),newName);
    }

    @Test
    public void deletingAccountFromDatabaseRemovesIt(){
        AccountEntity account = getAccount();
        accountRepository.save(account);
        AccountEntity accountFromDB = accountRepository.findByUsername(account.getUsername());
        assertNotNull(accountFromDB);
        accountRepository.delete(accountFromDB);
        accountFromDB = accountRepository.findByUsername(account.getUsername());
        assertNull(accountFromDB);
    }

    private AccountEntity getAccount(){
        return new AccountEntity("John Doe","password123","johndoe@example.com","New York","123 Main Street",1000.00,null);
    }

}
