package cz.cvut.fel.monolith.service;

import cz.cvut.fel.monolith.model.AccountEntity;
import cz.cvut.fel.monolith.model.OrderEntity;
import cz.cvut.fel.monolith.model.PaymentEntity;
import cz.cvut.fel.monolith.repository.PaymentRepository;
import cz.cvut.fel.monolith.service.PaymentService;
import jakarta.persistence.EntityNotFoundException;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.transaction.annotation.Transactional;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertThrows;

@SpringBootTest
@Transactional
public class PaymentServiceIntegrationTest {
    @Autowired
    private PaymentService paymentService;

    @Autowired
    private PaymentRepository paymentRepository;

    @BeforeEach
    void setUp() {
        paymentRepository.deleteAll();
    }

    @Test
    public void getPayment_existingPayment_returnsPayment() {
        // Arrange
        PaymentEntity payment = createPayment();
        paymentRepository.save(payment);

        // Act
        PaymentEntity result = paymentService.getPayment(payment.getPaymentID());

        // Assert
        assertThat(result).isNotNull();
        assertThat(result).isEqualTo(payment);
    }

    @Test
    public void getPayment_nonExistingPayment_throwsEntityNotFoundException() {
        // Arrange
        Integer paymentId = 1;

        // Act & Assert
        assertThrows(EntityNotFoundException.class, () -> paymentService.getPayment(paymentId));
    }

    @Test
    public void getAccount_existingPayment_returnsPaymentAccount() {
        // Arrange
        PaymentEntity payment = createPayment();
        paymentRepository.save(payment);

        // Act
        AccountEntity result = paymentService.getAccount(payment.getPaymentID());

        // Assert
        assertThat(result).isNotNull();
        assertThat(result).isEqualTo(payment.getOrderEntity().getAccount());
    }

    @Test
    public void getAccount_nonExistingPayment_throwsEntityNotFoundException() {
        // Arrange
        Integer paymentId = 1;

        // Act & Assert
        assertThrows(EntityNotFoundException.class, () -> paymentService.getAccount(paymentId));
    }

    @Test
    public void getOrder_existingPayment_returnsPaymentOrder() {
        // Arrange
        PaymentEntity payment = createPayment();
        paymentRepository.save(payment);

        // Act
        OrderEntity result = paymentService.getOrder(payment.getPaymentID());

        // Assert
        assertThat(result).isNotNull();
        assertThat(result).isEqualTo(payment.getOrderEntity());
    }

    @Test
    public void getOrder_nonExistingPayment_throwsEntityNotFoundException() {
        // Arrange
        Integer paymentId = 1;

        // Act & Assert
        assertThrows(EntityNotFoundException.class, () -> paymentService.getOrder(paymentId));
    }

    private PaymentEntity createPayment() {
        PaymentEntity payment = new PaymentEntity();
        OrderEntity order = new OrderEntity();
        AccountEntity account = new AccountEntity();

        payment.setOrderEntity(order);
        order.setAccount(account);

        return payment;
    }
}

