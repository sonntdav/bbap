package cz.cvut.fel.monolith.controller;

import cz.cvut.fel.monolith.controller.WishlistController;
import cz.cvut.fel.monolith.model.ProductEntity;
import cz.cvut.fel.monolith.service.WishlistService;
import jakarta.persistence.EntityNotFoundException;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

class WishlistControllerMockitoTest {
    @Mock
    private WishlistService wishlistService;

    @InjectMocks
    private WishlistController wishlistController;

    @BeforeEach
    void setUp() {
        MockitoAnnotations.openMocks(this);
    }


    @Test
    void testAddItemToWishlist_Success() {
        // Arrange
        Integer wishlistId = 1;
        Integer itemId = 1;

        // Act
        ResponseEntity<?> response = wishlistController.addItemToWishlist(wishlistId, itemId);

        // Assert
        assertEquals(HttpStatus.OK, response.getStatusCode());
        verify(wishlistService, times(1)).addItemToWishlist(wishlistId, itemId);
    }

    @Test
    void testAddItemToWishlist_NotFound() {
        // Arrange
        Integer wishlistId = 1;
        Integer itemId = 1;
        doThrow(new EntityNotFoundException("Wishlist or item not found"))
                .when(wishlistService).addItemToWishlist(wishlistId, itemId);

        // Act
        ResponseEntity<?> response = wishlistController.addItemToWishlist(wishlistId, itemId);

        // Assert
        assertEquals(HttpStatus.NOT_FOUND, response.getStatusCode());
        assertEquals("Wishlist or item not found", response.getBody());
        verify(wishlistService, times(1)).addItemToWishlist(wishlistId, itemId);
    }

    @Test
    void testRemoveItemFromWishlist_Success() {
        // Arrange
        Integer wishlistId = 1;
        Integer itemId = 1;

        // Act
        ResponseEntity<?> response = wishlistController.removeItemFromWishlist(wishlistId, itemId);

        // Assert
        assertEquals(HttpStatus.OK, response.getStatusCode());
        verify(wishlistService, times(1)).removeItemFromWishlist(wishlistId, itemId);
    }

    @Test
    void testRemoveItemFromWishlist_NotFound() {
        // Arrange
        Integer wishlistId = 1;
        Integer itemId = 1;
        doThrow(new EntityNotFoundException("Wishlist or item not found"))
                .when(wishlistService).removeItemFromWishlist(wishlistId, itemId);

        // Act
        ResponseEntity<?> response = wishlistController.removeItemFromWishlist(wishlistId, itemId);

        // Assert
        assertEquals(HttpStatus.NOT_FOUND, response.getStatusCode());
        assertEquals("Wishlist or item not found", response.getBody());
        verify(wishlistService, times(1)).removeItemFromWishlist(wishlistId, itemId);
    }
}
