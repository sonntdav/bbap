package cz.cvut.fel.monolith.service;

import cz.cvut.fel.monolith.exception.AccountAlreadyExistsException;
import cz.cvut.fel.monolith.model.ProductEntity;
import cz.cvut.fel.monolith.repository.ProductRepository;
import cz.cvut.fel.monolith.service.impl.ProductServiceImpl;
import jakarta.persistence.EntityNotFoundException;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

class ProductServiceMockitoTest {
    @Mock
    private ProductRepository productRepository;

    @InjectMocks
    private ProductServiceImpl productService;

    @BeforeEach
    void setUp() {
        MockitoAnnotations.openMocks(this);
    }

    @Test
    void getProduct_existingProduct_returnsProduct() {
        // Arrange
        Integer productId = 1;
        ProductEntity expectedProduct = new ProductEntity();
        expectedProduct.setProductID(productId);
        when(productRepository.findById(productId)).thenReturn(Optional.of(expectedProduct));

        // Act
        ProductEntity actualProduct = productService.getProduct(productId);

        // Assert
        assertEquals(expectedProduct, actualProduct);
        verify(productRepository, times(1)).findById(productId);
    }

    @Test
    void getProduct_nonExistingProduct_throwsEntityNotFoundException() {
        // Arrange
        Integer productId = 1;
        when(productRepository.findById(productId)).thenReturn(Optional.empty());

        // Act & Assert
        assertThrows(EntityNotFoundException.class, () -> productService.getProduct(productId));
        verify(productRepository, times(1)).findById(productId);
    }

    @Test
    void createProduct_newProduct_savesProduct() {
        // Arrange
        ProductEntity product = new ProductEntity();
        when(productRepository.findById(any())).thenReturn(Optional.empty());

        // Act
        productService.createProduct(product);

        // Assert
        verify(productRepository, times(1)).save(product);
    }

    @Test
    void createProduct_existingProduct_throwsAccountAlreadyExistsException() {
        // Arrange
        ProductEntity product = new ProductEntity();
        product.setProductID(1);
        when(productRepository.findById(any())).thenReturn(Optional.of(product));

        // Act & Assert
        assertThrows(AccountAlreadyExistsException.class, () -> productService.createProduct(product));
        verify(productRepository, never()).save(product);
    }

    @Test
    void updateProduct_existingProduct_savesUpdatedProduct() {
        // Arrange
        ProductEntity originalProduct = new ProductEntity();
        originalProduct.setProductID(1);
        ProductEntity updatedProduct = new ProductEntity();
        updatedProduct.setProductID(1);
        when(productRepository.findById(1)).thenReturn(Optional.of(originalProduct));

        // Act
        productService.updateProduct(updatedProduct);

        // Assert
        verify(productRepository, times(1)).save(updatedProduct);
    }

    @Test
    void updateProduct_nonExistingProduct_throwsEntityNotFoundException() {
        // Arrange
        ProductEntity product = new ProductEntity();
        product.setProductID(1);
        when(productRepository.findById(1)).thenReturn(Optional.empty());

        // Act & Assert
        assertThrows(EntityNotFoundException.class, () -> productService.updateProduct(product));
        verify(productRepository, never()).save(product);
    }
    @Test
    void deleteProduct_existingProduct_deletesProduct() {
        // Arrange
        Integer productId = 1;
        ProductEntity product = new ProductEntity();
        product.setProductID(productId);
        when(productRepository.findById(productId)).thenReturn(Optional.of(product));

        // Act
        productService.deleteProduct(productId);

        // Assert
        verify(productRepository, times(1)).delete(product);
    }

    @Test
    void deleteProduct_nonExistingProduct_throwsEntityNotFoundException() {
        // Arrange
        Integer productId = 1;
        when(productRepository.findById(productId)).thenReturn(Optional.empty());

        // Act & Assert
        assertThrows(EntityNotFoundException.class, () -> productService.deleteProduct(productId));
        verify(productRepository, never()).delete(any());
    }

    @Test
    void getListOfAllProducts_returnsListOfProducts() {
        // Arrange
        List<ProductEntity> expectedProducts = new ArrayList<>();
        expectedProducts.add(new ProductEntity());
        when(productRepository.findAll()).thenReturn(expectedProducts);

        // Act
        List<ProductEntity> actualProducts = productService.getListOfAllProducts();

        // Assert
        assertEquals(expectedProducts, actualProducts);
        verify(productRepository, times(1)).findAll();
    }
}