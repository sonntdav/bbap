package cz.cvut.fel.monolith.service;


import cz.cvut.fel.monolith.model.AccountEntity;
import cz.cvut.fel.monolith.model.ProductEntity;
import cz.cvut.fel.monolith.model.ReviewEntity;
import cz.cvut.fel.monolith.repository.ReviewRepository;
import cz.cvut.fel.monolith.service.AccountService;
import cz.cvut.fel.monolith.service.ProductService;
import cz.cvut.fel.monolith.service.ReviewService;
import jakarta.persistence.EntityNotFoundException;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertThrows;

@SpringBootTest
@Transactional
public class ReviewServiceIntegrationTest {
    @Autowired
    private ReviewService reviewService;

    @Autowired
    private ReviewRepository reviewRepository;

    @Autowired
    private ProductService productService;

    @Autowired
    private AccountService accountService;

    @BeforeEach
    void setUp() {
        reviewRepository.deleteAll();
    }

    @Test
    public void getReview_existingReview_returnsReview() {
        // Arrange
        ReviewEntity review = createReview();
        reviewRepository.save(review);

        // Act
        ReviewEntity result = reviewService.getReview(review.getReviewID());

        // Assert
        assertThat(result).isNotNull();
        assertThat(result).isEqualTo(review);
    }

    @Test
    public void getReview_nonExistingReview_throwsEntityNotFoundException() {
        // Arrange
        Integer reviewId = 1;

        // Act & Assert
        assertThrows(EntityNotFoundException.class, () -> reviewService.getReview(reviewId));
    }

    @Test
    public void getAllReviews_existingProduct_returnsReviews() {
        // Arrange
        ProductEntity product = createProduct();
        productService.createProduct(product);

        AccountEntity account = createAccount();

        accountService.createAccount(account);


        reviewService.addReview(product.getProductID(), account.getId(), "reviewMessage1");
        reviewService.addReview(product.getProductID(), account.getId(), "reviewMessage2");
        // Act
        List<ReviewEntity> result = reviewService.getAllReviews(product.getProductID());

        // Assert
        assertThat(result).hasSize(2);
    }

    @Test
    public void getAllReviews_nonExistingProduct_throwsEntityNotFoundException() {
        // Arrange
        Integer productId = 1;

        // Act & Assert
        assertThrows(EntityNotFoundException.class, () -> reviewService.getAllReviews(productId));
    }

    @Test
    public void addReview_existingProductAndUser_addsReview() {
        // Arrange
        AccountEntity account = createAccount();
        accountService.createAccount(account);

        ProductEntity product = createProduct();
        productService.createProduct(product);

        String reviewMessage = "This is a review.";

        // Act
        reviewService.addReview(product.getProductID(), account.getId(), reviewMessage);

        // Assert
        List<ReviewEntity> reviews = reviewService.getAllReviews(product.getProductID());
        assertThat(reviews).hasSize(1);
        assertThat(reviews.get(0).getAuthor()).isEqualTo(account);
        assertThat(reviews.get(0).getProduct()).isEqualTo(product);
        assertThat(reviews.get(0).getMessage()).isEqualTo(reviewMessage);
    }

    @Test
    public void addReview_nonExistingProduct_throwsEntityNotFoundException() {
        // Arrange
        Integer productId = 1;
        Integer userId = 1;
        String reviewMessage = "This is a review.";

        // Act & Assert
        assertThrows(EntityNotFoundException.class, () -> reviewService.addReview(productId, userId, reviewMessage));
    }

    @Test
    public void addReview_nonExistingUser_throwsEntityNotFoundException() {
        // Arrange
        ProductEntity product = createProduct();
        productService.createProduct(product);

        Integer userId = 1;
        String reviewMessage = "This is a review.";

        // Act & Assert
        assertThrows(EntityNotFoundException.class, () -> reviewService.addReview(product.getProductID(), userId, reviewMessage));
    }

    @Test
    public void updateReview_existingReview_updatesReview() {
        // Arrange
        ReviewEntity review = createReview();
        reviewRepository.save(review);

        String newReviewMessage = "Updated review message.";

        // Act
        review.setMessage(newReviewMessage);
        reviewService.updateReview(review);

        // Assert
        ReviewEntity updatedReview = reviewService.getReview(review.getReviewID());
        assertThat(updatedReview.getMessage()).isEqualTo(newReviewMessage);
    }

    @Test
    public void removeReview_existingReview_removesReview() {
        // Arrange
        ReviewEntity review = createReview();
        reviewRepository.save(review);

        // Act
        reviewService.removeReview(review.getReviewID());

        // Assert
        assertThrows(EntityNotFoundException.class, () -> reviewService.getReview(review.getReviewID()));
    }

    @Test
    public void removeReview_nonExistingReview_throwsEntityNotFoundException() {
        // Arrange
        Integer reviewId = 1;

        // Act & Assert
        assertThrows(EntityNotFoundException.class, () -> reviewService.removeReview(reviewId));
    }

    private ReviewEntity createReview() {
        ReviewEntity review = new ReviewEntity();
        review.setMessage("Test review");
        return review;
    }

    private ProductEntity createProduct() {
        ProductEntity product = new ProductEntity();
        product.setName("Test Product");
        product.setDescription("Test product description");
        product.setPrice(9.99);
        return product;
    }

    private AccountEntity createAccount() {
        AccountEntity account = new AccountEntity();
        account.setUsername("TestUser");
        account.setEmail("testuser@example.com");
        return account;
    }
}

