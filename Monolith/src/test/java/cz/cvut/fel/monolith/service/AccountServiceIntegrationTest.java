package cz.cvut.fel.monolith.service;

import cz.cvut.fel.monolith.model.AccountEntity;
import cz.cvut.fel.monolith.repository.AccountRepository;
import org.checkerframework.checker.units.qual.A;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.transaction.annotation.Transactional;



import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

@SpringBootTest
@Transactional
public class AccountServiceIntegrationTest {
    @Autowired
    private AccountService accountService;

    @Test
    public void creatingAccountSavesCorrectlyInDatabase(){
        AccountEntity account = getAccount();
        accountService.createAccount(account);
        AccountEntity result = accountService.getAccount(account.getUsername());
        assertThat(account)
                .usingRecursiveComparison()
                .isEqualTo(result);
    }

    @Test
    public void gettingAccountThatDoesNotExistResultsInError(){
        AccountEntity account = getAccount();
        AccountEntity result = accountService.getAccount(account.getUsername());
        assertNull(result);
    }

    @Test
    public void updatingAccountSavesItCorrectlyInDatabase(){
        AccountEntity account = getAccount();
        accountService.createAccount(account);

        String newUsername = "New username";
        String newStreet = "New street";
        String newCity = "New city";
        account.setStreet(newStreet);
        account.setCityName(newCity);
        account.setUsername(newUsername);

        accountService.updateAccount(account.getId(),account);

        AccountEntity result = accountService.getAccount(account.getId());
        assertThat(account)
                .usingRecursiveComparison()
                .isEqualTo(result);
    }

    @Test
    public void deletingAccountRemovesItFromDB(){
        AccountEntity account = getAccount();
        accountService.createAccount(account);

        assertNotNull(accountService.getAccount(account.getId()));
        accountService.deleteAccount(account.getId());
        assertNull(accountService.getAccount(account.getUsername()));
    }

    @Test
    public void addingAndWithdrawingLeavesCorrectBalance(){
        AccountEntity account = getAccount();
        accountService.createAccount(account);

        accountService.addFunds(account.getId(),500.00);
        assertEquals(1500,accountService.getAccountBalance(account.getId()));

        accountService.withdrawBalance(account.getId(),700.00);
        assertEquals(800,accountService.getAccountBalance(account.getId()));

    }


    private AccountEntity getAccount(){
        return new AccountEntity("John Doe","password123","johndoe@example.com","New York","\"123 Main Street",1000.00);
    }

}
