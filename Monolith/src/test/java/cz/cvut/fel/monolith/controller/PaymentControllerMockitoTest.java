package cz.cvut.fel.monolith.controller;

import cz.cvut.fel.monolith.model.AccountEntity;
import cz.cvut.fel.monolith.model.OrderEntity;
import cz.cvut.fel.monolith.model.PaymentEntity;
import cz.cvut.fel.monolith.service.PaymentService;
import jakarta.persistence.EntityNotFoundException;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.*;

class PaymentControllerMockitoTest {
    @Mock
    private PaymentService paymentService;

    @InjectMocks
    private PaymentController paymentController;

    @BeforeEach
    void setUp() {
        MockitoAnnotations.openMocks(this);
    }

    @Test
    void testGetPayment_Success() {
        PaymentEntity paymentEntity = new PaymentEntity();
        when(paymentService.getPayment(1)).thenReturn(paymentEntity);

        ResponseEntity<PaymentEntity> response = paymentController.getPayment(1);

        assertEquals(HttpStatus.OK, response.getStatusCode());
        assertEquals(paymentEntity, response.getBody());
        verify(paymentService, times(1)).getPayment(1);
    }

    @Test
    void testGetPayment_NotFound() {
        when(paymentService.getPayment(1)).thenThrow(new EntityNotFoundException("Payment not found"));

        ResponseEntity<PaymentEntity> response = paymentController.getPayment(1);

        assertEquals(HttpStatus.NOT_FOUND, response.getStatusCode());
        assertEquals(null, response.getBody());
        verify(paymentService, times(1)).getPayment(1);
    }
}
