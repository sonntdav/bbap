package cz.cvut.fel.monolith.end_to_end;

import cz.cvut.fel.monolith.model.*;
import cz.cvut.fel.monolith.service.OrderService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Order;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.boot.test.web.server.LocalServerPort;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.ResponseStatus;

import java.util.Objects;

import static org.junit.jupiter.api.Assertions.assertEquals;

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@Transactional
public class EndToEndTests {

    @LocalServerPort
    private int port;

    @Autowired
    private TestRestTemplate restTemplate;

    private String accountUrl;
    private String cartUrl;
    private String orderUrl;
    private String paymentUrl;
    private String productUrl;
    private String reviewUrl;
    private String wishlistUrl;

    @BeforeEach
    public void setUp() {
        accountUrl = "http://localhost:" + port + "/account";
        cartUrl = "http://localhost:" + port +"/cart";
        orderUrl = "http://localhost:" + port +"/order";
        paymentUrl = "http://localhost:" + port +"/payment";
        productUrl = "http://localhost:" + port +"/product";
        reviewUrl = "http://localhost:" + port +"/review";
        wishlistUrl = "http://localhost:" + port +"/wishlist";
    }
    @Test
    public void endToEndTest(){
        AccountEntity account = createUser();

        ResponseEntity<Void> createAccountResponse = restTemplate.postForEntity(accountUrl, account, Void.class);
        assertEquals(HttpStatus.CREATED, createAccountResponse.getStatusCode());

        ResponseEntity<AccountEntity> getAccountResponse = restTemplate.getForEntity(accountUrl + "/" + account.getId(), AccountEntity.class);
        AccountEntity userFromDB = getAccountResponse.getBody();
        assertEquals(Objects.requireNonNull(userFromDB).getUsername(),account.getUsername());

        ProductEntity product = createProduct();
        ProductEntity product2 = createProduct();
        product2.setName(product2.getName() + "2");
        ResponseEntity<Void> createProductResponse = restTemplate.postForEntity(productUrl,product,Void.class);
        ResponseEntity<Void> createProductResponse2 = restTemplate.postForEntity(productUrl,product2,Void.class);

        assertEquals(HttpStatus.CREATED, createProductResponse.getStatusCode());
        assertEquals(HttpStatus.CREATED, createProductResponse2.getStatusCode());

        product = restTemplate.getForEntity(productUrl + "/byName/" + product.getName(),ProductEntity.class).getBody();
        product2 = restTemplate.getForEntity(productUrl + "/byName/" + product2.getName(),ProductEntity.class).getBody();

        assert product != null;
        assert product2 != null;

        CartEntity cart = userFromDB.getCartEntity();

        ResponseEntity<Void> addProduct1ToCart = restTemplate.postForEntity(cartUrl + "/" + cart.getCartId() + "/products/" + product.getProductID(),product,Void.class);
        assertEquals(HttpStatus.OK, addProduct1ToCart.getStatusCode());
        ResponseEntity<Void> addProduct2ToCart = restTemplate.postForEntity(cartUrl + "/" + cart.getCartId() + "/products/" + product2.getProductID(),product,Void.class);
        assertEquals(HttpStatus.OK, addProduct2ToCart.getStatusCode());
        ResponseEntity<Void> addProduct2ToCart2 = restTemplate.postForEntity(cartUrl + "/" + cart.getCartId() + "/products/" + product2.getProductID(),product,Void.class);
        assertEquals(HttpStatus.OK, addProduct2ToCart2.getStatusCode());

        cart = restTemplate.getForEntity(cartUrl + "/" + cart.getCartId(),CartEntity.class).getBody();
        assert cart != null;
        assertEquals(cart.getCartItemEntities().size(),2);

        ResponseEntity<Void> checkoutResult = restTemplate.postForEntity(cartUrl + "/" + cart.getCartId() + "/checkout",account,Void.class);
        assertEquals(HttpStatus.OK, checkoutResult.getStatusCode());

        cart = restTemplate.getForEntity(cartUrl + "/" + cart.getCartId(),CartEntity.class).getBody();
        assert cart != null;
        assertEquals(0,cart.getCartItemEntities().size());

        account = restTemplate.getForEntity(accountUrl + "/" + account.getId(), AccountEntity.class).getBody();
        assert account != null;
        assertEquals(account.getOrders().size(),1);
        OrderEntity order = account.getOrders().get(0);
        OrderItemEntity orderItemEntity = order.getOrderItems().get(0);
        assertEquals(orderItemEntity.getAmount(),1);
        OrderItemEntity orderItemEntity2 = order.getOrderItems().get(1);
        assertEquals(orderItemEntity2.getAmount(),2);


        ResponseEntity<Void> payForOrderResult = restTemplate.postForEntity(orderUrl + "/" + order.getOrderID() + "/pay",account,Void.class);
        assertEquals(HttpStatus.BAD_REQUEST, payForOrderResult.getStatusCode());


        double curBalance = account.getBalance();
        double addedAmount = 10000.0;
        double priceOfOrder = 0;
        for (OrderItemEntity orderItem : account.getOrders().get(0).getOrderItems()) {
            priceOfOrder+= orderItem.getAmount() * orderItem.getProductEntity().getPrice();
        }


        ResponseEntity<Void> addFundsToAccount = restTemplate.postForEntity(accountUrl + "/" + account.getId() + "/addFunds/" + addedAmount,account,Void.class);
        assertEquals(HttpStatus.OK, addFundsToAccount.getStatusCode());
        payForOrderResult = restTemplate.postForEntity(orderUrl + "/" + order.getOrderID() + "/pay",account,Void.class);
        assertEquals(HttpStatus.OK, payForOrderResult.getStatusCode());
        account = restTemplate.getForEntity(accountUrl + "/" + account.getId(), AccountEntity.class).getBody();
        assert account != null;
        assertEquals(curBalance + addedAmount - priceOfOrder, account.getBalance());

        ResponseEntity<OrderEntity> getOrderResponse = restTemplate.getForEntity(orderUrl +"/" + order.getOrderID(),OrderEntity.class);
        assertEquals(HttpStatus.OK, getOrderResponse.getStatusCode());
        order = getOrderResponse.getBody();
        assert order != null;
        assertEquals(OrderStatus.IN_PROGRESS,order.getOrderStatus());

        ResponseEntity<Void> finishOrder = restTemplate.postForEntity(orderUrl + "/" + order.getOrderID() + "/finish",order,Void.class);
        assertEquals(HttpStatus.OK,finishOrder.getStatusCode());

        getOrderResponse = restTemplate.getForEntity(orderUrl +"/" + order.getOrderID(),OrderEntity.class);
        assertEquals(HttpStatus.OK, getOrderResponse.getStatusCode());
        order = getOrderResponse.getBody();
        assert order != null;
        assertEquals(OrderStatus.COMPLETED,order.getOrderStatus());

        //review test
        String reviewMessage = "test review message";
        ResponseEntity<Void>  createReviewResponse = restTemplate.postForEntity(reviewUrl + "/" + product.getProductID() + "/" + account.getId(),reviewMessage,Void.class);
        assertEquals(HttpStatus.CREATED,createReviewResponse.getStatusCode());

        product = restTemplate.getForEntity(productUrl + "/byName/" + product.getName(),ProductEntity.class).getBody();
        assert product != null;
        assertEquals(1,product.getReviews().size());
        ReviewEntity review = product.getReviews().get(0);
        assertEquals(reviewMessage,review.getMessage());
        ResponseEntity<AccountEntity>  reviewAuthorResponse = restTemplate.getForEntity(reviewUrl + "/" + review.getReviewID() + "/author",AccountEntity.class);
        assertEquals(account,reviewAuthorResponse.getBody());

        //wishlist test
        WishlistEntity wishlist = account.getWishlist();
        ResponseEntity<Void> addItemToWishlistResult = restTemplate.postForEntity(wishlistUrl + "/" + wishlist.getWishlistID() + "/items/" + product.getProductID(),review,Void.class);
        assertEquals(HttpStatus.OK,addItemToWishlistResult.getStatusCode());

        ResponseEntity<WishlistEntity> getWishlistResult = restTemplate.getForEntity(wishlistUrl +"/" + wishlist.getWishlistID(),WishlistEntity.class);
        assertEquals(HttpStatus.OK,getWishlistResult.getStatusCode());
        WishlistEntity wishlistFromDB = getWishlistResult.getBody();
        assert wishlistFromDB != null;
        assertEquals(wishlistFromDB.getProductsInWishlist().size(),1);
        assertEquals(wishlistFromDB.getProductsInWishlist().get(0),product);

    }
    private AccountEntity createUser(){
        AccountEntity account = new AccountEntity();
        account.setId(1);
        account.setUsername("john_doe");
        account.setPassword("password123");
        account.setEmail("johndoe@example.com");
        account.setCityName("City");
        account.setStreet("Street");
        account.setBalance(1000.0);

         return account;
    }

    private ProductEntity createProduct(){
         ProductEntity product = new ProductEntity();
         product.setName("testProduct");
         product.setPrice(1000.00);
         product.setDescription("testDescription");
         return product;
    }
}
