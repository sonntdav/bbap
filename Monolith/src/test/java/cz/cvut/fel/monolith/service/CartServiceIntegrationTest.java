package cz.cvut.fel.monolith.service;

import cz.cvut.fel.monolith.exception.ItemNotInCartException;
import cz.cvut.fel.monolith.model.*;
import cz.cvut.fel.monolith.repository.CartRepository;
import cz.cvut.fel.monolith.service.CartService;
import cz.cvut.fel.monolith.service.OrderService;
import cz.cvut.fel.monolith.service.ProductService;
import jakarta.persistence.EntityNotFoundException;
import org.checkerframework.checker.units.qual.A;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.*;

@SpringBootTest
@Transactional
public class CartServiceIntegrationTest {
    @Autowired
    private AccountService accountService;

    @Autowired
    private CartService cartService;

    @Autowired
    private CartRepository cartRepository;

    @Autowired
    private ProductService productService;

    @Autowired
    private OrderService orderService;

    @BeforeEach
    void setUp() {
        cartRepository.deleteAll();
    }

    @Test
    public void getCart_existingCart_returnsCart() {
        // Arrange
        CartEntity cart = createCart();
        cartRepository.save(cart);

        // Act
        CartEntity result = cartService.getCart(cart.getCartId());

        // Assert
        assertThat(result).isNotNull();
        assertThat(result).isEqualTo(cart);
    }

    @Test
    public void getCart_nonExistingCart_throwsEntityNotFoundException() {
        // Arrange
        Integer cartId = 1;

        // Act & Assert
        assertThrows(EntityNotFoundException.class, () -> cartService.getCart(cartId));
    }

    @Test
    public void addProduct_validCartAndProduct_addsProductToCart() {
        // Arrange
        CartEntity cart = createCart();
        cartRepository.save(cart);

        ProductEntity product = createProduct();
        productService.createProduct(product);

        // Act
        cartService.addProduct(cart.getCartId(), product.getProductID());

        // Assert
        CartEntity result = cartService.getCart(cart.getCartId());
        assertThat(result.getCartItemEntities()).hasSize(1);
        assertThat(result.getCartItemEntities().get(0).getProductEntity()).isEqualTo(product);
    }

    @Test
    public void addProduct_nonExistingCart_throwsEntityNotFoundException() {
        // Arrange
        Integer cartId = 1;
        ProductEntity product = createProduct();
        productService.createProduct(product);

        // Act & Assert
        assertThrows(EntityNotFoundException.class, () -> cartService.addProduct(cartId, product.getProductID()));
    }

    @Test
    public void addProduct_nonExistingProduct_throwsEntityNotFoundException() {
        // Arrange
        CartEntity cart = createCart();
        cartRepository.save(cart);
        Integer productId = 1;

        // Act & Assert
        assertThrows(EntityNotFoundException.class, () -> cartService.addProduct(cart.getCartId(), productId));
    }

//    @Test
//    public void addProduct_productAlreadyInCart_increasesQuantity() {
//        // Arrange
//        accountService.createAccount(new AccountEntity("myName","myPassword","MyEmail","myCityName","myStreet",1000.0));
//
//        CartEntity myCart = accountService.getAccount("myName").getCartEntity();
//
//        ProductEntity product = createProduct();
//
//        productService.createProduct(product);
//
//        cartService.addProduct(myCart.getCartId(),product.getProductID());
//        cartService.addProduct(myCart.getCartId(),product.getProductID());
//
//        // Assert
//        CartEntity result = cartService.getCart(myCart.getCartId());
//        assertThat(result.getCartItemEntities()).hasSize(1);
//        assertThat(result.getCartItemEntities().get(0).getAmount()).isEqualTo(2);
//    }

    @Test
    public void increaseQuantity_existingCartAndProduct_increasesQuantity() {
        // Arrange
        CartEntity cart = createCart();
        cartRepository.save(cart);

        ProductEntity product = createProduct();
        productService.createProduct(product);

        cartService.addProduct(cart.getCartId(), product.getProductID());

        // Act
        cartService.increaseQuantity(cart.getCartId(), product.getProductID());

        // Assert
        CartEntity result = cartService.getCart(cart.getCartId());
        assertThat(result.getCartItemEntities()).hasSize(1);
        assertThat(result.getCartItemEntities().get(0).getAmount()).isEqualTo(2);
    }

    @Test
    public void increaseQuantity_nonExistingCart_throwsEntityNotFoundException() {
        // Arrange
        Integer cartId = 1;
        Integer productId = 1;

        // Act & Assert
        assertThrows(EntityNotFoundException.class, () -> cartService.increaseQuantity(cartId, productId));
    }

    @Test
    public void increaseQuantity_nonExistingProduct_throwsEntityNotFoundException() {
        // Arrange
        CartEntity cart = createCart();
        cartRepository.save(cart);
        Integer productId = 1;

        // Act & Assert
        assertThrows(EntityNotFoundException.class, () -> cartService.increaseQuantity(cart.getCartId(), productId));
    }

    @Test
    public void increaseQuantity_productNotInCart_throwsItemNotInCartException() {
        // Arrange
        CartEntity cart = createCart();
        cartRepository.save(cart);

        ProductEntity product1 = createProduct();
        productService.createProduct(product1);

        ProductEntity product2 = createProduct();
        productService.createProduct(product2);

        cartService.addProduct(cart.getCartId(), product1.getProductID());

        // Act & Assert
        assertThrows(ItemNotInCartException.class, () -> cartService.increaseQuantity(cart.getCartId(), product2.getProductID()));
    }

    @Test
    public void decreaseQuantity_existingCartAndProduct_decreasesQuantity() {
        // Arrange
        CartEntity cart = createCart();
        cartRepository.save(cart);

        ProductEntity product = createProduct();
        productService.createProduct(product);

        cartService.addProduct(cart.getCartId(), product.getProductID());

        // Act
        cartService.decreaseQuantity(cart.getCartId(), product.getProductID());

        // Assert
        CartEntity result = cartService.getCart(cart.getCartId());
        assertThat(result.getCartItemEntities()).hasSize(0);
    }

    @Test
    public void decreaseQuantity_nonExistingCart_throwsEntityNotFoundException() {
        // Arrange
        Integer cartId = 1;
        Integer productId = 1;

        // Act & Assert
        assertThrows(EntityNotFoundException.class, () -> cartService.decreaseQuantity(cartId, productId));
    }

    @Test
    public void decreaseQuantity_nonExistingProduct_throwsEntityNotFoundException() {
        // Arrange
        CartEntity cart = createCart();
        cartRepository.save(cart);
        Integer productId = 1;

        // Act & Assert
        assertThrows(EntityNotFoundException.class, () -> cartService.decreaseQuantity(cart.getCartId(), productId));
    }

    @Test
    public void decreaseQuantity_productNotInCart_throwsItemNotInCartException() {
        // Arrange
        CartEntity cart = createCart();
        cartRepository.save(cart);

        ProductEntity product1 = createProduct();
        productService.createProduct(product1);

        ProductEntity product2 = createProduct();
        productService.createProduct(product2);

        cartService.addProduct(cart.getCartId(), product1.getProductID());

        // Act & Assert
        assertThrows(ItemNotInCartException.class, () -> cartService.decreaseQuantity(cart.getCartId(), product2.getProductID()));
    }

    @Test
    public void removeItem_existingCartAndProduct_removesItemFromCart() {
        // Arrange
        CartEntity cart = createCart();
        cartRepository.save(cart);

        ProductEntity product = createProduct();
        productService.createProduct(product);

        cartService.addProduct(cart.getCartId(), product.getProductID());

        // Act
        cartService.removeItem(cart.getCartId(), product.getProductID());

        // Assert
        CartEntity result = cartService.getCart(cart.getCartId());
        assertThat(result.getCartItemEntities()).isEmpty();
    }

    @Test
    public void removeItem_nonExistingCart_throwsEntityNotFoundException() {
        // Arrange
        Integer cartId = 1;
        Integer productId = 1;

        // Act & Assert
        assertThrows(EntityNotFoundException.class, () -> cartService.removeItem(cartId, productId));
    }

    @Test
    public void removeItem_nonExistingProduct_throwsEntityNotFoundException() {
        // Arrange
        CartEntity cart = createCart();
        cartRepository.save(cart);
        Integer productId = 1;

        // Act & Assert
        assertThrows(EntityNotFoundException.class, () -> cartService.removeItem(cart.getCartId(), productId));
    }

    @Test
    public void removeItem_productNotInCart_throwsItemNotInCartException() {
        // Arrange
        CartEntity cart = createCart();
        cartRepository.save(cart);

        ProductEntity product1 = createProduct();
        productService.createProduct(product1);

        ProductEntity product2 = createProduct();
        productService.createProduct(product2);

        cartService.addProduct(cart.getCartId(), product1.getProductID());

        // Act & Assert
        assertThrows(ItemNotInCartException.class, () -> cartService.removeItem(cart.getCartId(), product2.getProductID()));
    }

    @Test
    public void getOwnerOfCart_existingCart_returnsCartOwner() {
        // Arrange
        CartEntity cart = createCart();
        cartRepository.save(cart);

        // Act
        AccountEntity result = cartService.getOwnerOfCart(cart.getCartId());

        // Assert
        assertThat(result).isNotNull();
        assertThat(result).isEqualTo(cart.getAccount());
    }

    @Test
    public void getOwnerOfCart_nonExistingCart_throwsEntityNotFoundException() {
        // Arrange
        Integer cartId = 1;

        // Act & Assert
        assertThrows(EntityNotFoundException.class, () -> cartService.getOwnerOfCart(cartId));
    }

    @Test
    public void getAllItemsInCart_existingCart_returnsItemsInCart() {
        // Arrange
        CartEntity cart = createCart();
        cartRepository.save(cart);

        ProductEntity product1 = createProduct();
        productService.createProduct(product1);

        ProductEntity product2 = createProduct();
        productService.createProduct(product2);

        cartService.addProduct(cart.getCartId(), product1.getProductID());
        cartService.addProduct(cart.getCartId(), product2.getProductID());

        // Act
        List<CartItemEntity> result = cartService.getAllItemsInCart(cart.getCartId());

        // Assert
        assertThat(result).hasSize(2);
        assertThat(result).extracting(CartItemEntity::getProductEntity).contains(product1, product2);
    }

    @Test
    public void getAllItemsInCart_nonExistingCart_throwsEntityNotFoundException() {
        // Arrange
        Integer cartId = 1;

        // Act & Assert
        assertThrows(EntityNotFoundException.class, () -> cartService.getAllItemsInCart(cartId));
    }

//    @Test
//    public void checkout_existingCart_createsOrderWithCartItemsAndAssociatesWithAccount() {
//        // Arrange
//        CartEntity cart = createCart();
//        cartRepository.save(cart);
//
//        ProductEntity product1 = createProduct();
//        productService.createProduct(product1);
//
//        ProductEntity product2 = createProduct();
//        product2.setName("newName");
//        productService.createProduct(product2);
//
//        cartService.addProduct(cart.getCartId(), product1.getProductID());
//        cartService.addProduct(cart.getCartId(), product2.getProductID());
//
//        // Act
//        cartService.checkout(cart.getCartId());
//
//        // Assert
//        List<OrderEntity> orders = orderService.getAllOrders();
//        assertThat(orders).hasSize(1);
//
//        OrderEntity order = orders.get(0);
//        assertThat(order.getOrderItems()).hasSize(2);
//        assertThat(order.getAccount()).isEqualTo(cart.getAccount());
//    }

    private CartEntity createCart() {
        AccountEntity account = new AccountEntity();
        account.setUsername("Test User");

        return new CartEntity(account);
    }

    private ProductEntity createProduct() {
        ProductEntity product = new ProductEntity();
        product.setName("Test Product");
        product.setPrice(10.0);
        return product;
    }
}

