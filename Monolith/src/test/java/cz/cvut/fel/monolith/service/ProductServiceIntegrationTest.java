package cz.cvut.fel.monolith.service;

import cz.cvut.fel.monolith.model.ProductEntity;
import cz.cvut.fel.monolith.repository.ProductRepository;
import jakarta.persistence.EntityNotFoundException;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertNull;
import static org.junit.jupiter.api.Assertions.*;
@SpringBootTest
@Transactional
public class ProductServiceIntegrationTest {
    @Autowired
    private ProductService productService;

    @Autowired
    private ProductRepository productRepository;

    @BeforeEach
    void setUp() {
        productRepository.deleteAll();
    }

    @Test
    public void createProduct_savesProductInDatabase() {
        // Arrange
        ProductEntity product = new ProductEntity();
        product.setName("Test Product");
        product.setDescription("Test Description");
        product.setPrice(9.99);

        // Act
        productService.createProduct(product);

        // Assert
        ProductEntity result = productRepository.findById(product.getProductID()).orElse(null);
        assertThat(result).isNotNull();
        assertThat(result).isEqualTo(product);
    }

    @Test
    public void getProduct_existingProduct_returnsProduct() {
        // Arrange
        ProductEntity product = new ProductEntity();
        product.setName("Test Product");
        product.setDescription("Test Description");
        product.setPrice(9.99);
        productRepository.save(product);

        // Act
        ProductEntity result = productService.getProduct(product.getProductID());

        // Assert
        assertThat(result).isNotNull();
        assertThat(result).isEqualTo(product);
    }

    @Test
    public void getProduct_nonExistingProduct_throwsEntityNotFoundException() {
        // Arrange
        Integer productId = 1;

        // Act & Assert
        assertThrows(EntityNotFoundException.class, () -> productService.getProduct(productId));
    }

    @Test
    public void updateProduct_existingProduct_updatesProductInDatabase() {
        // Arrange
        ProductEntity product = new ProductEntity();
        product.setName("Test Product");
        product.setDescription("Test Description");
        product.setPrice(9.99);
        productRepository.save(product);

        String newDescription = "New Description";
        product.setDescription(newDescription);

        // Act
        productService.updateProduct(product);

        // Assert
        ProductEntity result = productRepository.findById(product.getProductID()).orElse(null);
        assertThat(result).isNotNull();
        assertThat(result.getDescription()).isEqualTo(newDescription);
    }

    @Test
    public void updateProduct_nonExistingProduct_throwsEntityNotFoundException() {
        // Arrange
        ProductEntity product = new ProductEntity();
        product.setProductID(1);

        // Act & Assert
        assertThrows(EntityNotFoundException.class, () -> productService.updateProduct(product));
    }

    @Test
    public void deleteProduct_existingProduct_removesProductFromDatabase() {
        // Arrange
        ProductEntity product = new ProductEntity();
        product.setName("Test Product");
        product.setDescription("Test Description");
        product.setPrice(9.99);
        productRepository.save(product);

        ProductEntity result = productService.getProduct(product.getProductID());
        //Assert
        assertThat(product)
                .usingRecursiveComparison()
                .isEqualTo(result);

        // Act
        productService.deleteProduct(product.getProductID());

        // Assert
        assertThrows(EntityNotFoundException.class, () -> productService.updateProduct(product));
    }

    @Test
    public void deleteProduct_nonExistingProduct_throwsEntityNotFoundException() {
        // Arrange
        Integer productId = 1;

        // Act & Assert
        assertThrows(EntityNotFoundException.class, () -> productService.deleteProduct(productId));
    }

    @Test
    public void getListOfAllProducts_returnsListOfProducts() {
        // Arrange
        ProductEntity product1 = new ProductEntity();
        product1.setName("Test Product 1");
        product1.setDescription("Test Description 1");
        product1.setPrice(9.99);
        productRepository.save(product1);

        ProductEntity product2 = new ProductEntity();
        product2.setName("Test Product 2");
        product2.setDescription("Test Description 2");
        product2.setPrice(19.99);
        productRepository.save(product2);

        // Act
        List<ProductEntity> result = productService.getListOfAllProducts();

        // Assert
        assertThat(result).hasSize(2);
        assertThat(result).contains(product1, product2);
    }
}

