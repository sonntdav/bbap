package cz.cvut.fel.monolith.controller;

import cz.cvut.fel.monolith.controller.OrderController;
import cz.cvut.fel.monolith.exception.AccountAlreadyExistsException;
import cz.cvut.fel.monolith.exception.OrderAlreadyPayedException;
import cz.cvut.fel.monolith.model.AccountEntity;
import cz.cvut.fel.monolith.model.OrderEntity;
import cz.cvut.fel.monolith.model.OrderItemEntity;
import cz.cvut.fel.monolith.service.OrderService;
import jakarta.persistence.EntityNotFoundException;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

class OrderControllerMockitoTest {
    @Mock
    private OrderService orderService;

    @InjectMocks
    private OrderController orderController;

    @BeforeEach
    void setUp() {
        MockitoAnnotations.openMocks(this);
    }

    @Test
    void testGetOrder_Success() {
        // Arrange
        Integer orderId = 1;
        OrderEntity order = new OrderEntity();
        when(orderService.getOrder(orderId)).thenReturn(order);

        // Act
        ResponseEntity<OrderEntity> response = orderController.getOrder(orderId);

        // Assert
        assertEquals(HttpStatus.OK, response.getStatusCode());
        assertEquals(order, response.getBody());
        verify(orderService, times(1)).getOrder(orderId);
    }

    @Test
    void testGetOrder_NotFound() {
        // Arrange
        Integer orderId = 1;
        when(orderService.getOrder(orderId)).thenThrow(new EntityNotFoundException("Order not found"));

        // Act
        ResponseEntity<OrderEntity> response = orderController.getOrder(orderId);

        // Assert
        assertEquals(HttpStatus.NOT_FOUND, response.getStatusCode());
        assertNull(response.getBody());
        verify(orderService, times(1)).getOrder(orderId);
    }

    @Test
    void testCreateOrder_Success() {
        // Arrange
        OrderEntity order = new OrderEntity();

        // Act
        ResponseEntity<?> response = orderController.createOrder(order);

        // Assert
        assertEquals(HttpStatus.OK, response.getStatusCode());
        verify(orderService, times(1)).createOrder(order);
    }

    @Test
    void testCreateOrder_Conflict() {
        // Arrange
        OrderEntity order = new OrderEntity();
        doThrow(new AccountAlreadyExistsException("Order with this ID already exists."))
                .when(orderService).createOrder(order);

        // Act
        ResponseEntity<?> response = orderController.createOrder(order);

        // Assert
        assertEquals(HttpStatus.CONFLICT, response.getStatusCode());
        assertEquals("Order with this ID already exists.", response.getBody());
        verify(orderService, times(1)).createOrder(order);
    }

    @Test
    void testGetAllOrders_Success() {
        // Arrange
        List<OrderEntity> orders = new ArrayList<>();
        when(orderService.getAllOrders()).thenReturn(orders);

        // Act
        ResponseEntity<List<OrderEntity>> response = orderController.getAllOrders();

        // Assert
        assertEquals(HttpStatus.OK, response.getStatusCode());
        assertEquals(orders, response.getBody());
        verify(orderService, times(1)).getAllOrders();
    }

    @Test
    void testDeleteOrder_Success() {
        // Arrange
        Integer orderId = 1;

        // Act
        ResponseEntity<?> response = orderController.deleteOrder(orderId);

        // Assert
        assertEquals(HttpStatus.OK, response.getStatusCode());
        verify(orderService, times(1)).deleteOrder(orderId);
    }

    @Test
    void testDeleteOrder_NotFound() {
        // Arrange
        Integer orderId = 1;
        doThrow(new EntityNotFoundException("Order not found"))
                .when(orderService).deleteOrder(orderId);

        // Act
        ResponseEntity<?> response = orderController.deleteOrder(orderId);

        // Assert
        assertEquals(HttpStatus.NOT_FOUND, response.getStatusCode());
        assertEquals("Order not found", response.getBody());
        verify(orderService, times(1)).deleteOrder(orderId);
    }

    @Test
    void testPayForOrder_Success() {
        // Arrange
        Integer orderId = 1;

        // Act
        ResponseEntity<?> response = orderController.payForOrder(orderId);

        // Assert
        assertEquals(HttpStatus.OK, response.getStatusCode());
        verify(orderService, times(1)).payForOrder(orderId);
    }

    @Test
    void testPayForOrder_BadRequest() {
        // Arrange
        Integer orderId = 1;
        doThrow(new OrderAlreadyPayedException("Order was already paid for!"))
                .when(orderService).payForOrder(orderId);

        // Act
        ResponseEntity<?> response = orderController.payForOrder(orderId);

        // Assert
        assertEquals(HttpStatus.BAD_REQUEST, response.getStatusCode());
        assertEquals("Order was already paid for!", response.getBody());
        verify(orderService, times(1)).payForOrder(orderId);
    }

    @Test
    void testFinishOrder_Success() {
        // Arrange
        Integer orderId = 1;

        // Act
        ResponseEntity<?> response = orderController.finishOrder(orderId);

        // Assert
        assertEquals(HttpStatus.OK, response.getStatusCode());
        verify(orderService, times(1)).finishOrder(orderId);
    }

    @Test
    void testFinishOrder_BadRequest() {
        // Arrange
        Integer orderId = 1;
        doThrow(new OrderAlreadyPayedException("Order is already completed or not paid for at all"))
                .when(orderService).finishOrder(orderId);

        // Act
        ResponseEntity<?> response = orderController.finishOrder(orderId);

        // Assert
        assertEquals(HttpStatus.BAD_REQUEST, response.getStatusCode());
        assertEquals("Order is already completed or not paid for at all", response.getBody());
        verify(orderService, times(1)).finishOrder(orderId);
    }

    @Test
    void testGetOrderOwner_Success() {
        // Arrange
        Integer orderId = 1;
        AccountEntity owner = new AccountEntity();
        when(orderService.getOrderOwner(orderId)).thenReturn(owner);

        // Act
        ResponseEntity<?> response = orderController.getOrderOwner(orderId);

        // Assert
        assertEquals(HttpStatus.OK, response.getStatusCode());
        assertEquals(owner, response.getBody());
        verify(orderService, times(1)).getOrderOwner(orderId);
    }

    @Test
    void testGetOrderOwner_NotFound() {
        // Arrange
        Integer orderId = 1;
        when(orderService.getOrderOwner(orderId)).thenThrow(new EntityNotFoundException("Order not found"));

        // Act
        ResponseEntity<?> response = orderController.getOrderOwner(orderId);

        // Assert
        assertEquals(HttpStatus.NOT_FOUND, response.getStatusCode());
        assertEquals("Order not found", response.getBody());
        verify(orderService, times(1)).getOrderOwner(orderId);
    }

    @Test
    void testGetAllItemsInOrder_Success() {
        // Arrange
        Integer orderId = 1;
        List<OrderItemEntity> items = new ArrayList<>();
        when(orderService.getAllItemsInOrder(orderId)).thenReturn(items);

        // Act
        ResponseEntity<List<OrderItemEntity>> response = orderController.getAllItemsInOrder(orderId);

        // Assert
        assertEquals(HttpStatus.OK, response.getStatusCode());
        assertEquals(items, response.getBody());
        verify(orderService, times(1)).getAllItemsInOrder(orderId);
    }

    @Test
    void testGetAllItemsInOrder_NotFound() {
        // Arrange
        Integer orderId = 1;
        when(orderService.getAllItemsInOrder(orderId)).thenThrow(new EntityNotFoundException("Order not found"));

        // Act
        ResponseEntity<List<OrderItemEntity>> response = orderController.getAllItemsInOrder(orderId);

        // Assert
        assertEquals(HttpStatus.NOT_FOUND, response.getStatusCode());
        assertNull(response.getBody());
        verify(orderService, times(1)).getAllItemsInOrder(orderId);
    }
}
