package cz.cvut.fel.monolith.controller;

import cz.cvut.fel.monolith.controller.AccountController;
import cz.cvut.fel.monolith.exception.AccountAlreadyExistsException;
import cz.cvut.fel.monolith.exception.BalanceException;
import cz.cvut.fel.monolith.model.AccountEntity;
import cz.cvut.fel.monolith.service.AccountService;
import jakarta.persistence.EntityNotFoundException;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

class AccountControllerMockitoTest {
    @Mock
    private AccountService accountService;

    @InjectMocks
    private AccountController accountController;

    @BeforeEach
    void setUp() {
        MockitoAnnotations.openMocks(this);
    }

    @Test
    void testGetAccount_Success() {
        // Arrange
        Integer accountId = 1;
        AccountEntity account = new AccountEntity();
        when(accountService.getAccount(accountId)).thenReturn(account);

        // Act
        ResponseEntity<AccountEntity> response = accountController.getAccount(accountId);

        // Assert
        assertEquals(HttpStatus.OK, response.getStatusCode());
        assertEquals(account, response.getBody());
        verify(accountService, times(1)).getAccount(accountId);
    }

    @Test
    void testGetAccount_NotFound() {
        // Arrange
        Integer accountId = 1;
        when(accountService.getAccount(accountId)).thenThrow(new EntityNotFoundException("Account not found"));

        // Act
        ResponseEntity<AccountEntity> response = accountController.getAccount(accountId);

        // Assert
        assertEquals(HttpStatus.NOT_FOUND, response.getStatusCode());
        assertNull(response.getBody());
        verify(accountService, times(1)).getAccount(accountId);
    }

    @Test
    void testCreateAccount_Success() {
        // Arrange
        AccountEntity account = new AccountEntity();

        // Act
        ResponseEntity<?> response = accountController.createAccount(account);

        // Assert
        assertEquals(HttpStatus.CREATED, response.getStatusCode());
        verify(accountService, times(1)).createAccount(account);
    }

    @Test
    void testCreateAccount_Conflict() {
        // Arrange
        AccountEntity account = new AccountEntity();
        doThrow(new AccountAlreadyExistsException("Account already exists"))
                .when(accountService).createAccount(account);

        // Act
        ResponseEntity<?> response = accountController.createAccount(account);

        // Assert
        assertEquals(HttpStatus.CONFLICT, response.getStatusCode());
        assertEquals("Account already exists", response.getBody());
        verify(accountService, times(1)).createAccount(account);
    }

    @Test
    void testUpdateAccount_Success() {
        // Arrange
        Integer accountId = 1;
        AccountEntity account = new AccountEntity();

        // Act
        ResponseEntity<?> response = accountController.updateAccount(accountId, account);

        // Assert
        assertEquals(HttpStatus.OK, response.getStatusCode());
        verify(accountService, times(1)).updateAccount(accountId, account);
    }

    @Test
    void testUpdateAccount_NotFound() {
        // Arrange
        Integer accountId = 1;
        AccountEntity account = new AccountEntity();
        doThrow(new EntityNotFoundException("Account not found"))
                .when(accountService).updateAccount(accountId, account);

        // Act
        ResponseEntity<?> response = accountController.updateAccount(accountId, account);

        // Assert
        assertEquals(HttpStatus.NOT_FOUND, response.getStatusCode());
        assertEquals("Account not found", response.getBody());
        verify(accountService, times(1)).updateAccount(accountId, account);
    }

    @Test
    void testDeleteAccount_Success() {
        // Arrange
        Integer accountId = 1;

        // Act
        ResponseEntity<?> response = accountController.deleteAccount(accountId);

        // Assert
        assertEquals(HttpStatus.OK, response.getStatusCode());
        verify(accountService, times(1)).deleteAccount(accountId);
    }

    @Test
    void testDeleteAccount_NotFound() {
        // Arrange
        Integer accountId = 1;
        doThrow(new EntityNotFoundException("Account not found"))
                .when(accountService).deleteAccount(accountId);

        // Act
        ResponseEntity<?> response = accountController.deleteAccount(accountId);

        // Assert
        assertEquals(HttpStatus.NOT_FOUND, response.getStatusCode());
        assertEquals("Account not found", response.getBody());
        verify(accountService, times(1)).deleteAccount(accountId);
    }

    @Test
    void testGetAccountBalance_Success() {
        // Arrange
        Integer accountId = 1;
        Double balance = 100.0;
        when(accountService.getAccountBalance(accountId)).thenReturn(balance);

        // Act
        ResponseEntity<Double> response = accountController.getAccountBalance(accountId);

        // Assert
        assertEquals(HttpStatus.OK, response.getStatusCode());
        assertEquals(balance, response.getBody());
        verify(accountService, times(1)).getAccountBalance(accountId);
    }

    @Test
    void testGetAccountBalance_NotFound() {
        // Arrange
        Integer accountId = 1;
        when(accountService.getAccountBalance(accountId)).thenThrow(new EntityNotFoundException("Account not found"));

        // Act
        ResponseEntity<Double> response = accountController.getAccountBalance(accountId);

        // Assert
        assertEquals(HttpStatus.NOT_FOUND, response.getStatusCode());
        assertNull(response.getBody());
        verify(accountService, times(1)).getAccountBalance(accountId);
    }

    @Test
    void testWithdrawBalance_Success() {
        // Arrange
        Integer accountId = 1;
        Double amount = 50.0;

        // Act
        ResponseEntity<?> response = accountController.withdrawBalance(accountId, amount);

        // Assert
        assertEquals(HttpStatus.OK, response.getStatusCode());
        verify(accountService, times(1)).withdrawBalance(accountId, amount);
    }

    @Test
    void testWithdrawBalance_NotFound() {
        // Arrange
        Integer accountId = 1;
        Double amount = 50.0;
        doThrow(new EntityNotFoundException("Account not found"))
                .when(accountService).withdrawBalance(accountId, amount);

        // Act
        ResponseEntity<?> response = accountController.withdrawBalance(accountId, amount);

        // Assert
        assertEquals(HttpStatus.BAD_REQUEST, response.getStatusCode());
        assertEquals("Account not found", response.getBody());
        verify(accountService, times(1)).withdrawBalance(accountId, amount);
    }

    @Test
    void testWithdrawBalance_BalanceException() {
        // Arrange
        Integer accountId = 1;
        Double amount = 200.0;
        doThrow(new BalanceException("Not enough balance"))
                .when(accountService).withdrawBalance(accountId, amount);

        // Act
        ResponseEntity<?> response = accountController.withdrawBalance(accountId, amount);

        // Assert
        assertEquals(HttpStatus.BAD_REQUEST, response.getStatusCode());
        assertEquals("Not enough balance", response.getBody());
        verify(accountService, times(1)).withdrawBalance(accountId, amount);
    }

    @Test
    void testAddFunds_Success() {
        // Arrange
        Integer accountId = 1;
        Double amount = 100.0;

        // Act
        ResponseEntity<?> response = accountController.addFunds(accountId, amount);

        // Assert
        assertEquals(HttpStatus.OK, response.getStatusCode());
        verify(accountService, times(1)).addFunds(accountId, amount);
    }

    @Test
    void testAddFunds_NotFound() {
        // Arrange
        Integer accountId = 1;
        Double amount = 100.0;
        doThrow(new EntityNotFoundException("Account not found"))
                .when(accountService).addFunds(accountId, amount);

        // Act
        ResponseEntity<?> response = accountController.addFunds(accountId, amount);

        // Assert
        assertEquals(HttpStatus.NOT_FOUND, response.getStatusCode());
        assertEquals("Account not found", response.getBody());
        verify(accountService, times(1)).addFunds(accountId, amount);
    }
}
