package cz.cvut.fel.monolith.controller;

import cz.cvut.fel.monolith.model.ReviewEntity;
import cz.cvut.fel.monolith.service.ReviewService;
import jakarta.persistence.EntityNotFoundException;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.*;

class ReviewControllerMockitoTest {
    @Mock
    private ReviewService reviewService;

    @InjectMocks
    private ReviewController reviewController;

    @BeforeEach
    void setUp() {
        MockitoAnnotations.openMocks(this);
    }

    @Test
    void testGetReview_Success() {
        ReviewEntity reviewEntity = new ReviewEntity();
        when(reviewService.getReview(1)).thenReturn(reviewEntity);

        ResponseEntity<ReviewEntity> response = reviewController.getReview(1);

        assertEquals(HttpStatus.OK, response.getStatusCode());
        assertEquals(reviewEntity, response.getBody());
        verify(reviewService, times(1)).getReview(1);
    }

    @Test
    void testGetReview_NotFound() {
        when(reviewService.getReview(1)).thenThrow(new EntityNotFoundException("Review not found"));

        ResponseEntity<ReviewEntity> response = reviewController.getReview(1);

        assertEquals(HttpStatus.NOT_FOUND, response.getStatusCode());
        assertEquals(null, response.getBody());
        verify(reviewService, times(1)).getReview(1);
    }

}

