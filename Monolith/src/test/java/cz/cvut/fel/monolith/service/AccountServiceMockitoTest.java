package cz.cvut.fel.monolith.service;

import cz.cvut.fel.monolith.exception.AccountAlreadyExistsException;
import cz.cvut.fel.monolith.exception.BalanceException;
import cz.cvut.fel.monolith.model.AccountEntity;
import cz.cvut.fel.monolith.repository.AccountRepository;
import cz.cvut.fel.monolith.service.impl.AccountServiceImpl;
import jakarta.persistence.EntityNotFoundException;
import org.aspectj.lang.annotation.Before;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.transaction.annotation.Transactional;

import java.util.Optional;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;


@SpringBootTest
@Transactional
public class AccountServiceMockitoTest {
    @Mock
    private AccountRepository accountRepository;
    private AccountService accountService = new AccountServiceImpl(accountRepository);

    @BeforeEach
    public void prepService(){
        this.accountService = new AccountServiceImpl(accountRepository);
    }
    @Test
    public void getAccountRetrievesCallsFindByIDFromDB(){
        final AccountEntity account = getAccount();
        Mockito.when(accountRepository.findById(account.getId())).thenReturn(Optional.of(account));
        AccountEntity result = accountService.getAccount(account.getId());
        assertThat(account)
                .usingRecursiveComparison()
                .isEqualTo(result);

    }
    @Test
    public void getAccountResultsInErrorIfNotInDB(){
        final AccountEntity account = getAccount();
        Mockito.when(accountRepository.findById(account.getId())).thenReturn(Optional.empty());
        assertThrows(EntityNotFoundException.class,() -> accountService.getAccount(account.getId()));
    }
    @Test
    public void createAccountCallsSaveFromRepository(){
        final AccountEntity account = getAccount();
        accountService.createAccount(account);
        verify(accountRepository,times(1)).save(account);
    }
    @Test
    public void creatingThreeAccountsWithNullIDCallsSaveToDBThreeTimes(){
        AccountEntity account = getAccount();
        account.setId(null);
        accountService.createAccount(account);
        accountService.createAccount(account);
        accountService.createAccount(account);
        verify(accountRepository,times(3)).save(account);
    }

    @Test
    public void creatingAccountWithExistingIDResultsInError(){
        AccountEntity account = getAccount();
        account.setId(1);
        Mockito.when(accountRepository.findById(account.getId())).thenReturn(Optional.of(account));
        assertThrows(AccountAlreadyExistsException.class,() -> accountService.createAccount(account));
    }
    @Test
    public void updateAccountCallsSaveToDB(){
        AccountEntity account = getAccount();
        Mockito.when(accountRepository.findById(account.getId())).thenReturn(Optional.of(account));
        accountService.updateAccount(account.getId(),account);
        verify(accountRepository,times(1)).save(account);
    }
    @Test
    public void updateAccountThrowsErrorIfIDsDontMatch(){
        AccountEntity account = getAccount();
        account.setId(1);
        Mockito.when(accountRepository.findById(account.getId())).thenReturn(Optional.of(account));
        Integer newID = account.getId() + 1;
        assertThrows(EntityNotFoundException.class,() -> accountService.updateAccount(newID,account));
    }
    @Test
    public void deleteAccountCallsDeleteFromDB(){
        AccountEntity account = getAccount();
        Mockito.when(accountRepository.findById(account.getId())).thenReturn(Optional.of(account));
        accountService.deleteAccount(account.getId());
        verify(accountRepository,times(1)).delete(account);
    }
    @Test
    public void deletingNonexistentAccountThrowsException(){
        AccountEntity account = getAccount();
        Mockito.when(accountRepository.findById(account.getId())).thenReturn(Optional.empty());
        assertThrows(EntityNotFoundException.class,() -> accountService.deleteAccount(account.getId()));
    }
    @Test
    public void withdrawingFiveHundredFromOneThousandLeavesFiveHundredInAccount(){
        AccountEntity account = getAccount();
        account.setId(1);
        Double withdrawingAmount = 500.00;
        AccountEntity accWithUpdatedBalance = getAccount();
        accWithUpdatedBalance.setBalance(account.getBalance() - withdrawingAmount);
        accWithUpdatedBalance.setId(1);
        Mockito.when(accountRepository.findById(account.getId())).thenReturn(Optional.of(account));
        accountService.withdrawBalance(account.getId(),withdrawingAmount);
        verify(accountRepository,times(1)).save(accWithUpdatedBalance);
    }
    @Test
    public void withdrawingFiveThousandFromOneThousandReturnsError(){
        AccountEntity account = getAccount();
        Double withdrawingAmount = 5000.00;
        Mockito.when(accountRepository.findById(account.getId())).thenReturn(Optional.of(account));
        assertThrows(BalanceException.class,() -> accountService.withdrawBalance(account.getId(),withdrawingAmount));
    }
    @Test
    public void addFundsAddsCorrectlyToRepository(){
        AccountEntity account = getAccount();
        account.setId(1);
        Double addingAmount = 500.00;
        AccountEntity expectedAccountState = getAccount();
        expectedAccountState.setBalance(account.getBalance() + addingAmount);
        expectedAccountState.setId(1);
        Mockito.when(accountRepository.findById(account.getId())).thenReturn(Optional.of(account));
        accountService.addFunds(account.getId(),addingAmount);
        verify(accountRepository,times(1)).save(expectedAccountState);
    }
    @Test
    public void wishlistgetBalanceShowsCorrectBalance(){
        AccountEntity account = getAccount();
        Mockito.when(accountRepository.findById(account.getId())).thenReturn(Optional.of(account));
        Double result = accountService.getAccountBalance(account.getId());
        assertEquals(account.getBalance(),result);
    }

    private AccountEntity getAccount(){
        return new AccountEntity("John Doe","password123","johndoe@example.com","New York","\"123 Main Street",1000.00);
    }


}
