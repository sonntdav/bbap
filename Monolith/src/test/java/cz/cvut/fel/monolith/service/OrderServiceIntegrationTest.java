package cz.cvut.fel.monolith.service;

import cz.cvut.fel.monolith.exception.AccountAlreadyExistsException;
import cz.cvut.fel.monolith.exception.OrderAlreadyPayedException;
import cz.cvut.fel.monolith.model.*;
import cz.cvut.fel.monolith.repository.OrderRepository;
import jakarta.persistence.EntityNotFoundException;
import org.checkerframework.checker.units.qual.A;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.*;

@SpringBootTest
@Transactional
public class OrderServiceIntegrationTest {
    @Autowired
    private OrderService orderService;

    @Autowired
    private OrderRepository orderRepository;

    @BeforeEach
    void setUp() {
        orderRepository.deleteAll();
    }

    @Test
    public void getOrder_existingOrder_returnsOrder() {
        // Arrange
        OrderEntity order = createOrder();
        orderService.createOrder(order);

        // Act
        OrderEntity result = orderService.getOrder(order.getOrderID());

        // Assert
        assertThat(result).isNotNull();
        assertThat(result).isEqualTo(order);
    }

    @Test
    public void getOrder_nonExistingOrder_throwsEntityNotFoundException() {
        // Arrange
        Integer orderId = 1;

        // Act & Assert
        assertThrows(EntityNotFoundException.class, () -> orderService.getOrder(orderId));
    }

    @Test
    public void createOrder_savesOrderInDatabase() {
        // Arrange
        OrderEntity order = createOrder();

        // Act
        orderService.createOrder(order);

        // Assert
        OrderEntity result = orderService.getOrder(order.getOrderID());
        assertThat(result).isNotNull();
        assertThat(result).isEqualTo(order);
    }

    @Test
    public void createOrder_duplicateOrderId_throwsAccountAlreadyExistsException() {
        // Arrange
        OrderEntity order = createOrder();
        orderService.createOrder(order);

        // Act & Assert
        assertThrows(AccountAlreadyExistsException.class, () -> orderService.createOrder(order));
    }

    @Test
    public void deleteOrder_existingOrder_removesOrderFromDatabase() {
        // Arrange
        OrderEntity order = createOrder();
        orderService.createOrder(order);

        // Act
        orderService.deleteOrder(order.getOrderID());

        // Assert
        assertThrows(EntityNotFoundException.class, () -> orderService.getOrder(order.getOrderID()));
    }

    @Test
    public void deleteOrder_nonExistingOrder_throwsEntityNotFoundException() {
        // Arrange
        Integer orderId = 1;

        // Act & Assert
        assertThrows(EntityNotFoundException.class, () -> orderService.deleteOrder(orderId));
    }

    @Test
    public void payForOrder_pendingOrder_paysForOrder() {
        // Arrange
        AccountEntity account = new AccountEntity();
        OrderEntity order = createOrder();
        order.setAccount(account);
        orderService.createOrder(order);

        // Act
        orderService.payForOrder(order.getOrderID());

        // Assert
        OrderEntity result = orderService.getOrder(order.getOrderID());
        assertThat(result.getOrderStatus()).isEqualTo(OrderStatus.IN_PROGRESS);
        assertThat(result.getPayment()).isNotNull();
        assertNotNull(result.getOrderDate());
    }


    @Test
    public void finishOrder_inProgressOrder_marksOrderAsCompleted() {
        // Arrange
        OrderEntity order = createOrder();
        order.setOrderStatus(OrderStatus.IN_PROGRESS);
        orderService.createOrder(order);

        // Act
        orderService.finishOrder(order.getOrderID());

        // Assert
        OrderEntity result = orderService.getOrder(order.getOrderID());
        assertThat(result.getOrderStatus()).isEqualTo(OrderStatus.COMPLETED);
    }

    @Test
    public void finishOrder_completedOrder_throwsOrderAlreadyPayedException() {
        // Arrange
        OrderEntity order = createOrder();
        order.setOrderStatus(OrderStatus.COMPLETED);
        orderService.createOrder(order);

        // Act & Assert
        assertThrows(OrderAlreadyPayedException.class, () -> orderService.finishOrder(order.getOrderID()));
    }

    @Test
    public void getOrderOwner_existingOrder_returnsOrderOwner() {
        // Arrange
        OrderEntity order = createOrder();
        orderService.createOrder(order);

        // Act
        AccountEntity result = orderService.getOrderOwner(order.getOrderID());

        // Assert
        assertThat(result).isNotNull();
        assertThat(result).isEqualTo(order.getAccount());
    }

    @Test
    public void getOrderOwner_nonExistingOrder_throwsEntityNotFoundException() {
        // Arrange
        Integer orderId = 1;

        // Act & Assert
        assertThrows(EntityNotFoundException.class, () -> orderService.getOrderOwner(orderId));
    }


    @Test
    public void getAllItemsInOrder_nonExistingOrder_throwsEntityNotFoundException() {
        // Arrange
        Integer orderId = 1;

        // Act & Assert
        assertThrows(EntityNotFoundException.class, () -> orderService.getAllItemsInOrder(orderId));
    }

    private OrderEntity createOrder() {
        AccountEntity account = new AccountEntity();
        return new OrderEntity(account);
    }
}
