package cz.cvut.fel.accountsvc.kafka.transfer;

import lombok.NoArgsConstructor;

@NoArgsConstructor
public class WithdrawMoneyResponse {
    private Integer accountId;
    private double amount;

    public WithdrawMoneyResponse(Integer accountId, Integer cartId) {
        this.accountId = accountId;
        this.amount = cartId;
    }

    public Integer getAccountId() {
        return accountId;
    }

    public void setAccountId(Integer accountId) {
        this.accountId = accountId;
    }

    public double getAmount() {
        return amount;
    }

    public void setAmount(double amount) {
        this.amount = amount;
    }
}
