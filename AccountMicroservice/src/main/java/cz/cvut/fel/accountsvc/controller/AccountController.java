package cz.cvut.fel.accountsvc.controller;

import cz.cvut.fel.accountsvc.exception.AccountAlreadyExistsException;
import cz.cvut.fel.accountsvc.exception.BalanceException;
import cz.cvut.fel.accountsvc.model.AccountEntity;
import cz.cvut.fel.accountsvc.service.AccountService;
import io.github.resilience4j.circuitbreaker.annotation.CircuitBreaker;
import jakarta.persistence.EntityNotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/account")
public class AccountController {
    private final AccountService accountService;

    @Autowired
    public AccountController(AccountService accountService) {
        this.accountService = accountService;
    }

    @GetMapping
    public ResponseEntity<List<AccountEntity>> getAllAccounts(){
        try {
            List<AccountEntity> allAccounts = accountService.getAllAccounts();
            return ResponseEntity.ok(allAccounts);
        } catch (EntityNotFoundException e) {
            return ResponseEntity.status(HttpStatus.NOT_FOUND).body(null);
        }
    }

    @GetMapping("/byName/{accountName}")
    public ResponseEntity<AccountEntity> getAccountByName(@PathVariable String accountName){
        try {
            AccountEntity account = accountService.getAccount(accountName);
            return ResponseEntity.ok(account);
        } catch (EntityNotFoundException e) {
            return ResponseEntity.status(HttpStatus.NOT_FOUND).body(null);
        }
    }
    @GetMapping("/{accountId}")
    public ResponseEntity<AccountEntity> getAccount(@PathVariable Integer accountId) {
        try {
            AccountEntity account = accountService.getAccount(accountId);
            return ResponseEntity.ok(account);
        } catch (EntityNotFoundException e) {
            return ResponseEntity.status(HttpStatus.NOT_FOUND).body(null);
        }
    }

    @PostMapping
    public ResponseEntity<?> createAccount(@RequestBody AccountEntity account) {
        try {
            accountService.createAccount(account);
            return ResponseEntity.status(HttpStatus.CREATED).build();
        } catch (AccountAlreadyExistsException e) {
            return ResponseEntity.status(HttpStatus.CONFLICT).body(e.getMessage());
        }
    }

    @PutMapping("/{accountId}")
    public ResponseEntity<?> updateAccount(@PathVariable Integer accountId, @RequestBody AccountEntity account) {
        try {
            accountService.updateAccount(accountId, account);
            return ResponseEntity.ok().build();
        } catch (EntityNotFoundException e) {
            return ResponseEntity.status(HttpStatus.NOT_FOUND).body(e.getMessage());
        }
    }

    @DeleteMapping("/{accountId}")
    public ResponseEntity<?> deleteAccount(@PathVariable Integer accountId) {
        try {
            accountService.deleteAccount(accountId);
            return ResponseEntity.ok().build();
        } catch (EntityNotFoundException e) {
            return ResponseEntity.status(HttpStatus.NOT_FOUND).body(e.getMessage());
        }
    }

    @GetMapping("/{accountId}/balance")
    public ResponseEntity<Double> getAccountBalance(@PathVariable Integer accountId) {
        try {
            Double balance = accountService.getAccountBalance(accountId);
            return ResponseEntity.ok(balance);
        } catch (EntityNotFoundException e) {
            return ResponseEntity.status(HttpStatus.NOT_FOUND).body(null);
        }
    }

    @PostMapping("/{accountId}/withdraw/{amount}")
    public ResponseEntity<?> withdrawBalance(
            @PathVariable Integer accountId,
            @PathVariable Double amount
    ) {
        try {
            accountService.withdrawBalance(accountId, amount);
            return ResponseEntity.ok().build();
        } catch (EntityNotFoundException | BalanceException e) {
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(e.getMessage());
        }
    }

    @PostMapping("/{accountId}/addFunds/{amount}")
    public ResponseEntity<?> addFunds(
            @PathVariable Integer accountId,
            @PathVariable Double amount
    ) {
        try {
            accountService.addFunds(accountId, amount);
            return ResponseEntity.ok().build();
        } catch (EntityNotFoundException e) {
            return ResponseEntity.status(HttpStatus.NOT_FOUND).body(e.getMessage());
        }
    }

}
