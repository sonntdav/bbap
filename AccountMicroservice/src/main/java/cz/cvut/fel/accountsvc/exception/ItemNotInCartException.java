package cz.cvut.fel.accountsvc.exception;

public class ItemNotInCartException extends RuntimeException{
    public ItemNotInCartException(String message) {
        super(message);
    }
}