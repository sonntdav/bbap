package cz.cvut.fel.accountsvc.service.impl;

import cz.cvut.fel.accountsvc.exception.AccountAlreadyExistsException;
import cz.cvut.fel.accountsvc.exception.BalanceException;
import cz.cvut.fel.accountsvc.kafka.MyKafkaProducer;
import cz.cvut.fel.accountsvc.model.AccountEntity;

import cz.cvut.fel.accountsvc.repository.AccountRepository;
import cz.cvut.fel.accountsvc.service.AccountService;
import jakarta.persistence.EntityNotFoundException;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Objects;

/**
 * Implementation of the AccountService interface.
 */
@Service
@AllArgsConstructor
@Slf4j
public class AccountServiceImpl implements AccountService {
    private AccountRepository accountRepository;
    private MyKafkaProducer kafkaProducer;

    /**
     * Creates a new account.
     *
     * @param account The account entity to create.
     * @throws AccountAlreadyExistsException If an account with the same ID already exists.
     */
    @Override
    public void createAccount(AccountEntity account) {
        if (account.getId() == null || accountRepository.findById(account.getId()).isEmpty()) {
            if (account.getCartEntity() == null){
                accountRepository.save(account);
                kafkaProducer.accountCreated(account.getId());
                kafkaProducer.accountCreated2(account.getId());
            }else {
                accountRepository.save(account);
            }
        } else {
            throw new AccountAlreadyExistsException("Account with this ID already exists.");
        }
    }

    /**
     * Retrieves an account by its ID.
     *
     * @param id The ID of the account to retrieve.
     * @return The account entity.
     * @throws EntityNotFoundException If the account with the specified ID does not exist.
     */

    @Override
    public AccountEntity getAccount(Integer id) {
        return accountRepository.findById(id).orElseThrow(() -> new EntityNotFoundException("Account was not found. ID of account: " + id));
    }

    @Override
    public AccountEntity getAccount(String username){
        return accountRepository.findByUsername(username);
    }

    @Override
    public List<AccountEntity> getAllAccounts() {
        return accountRepository.findAll();
    }



    /**
     * Updates an existing account.
     *
     * @param id      The ID of the account to update.
     * @param account The updated account entity.
     * @throws EntityNotFoundException If the account with the specified ID does not exist.
     */
    @Override
    public void updateAccount(Integer id, AccountEntity account) {
        final AccountEntity originalAcc = getAccount(id);
        if (!Objects.equals(id, originalAcc.getId())) {
            throw new EntityNotFoundException("Account IDs do not match.");
        } else {
            accountRepository.save(account);
        }
    }

    /**
     * Deletes an account.
     *
     * @param id The ID of the account to delete.
     * @throws EntityNotFoundException If the account with the specified ID does not exist.
     */
    @Override
    public void deleteAccount(Integer id) {
        final AccountEntity originalAcc = getAccount(id);
        accountRepository.delete(originalAcc);
    }

    /**
     * Retrieves the balance of an account.
     *
     * @param id The ID of the account.
     * @return The balance of the account.
     * @throws EntityNotFoundException If the account with the specified ID does not exist.
     */
    @Override
    public Double getAccountBalance(Integer id) {
        final AccountEntity originalAcc = accountRepository.findById(id).orElseThrow(() -> new EntityNotFoundException("Account with this ID does not exist."));
        return originalAcc.getBalance();
    }

    /**
     * Withdraws a specified amount from an account.
     *
     * @param id      The ID of the account.
     * @param balance The amount to withdraw.
     * @throws EntityNotFoundException If the account with the specified ID does not exist.
     * @throws BalanceException        If there is not enough balance in the account.
     */
    @Override
    public void withdrawBalance(Integer id, Double balance) {
        final AccountEntity originalAcc = getAccount(id);
        if (originalAcc.getBalance() < balance) {
            throw new BalanceException("Not enough balance in account.");
        }
        originalAcc.setBalance(originalAcc.getBalance() - balance);
        accountRepository.save(originalAcc);
    }

    /**
     * Adds funds to an account.
     *
     * @param id      The ID of the account.
     * @param balance The amount to add.
     * @throws EntityNotFoundException If the account with the specified ID does not exist.
     */
    @Override
    public void addFunds(Integer id, Double balance) {
        final AccountEntity originalAcc = getAccount(id);
        originalAcc.setBalance(originalAcc.getBalance() + balance);
        accountRepository.save(originalAcc);
    }
}
