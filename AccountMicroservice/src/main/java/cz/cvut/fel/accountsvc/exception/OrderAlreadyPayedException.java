package cz.cvut.fel.accountsvc.exception;

public class OrderAlreadyPayedException extends RuntimeException{
    public OrderAlreadyPayedException(String message) {
        super(message);
    }
}
