package cz.cvut.fel.accountsvc.model;

import com.fasterxml.jackson.annotation.JsonManagedReference;
import jakarta.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Data;

import java.util.ArrayList;
import java.util.List;

@Entity
@Data
@AllArgsConstructor
@Table(name = "account_MC")
public class AccountEntity {
    public AccountEntity() {
        this.orders = new ArrayList<>();
        this.balance = 0.0;
    }

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Integer id;

    @Column(unique = true)
    private String username;
    private String password;
    private String email;
    private String cityName;
    private String street;
    private Double balance;

    private Integer wishlist;
    private Integer cartEntity;

    @ElementCollection(fetch = FetchType.EAGER) // Changed the fetch type to EAGER
    @CollectionTable(name = "account_orders")
    private List<Integer> orders;

    public void addOrder(Integer orderID) {
        if (this.getOrders() == null) {
            this.orders = new ArrayList<>();
        }
        this.orders.add(orderID);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof AccountEntity)) return false;
        return id.equals(((AccountEntity) o).id) && username.equals(((AccountEntity) o).username);
    }

    @Override
    public String toString() {
        return "AccountEntity{" +
                "id=" + id +
                ", username='" + username + '\'' +
                ", password='" + password + '\'' +
                ", email='" + email + '\'' +
                ", cityName='" + cityName + '\'' +
                ", street='" + street + '\'' +
                ", balance=" + balance +
                '}';
    }
}
