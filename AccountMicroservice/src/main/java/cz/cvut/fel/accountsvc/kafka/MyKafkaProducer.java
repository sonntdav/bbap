package cz.cvut.fel.accountsvc.kafka;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.stereotype.Component;

import java.io.Serializable;

@Component
public class MyKafkaProducer implements Serializable {
    private final KafkaTemplate<String, String> kafkaTemplate;

    @Autowired
    public MyKafkaProducer(KafkaTemplate<String, String> kafkaTemplate) {
        this.kafkaTemplate = kafkaTemplate;
    }

    public void accountCreated(Integer accountID){
        kafkaTemplate.send("accountCreated",accountID.toString());
    }

    public void accountCreated2(Integer accountID){
        kafkaTemplate.send("accountCreated2",accountID.toString());
    }
}
