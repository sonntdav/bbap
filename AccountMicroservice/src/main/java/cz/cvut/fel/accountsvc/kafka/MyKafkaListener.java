package cz.cvut.fel.accountsvc.kafka;

import com.fasterxml.jackson.core.JsonProcessingException;
import cz.cvut.fel.accountsvc.kafka.transfer.CartCreatedResponse;
import cz.cvut.fel.accountsvc.kafka.transfer.OrderCreatedResponse;
import cz.cvut.fel.accountsvc.kafka.transfer.WishlistCreatedResponse;
import cz.cvut.fel.accountsvc.kafka.transfer.WithdrawMoneyResponse;
import cz.cvut.fel.accountsvc.model.AccountEntity;
import cz.cvut.fel.accountsvc.service.AccountService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.kafka.annotation.KafkaListener;

@Component
public class MyKafkaListener {
    private final AccountService accountService;
    private final JsonSerializer jsonSerializer = new JsonSerializer();
    private final Object threadLocker = new Object();
    @Autowired
    public MyKafkaListener(AccountService accountService) {
        this.accountService = accountService;
    }

    @KafkaListener(topics = "cartCreated", groupId = "account_group_id")
    public void cartWasCreated(String  jsonCartCreatedResponse){
        CartCreatedResponse cartCreatedResponse;
        try {
            synchronized (threadLocker){
                cartCreatedResponse = jsonSerializer.departmentFromJson(jsonCartCreatedResponse);

                Integer accountID = cartCreatedResponse.getAccountId();
                Integer cartID = cartCreatedResponse.getCartId();

                AccountEntity account = accountService.getAccount(accountID);
                account.setCartEntity(cartID);
                accountService.updateAccount(accountID,account);
            }
        } catch (JsonProcessingException e) {
            e.printStackTrace();
        }
    }

    @KafkaListener(topics = "withdrawMoney", groupId = "account_group_id")
    public void withdrawFromAccount(String  jsonCartCreatedResponse){
        WithdrawMoneyResponse withdrawMoneyResponse;
        try {
            withdrawMoneyResponse = jsonSerializer.withdrawFromJson(jsonCartCreatedResponse);
            Integer accountID = withdrawMoneyResponse.getAccountId();
            double cartID = withdrawMoneyResponse.getAmount();
            accountService.withdrawBalance(accountID,cartID);
        } catch (JsonProcessingException e) {
            e.printStackTrace();
        }
    }

    @KafkaListener(topics = "orderCreated", groupId = "account_group_id")
    public void orderWasCreated(String  jsonCartCreatedResponse){
        OrderCreatedResponse orderCreatedResponse;
        try {
            orderCreatedResponse = jsonSerializer.orderFromJson(jsonCartCreatedResponse);

            Integer accountID = orderCreatedResponse.getAccountID();
            Integer orderID = orderCreatedResponse.getOrderID();

            AccountEntity account = accountService.getAccount(accountID);
            account.addOrder(orderID);
            accountService.updateAccount(accountID,account);
        } catch (JsonProcessingException e) {
            e.printStackTrace();
        }
    }

    @KafkaListener(topics = "wishlistCreated", groupId = "account_group_id")
    public void wishlistWasCreated(String  jsonCartCreatedResponse){
        try {
            synchronized (threadLocker){
                WishlistCreatedResponse wishlistCreatedResponse = jsonSerializer.wishlistFromJson(jsonCartCreatedResponse);

                Integer accountID = wishlistCreatedResponse.getAccountID();
                Integer wishlistID = wishlistCreatedResponse.getWishlistID();

                AccountEntity account = accountService.getAccount(accountID);
                account.setWishlist(wishlistID);
                accountService.updateAccount(accountID,account);
            }
        } catch (JsonProcessingException e) {
            e.printStackTrace();
        }
    }
}
