package cz.cvut.fel.accountsvc.kafka.transfer;

public class WishlistCreatedResponse {
    private Integer accountID;
    private Integer wishlistID;

    public WishlistCreatedResponse(Integer accountID, Integer wishlistID) {
        this.accountID = accountID;
        this.wishlistID = wishlistID;
    }

    public WishlistCreatedResponse() {
    }

    public Integer getAccountID() {
        return accountID;
    }

    public void setAccountID(Integer accountID) {
        this.accountID = accountID;
    }

    public Integer getWishlistID() {
        return wishlistID;
    }

    public void setWishlistID(Integer wishlistID) {
        this.wishlistID = wishlistID;
    }
}
