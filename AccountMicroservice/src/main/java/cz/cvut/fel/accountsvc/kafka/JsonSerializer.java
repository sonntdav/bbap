package cz.cvut.fel.accountsvc.kafka;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import cz.cvut.fel.accountsvc.kafka.transfer.CartCreatedResponse;
import cz.cvut.fel.accountsvc.kafka.transfer.OrderCreatedResponse;
import cz.cvut.fel.accountsvc.kafka.transfer.WishlistCreatedResponse;
import cz.cvut.fel.accountsvc.kafka.transfer.WithdrawMoneyResponse;

public class JsonSerializer {
    private final ObjectMapper obj = new ObjectMapper();

    public String transferClassToJson(Object o) throws JsonProcessingException {
        return obj.writeValueAsString(o);
    }

    public CartCreatedResponse departmentFromJson(String json) throws JsonProcessingException {
        return obj.readValue(json,CartCreatedResponse.class);
    }

    public WithdrawMoneyResponse withdrawFromJson(String json) throws  JsonProcessingException {
        return obj.readValue(json,WithdrawMoneyResponse.class);
    }

    public OrderCreatedResponse orderFromJson(String json) throws JsonProcessingException {
        return obj.readValue(json,OrderCreatedResponse.class);
    }

    public WishlistCreatedResponse wishlistFromJson(String json) throws  JsonProcessingException {
        return obj.readValue(json,WishlistCreatedResponse.class);
    }
}


