package cz.cvut.fel.accountsvc.kafka.transfer;

import java.io.Serializable;

public class OrderCreatedResponse implements Serializable {
    private Integer orderID;
    private Integer accountID;

    public OrderCreatedResponse() {
    }

    public Integer getOrderID() {
        return orderID;
    }

    public void setOrderID(Integer orderID) {
        this.orderID = orderID;
    }

    public Integer getAccountID() {
        return accountID;
    }

    public void setAccountID(Integer accountID) {
        this.accountID = accountID;
    }
}
