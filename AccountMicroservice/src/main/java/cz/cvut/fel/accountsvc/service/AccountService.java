package cz.cvut.fel.accountsvc.service;


import cz.cvut.fel.accountsvc.model.AccountEntity;

import java.util.List;

public interface AccountService {
    AccountEntity getAccount(Integer id);

    AccountEntity getAccount(String username);

    List<AccountEntity> getAllAccounts();

    void createAccount(AccountEntity account);

    void updateAccount(Integer id, AccountEntity account);

    void deleteAccount(Integer id);

    Double getAccountBalance(Integer id);

    void withdrawBalance(Integer id, Double balance);

    void addFunds(Integer id, Double balance);
}
