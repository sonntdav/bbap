package cz.cvut.fel.accountsvc;

import cz.cvut.fel.accountsvc.model.AccountEntity;
import cz.cvut.fel.accountsvc.service.AccountService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.event.ApplicationReadyEvent;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public class StartUp {
    private final AccountService paymentService;

    @Autowired
    public StartUp(AccountService reviewService) {
        this.paymentService = reviewService;
    }

    @EventListener(ApplicationReadyEvent.class)
    public void init() {
        AccountEntity account = new AccountEntity(1,"spring_userName","spring_password","safasf","ASFGASF","ASFGASF",2000.0,1,1,List.of(1));
        paymentService.createAccount(account);
    }

}
