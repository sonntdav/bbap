package cz.cvut.fel.accountsvc.repository;

import cz.cvut.fel.accountsvc.model.AccountEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.transaction.annotation.Transactional;

public interface AccountRepository extends JpaRepository<AccountEntity, Integer> {

    AccountEntity findByUsername(String username);

    @Query(value = "SELECT MAX(id) + 1 FROM AccountEntity")
    Integer getNextAccountId();

    @Modifying
    @Transactional
    @Query(value = "UPDATE AccountEntity SET id = ?1 WHERE id = ?1 - 1")
    void incrementId(Integer id);

    default Integer getAndIncrementNextAccountId() {
        Integer nextId = getNextAccountId();
        incrementId(nextId);
        return nextId;
    }


}
