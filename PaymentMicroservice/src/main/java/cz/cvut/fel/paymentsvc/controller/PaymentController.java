package cz.cvut.fel.paymentsvc.controller;

import cz.cvut.fel.paymentsvc.model.PaymentEntity;
import cz.cvut.fel.paymentsvc.service.PaymentService;
import jakarta.persistence.EntityNotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/payment")
public class PaymentController {
    private final PaymentService paymentService;

    @Autowired
    public PaymentController(PaymentService paymentService) {
        this.paymentService = paymentService;
    }

    @GetMapping("/{paymentID}")
    public ResponseEntity<PaymentEntity> getPayment(@PathVariable Integer paymentID) {
        try {
            PaymentEntity payment = paymentService.getPayment(paymentID);
            return ResponseEntity.ok(payment);
        } catch (EntityNotFoundException e) {
            return ResponseEntity.status(HttpStatus.NOT_FOUND).body(null);
        }
    }

    @GetMapping("/{paymentID}/order")
    public ResponseEntity<Integer> getOrder(@PathVariable Integer paymentID) {
        try {
            Integer order = paymentService.getOrder(paymentID);
            return ResponseEntity.ok(order);
        } catch (EntityNotFoundException e) {
            return ResponseEntity.status(HttpStatus.NOT_FOUND).body(null);
        }
    }
}
