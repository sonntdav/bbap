package cz.cvut.fel.paymentsvc.kafka.transfer;

import java.io.Serializable;

public class PaymentCreatedResponse implements Serializable {
    private Integer orderID;
    private Integer paymentID;

    public PaymentCreatedResponse(Integer orderID, Integer paymentID) {
        this.orderID = orderID;
        this.paymentID = paymentID;
    }

    public Integer getOrderID() {
        return orderID;
    }

    public void setOrderID(Integer orderID) {
        this.orderID = orderID;
    }

    public Integer getPaymentID() {
        return paymentID;
    }

    public void setPaymentID(Integer paymentID) {
        this.paymentID = paymentID;
    }
}
