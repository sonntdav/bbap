package cz.cvut.fel.paymentsvc.kafka;

import com.fasterxml.jackson.core.JsonProcessingException;
import cz.cvut.fel.paymentsvc.kafka.transfer.PaymentCreatedResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.stereotype.Component;


@Component
public class MyKafkaProducer {
    private final JsonSerializer jsonSerializer = new JsonSerializer();
    private final KafkaTemplate<String, String> kafkaTemplate;
    @Autowired
    public MyKafkaProducer(KafkaTemplate<String, String> kafkaTemplate) {
        this.kafkaTemplate = kafkaTemplate;
    }

    public void paymentWasCreated(PaymentCreatedResponse cartCreatedResponse){
        try {
            kafkaTemplate.send("paymentWasCreated", jsonSerializer.transferClassToJson(cartCreatedResponse));
        } catch (JsonProcessingException e) {
            e.printStackTrace();
        }
    }
}
