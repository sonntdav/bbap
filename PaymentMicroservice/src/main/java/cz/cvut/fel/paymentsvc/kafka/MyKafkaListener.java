package cz.cvut.fel.paymentsvc.kafka;

import cz.cvut.fel.paymentsvc.kafka.transfer.PaymentCreatedResponse;
import cz.cvut.fel.paymentsvc.model.PaymentEntity;
import cz.cvut.fel.paymentsvc.service.PaymentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.stereotype.Component;

@Component
public class MyKafkaListener {
    private final PaymentService paymentService;
    private final MyKafkaProducer kafkaProducer;

    @Autowired
    public MyKafkaListener(PaymentService paymentService, MyKafkaProducer kafkaProducer) {
        this.paymentService = paymentService;
        this.kafkaProducer = kafkaProducer;
    }

    @KafkaListener(topics = "createPayment", groupId = "cart_group_id")
    public void accountWasCreated(Integer orderID){
        PaymentEntity payment = paymentService.createPayment(orderID);

        PaymentCreatedResponse cartCreatedResponse = new PaymentCreatedResponse(orderID, payment.getPaymentID());
        kafkaProducer.paymentWasCreated(cartCreatedResponse);
    }
}
