package cz.cvut.fel.paymentsvc.model;

import jakarta.persistence.*;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;

@Entity
@Data
@NoArgsConstructor
@Table(name = "Payment_MC")
public class PaymentEntity {
    public PaymentEntity(Integer order){
        this.orderEntity = order;
        this.paymentDate = new Date();
    }
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Integer paymentID;
    private Date paymentDate;

    private Integer orderEntity;

}
