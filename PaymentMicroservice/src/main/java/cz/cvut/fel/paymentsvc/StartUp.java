package cz.cvut.fel.paymentsvc;

import cz.cvut.fel.paymentsvc.service.PaymentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.event.ApplicationReadyEvent;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

@Component
public class StartUp {
    private final PaymentService paymentService;

    @Autowired
    public StartUp(PaymentService reviewService) {
        this.paymentService = reviewService;
    }

    @EventListener(ApplicationReadyEvent.class)
    public void init() {
        paymentService.createPayment(1);
    }

}
