package cz.cvut.fel.paymentsvc.repository;

import cz.cvut.fel.paymentsvc.model.PaymentEntity;
import org.springframework.data.jpa.repository.JpaRepository;

public interface PaymentRepository extends JpaRepository<PaymentEntity, Integer> {
}
