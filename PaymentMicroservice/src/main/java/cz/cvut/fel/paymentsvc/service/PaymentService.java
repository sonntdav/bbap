package cz.cvut.fel.paymentsvc.service;


import cz.cvut.fel.paymentsvc.model.PaymentEntity;

public interface PaymentService {
    PaymentEntity getPayment(Integer paymentID);

    Integer getOrder(Integer paymentID);

    PaymentEntity createPayment(Integer orderID);
}

