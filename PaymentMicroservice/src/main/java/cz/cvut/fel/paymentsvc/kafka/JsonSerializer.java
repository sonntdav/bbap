package cz.cvut.fel.paymentsvc.kafka;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import cz.cvut.fel.paymentsvc.kafka.transfer.PaymentCreatedResponse;

public class JsonSerializer {
    private final ObjectMapper obj = new ObjectMapper();

    public PaymentCreatedResponse departmentFromJson(String json) throws JsonProcessingException {
        return obj.readValue(json, PaymentCreatedResponse.class);
    }

    public String transferClassToJson(Object o) throws JsonProcessingException {
        return obj.writeValueAsString(o);
    }


}


