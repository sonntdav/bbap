package cz.cvut.fel.paymentsvc.service.impl;

import cz.cvut.fel.paymentsvc.model.PaymentEntity;
import cz.cvut.fel.paymentsvc.repository.PaymentRepository;
import cz.cvut.fel.paymentsvc.service.PaymentService;
import jakarta.persistence.EntityNotFoundException;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

/**
 * Implementation of the {@link PaymentService} interface that provides methods
 * to manage payments.
 */
@Service
@AllArgsConstructor
@Slf4j
public class PaymentServiceImpl implements PaymentService {
    private PaymentRepository paymentRepository;

    /**
     * Retrieves the payment with the specified ID.
     *
     * @param paymentID the ID of the payment to retrieve
     * @return the {@link PaymentEntity} object with the specified ID
     * @throws EntityNotFoundException if the payment with the specified ID does not exist
     */
    @Override
    public PaymentEntity getPayment(Integer paymentID) throws EntityNotFoundException {
        return paymentRepository.findById(paymentID).orElseThrow(() -> new EntityNotFoundException("Payment with this ID not found."));
    }


    /**
     * Retrieves the order associated with the payment with the specified ID.
     *
     * @param paymentID the ID of the payment
     * @return the ID Of order object associated with the payment
     * @throws EntityNotFoundException if the payment with the specified ID does not exist
     */
    @Override
    public Integer getOrder(Integer paymentID) throws EntityNotFoundException {
        PaymentEntity payment = getPayment(paymentID);
        return payment.getOrderEntity();
    }

    @Override
    public PaymentEntity createPayment(Integer orderID) {
        PaymentEntity payment = new PaymentEntity(orderID);
        paymentRepository.save(payment);
        return payment;
    }
}
