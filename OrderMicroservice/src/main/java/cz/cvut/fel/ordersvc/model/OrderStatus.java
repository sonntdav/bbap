package cz.cvut.fel.ordersvc.model;

public enum OrderStatus {
    PENDING,
    IN_PROGRESS,
    COMPLETED
}
