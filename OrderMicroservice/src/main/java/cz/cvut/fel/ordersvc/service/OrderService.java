package cz.cvut.fel.ordersvc.service;



import cz.cvut.fel.ordersvc.model.OrderEntity;
import cz.cvut.fel.ordersvc.model.OrderItemEntity;

import java.util.List;

public interface OrderService {
    OrderEntity getOrder(Integer id);

    void createOrder(OrderEntity order);

    List<OrderEntity> getAllOrders();

    void deleteOrder(Integer id);

    void payForOrder(Integer id);

    void finishOrder(Integer id);

    void updateOrder(OrderEntity order);

    List<OrderItemEntity> getAllItemsInOrder(Integer id);
}

