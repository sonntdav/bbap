package cz.cvut.fel.ordersvc.kafka.transfer;

import lombok.NoArgsConstructor;

import java.io.Serializable;

@NoArgsConstructor
public class WithdrawMoneyResponse implements Serializable {
    private Integer accountId;
    private double amount;

    public WithdrawMoneyResponse(Integer accountId, double amount) {
        this.accountId = accountId;
        this.amount = amount;
    }

    public Integer getAccountId() {
        return accountId;
    }

    public void setAccountId(Integer accountId) {
        this.accountId = accountId;
    }

    public double getAmount() {
        return amount;
    }

    public void setAmount(double amount) {
        this.amount = amount;
    }
}
