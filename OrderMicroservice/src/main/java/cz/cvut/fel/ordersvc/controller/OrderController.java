package cz.cvut.fel.ordersvc.controller;


import cz.cvut.fel.ordersvc.exception.BalanceException;
import cz.cvut.fel.ordersvc.exception.EntityAlreadyExistsException;
import cz.cvut.fel.ordersvc.exception.OrderAlreadyPayedException;
import cz.cvut.fel.ordersvc.model.OrderEntity;
import cz.cvut.fel.ordersvc.model.OrderItemEntity;
import cz.cvut.fel.ordersvc.service.OrderService;
import jakarta.persistence.EntityNotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/order")
public class OrderController {
    private final OrderService orderService;

    @Autowired
    public OrderController(OrderService orderService) {
        this.orderService = orderService;
    }

    @GetMapping("/{orderId}")
    public ResponseEntity<OrderEntity> getOrder(@PathVariable Integer orderId) {
        try {
            OrderEntity order = orderService.getOrder(orderId);
            return ResponseEntity.ok(order);
        } catch (EntityNotFoundException e) {
            return ResponseEntity.status(HttpStatus.NOT_FOUND).body(null);
        }
    }

    @PostMapping
    public ResponseEntity<?> createOrder(@RequestBody OrderEntity order) {
        try {
            orderService.createOrder(order);
            return ResponseEntity.ok().build();
        } catch (EntityAlreadyExistsException e) {
            return ResponseEntity.status(HttpStatus.CONFLICT).body(e.getMessage());
        }
    }

    @GetMapping
    public ResponseEntity<List<OrderEntity>> getAllOrders() {
        List<OrderEntity> orders = orderService.getAllOrders();
        return ResponseEntity.ok(orders);
    }

    @DeleteMapping("/{orderId}")
    public ResponseEntity<?> deleteOrder(@PathVariable Integer orderId) {
        try {
            orderService.deleteOrder(orderId);
            return ResponseEntity.ok().build();
        } catch (EntityNotFoundException e) {
            return ResponseEntity.status(HttpStatus.NOT_FOUND).body(e.getMessage());
        }
    }

    @PostMapping("/{orderId}/pay")
    public ResponseEntity<?> payForOrder(@PathVariable Integer orderId) {
        try {
            orderService.payForOrder(orderId);
            return ResponseEntity.ok().build();
        } catch (OrderAlreadyPayedException | BalanceException e) {
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(e.getMessage());
        }

    }

    @PostMapping("/{orderId}/finish")
    public ResponseEntity<?> finishOrder(@PathVariable Integer orderId) {
        try {
            orderService.finishOrder(orderId);
            return ResponseEntity.ok().build();
        } catch (OrderAlreadyPayedException e) {
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(e.getMessage());
        }
    }


    @GetMapping("/{orderId}/items")
    public ResponseEntity<List<OrderItemEntity>> getAllItemsInOrder(@PathVariable Integer orderId) {
        try {
            return ResponseEntity.ok(orderService.getAllItemsInOrder(orderId));
        } catch (EntityNotFoundException e) {
            return ResponseEntity.status(HttpStatus.NOT_FOUND).body(null);
        }
    }
}
