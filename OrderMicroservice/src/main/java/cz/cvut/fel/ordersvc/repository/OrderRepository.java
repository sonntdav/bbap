package cz.cvut.fel.ordersvc.repository;

import cz.cvut.fel.ordersvc.model.OrderEntity;
import org.springframework.data.jpa.repository.JpaRepository;

public interface OrderRepository extends JpaRepository<OrderEntity, Integer> {
}
