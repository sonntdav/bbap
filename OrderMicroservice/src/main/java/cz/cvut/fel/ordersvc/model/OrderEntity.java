package cz.cvut.fel.ordersvc.model;

import com.fasterxml.jackson.annotation.JsonManagedReference;
import jakarta.persistence.*;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Entity
@Data
@NoArgsConstructor
@Table(name = "Order_MC")
public class OrderEntity {
    public OrderEntity(Integer account, List<OrderItemEntity> orderItems) {
        this.account = account;
        this.orderItems = orderItems;
        this.orderStatus = OrderStatus.PENDING;
        this.orderDate = new Date();
        this.orderItems = new ArrayList<>();
    }
    public OrderEntity(Integer account){
        this.account = account;
        this.orderStatus = OrderStatus.PENDING;
        this.orderDate = new Date();
        this.orderItems = new ArrayList<>();
    }

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Integer orderID;
    private Date orderDate;
    @Enumerated
    private OrderStatus orderStatus;

    private Integer payment;
    private Integer account;

    @JsonManagedReference
    @OneToMany(mappedBy = "order",cascade = CascadeType.ALL)
    private List<OrderItemEntity> orderItems;


    public void setPaymentEntity(Integer payment){
        this.payment = payment;
        this.orderStatus = OrderStatus.IN_PROGRESS;
    }

    public void markAsCompleted(){
        this.orderStatus = OrderStatus.COMPLETED;
    }

    @Override
    public String toString() {
        return "OrderEntity{" +
                "orderID=" + orderID +
                '}';
    }

}
