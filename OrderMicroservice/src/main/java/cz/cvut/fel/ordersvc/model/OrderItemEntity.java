package cz.cvut.fel.ordersvc.model;

import com.fasterxml.jackson.annotation.JsonBackReference;
import jakarta.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Entity
@Data
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "OrderItemMC")
public class OrderItemEntity {
    public OrderItemEntity(Integer amount, Integer productEntity, OrderEntity order) {
        this.amount = amount;
        this.productEntity = productEntity;
        this.order = order;
    }

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Integer orderItemID;
    private Integer amount;
    private Integer productEntity;

    @JsonBackReference
    @ManyToOne
    private OrderEntity order;


}
