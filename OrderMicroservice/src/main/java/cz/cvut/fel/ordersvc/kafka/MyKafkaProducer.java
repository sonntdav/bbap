package cz.cvut.fel.ordersvc.kafka;

import com.fasterxml.jackson.core.JsonProcessingException;
import cz.cvut.fel.ordersvc.kafka.transfer.OrderCreatedResponse;
import cz.cvut.fel.ordersvc.kafka.transfer.WithdrawMoneyResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.stereotype.Component;


@Component
public class MyKafkaProducer {
    private final JsonSerializer jsonSerializer = new JsonSerializer();
    private final KafkaTemplate<String, String> kafkaTemplate;
    @Autowired
    public MyKafkaProducer(KafkaTemplate<String, String> kafkaTemplate) {
        this.kafkaTemplate = kafkaTemplate;
    }

    public void withdrawMoney(Integer accID, double amount){
        WithdrawMoneyResponse withdrawMoneyResponse = new WithdrawMoneyResponse(accID,amount);
        try {
            kafkaTemplate.send("withdrawMoney", jsonSerializer.transferClassToJson(withdrawMoneyResponse));
        } catch (JsonProcessingException e) {
            e.printStackTrace();
        }
    }

    public void createPayment(Integer orderID){
        kafkaTemplate.send("createPayment",orderID.toString());
    }

    public void orderCreated(OrderCreatedResponse orderInfo) {
        try {
            kafkaTemplate.send("orderCreated",jsonSerializer.transferClassToJson(orderInfo));
        } catch (JsonProcessingException e) {
            e.printStackTrace();
        }
    }
}
