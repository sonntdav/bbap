package cz.cvut.fel.ordersvc;

import cz.cvut.fel.ordersvc.model.OrderEntity;
import cz.cvut.fel.ordersvc.model.OrderItemEntity;
import cz.cvut.fel.ordersvc.service.OrderService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.event.ApplicationReadyEvent;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public class StartUp {
    private final OrderService paymentService;

    @Autowired
    public StartUp(OrderService reviewService) {
        this.paymentService = reviewService;
    }

    @EventListener(ApplicationReadyEvent.class)
    public void init() {
        OrderEntity order = new OrderEntity(1);
        OrderItemEntity orderItemEntity = new OrderItemEntity(2,1,order);
        order.setOrderItems(List.of(orderItemEntity));
        paymentService.createOrder(order);
    }

}
