package cz.cvut.fel.ordersvc.kafka;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import cz.cvut.fel.ordersvc.kafka.transfer.CartResponse;
import cz.cvut.fel.ordersvc.kafka.transfer.PaymentCreatedResponse;

import java.util.List;

public class JsonSerializer {
    private final ObjectMapper obj = new ObjectMapper();

    public String transferClassToJson(Object o) throws JsonProcessingException {
        return obj.writeValueAsString(o);
    }

    public PaymentCreatedResponse paymentCreatedResponse(String json) throws JsonProcessingException {
        return obj.readValue(json,PaymentCreatedResponse.class);
    }

    public CartResponse getCartResponse(String json) throws  JsonProcessingException {
        return obj.readValue(json,CartResponse.class);
    }

}


