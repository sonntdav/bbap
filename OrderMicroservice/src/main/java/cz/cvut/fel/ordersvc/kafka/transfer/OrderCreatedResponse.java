package cz.cvut.fel.ordersvc.kafka.transfer;

import java.io.Serializable;

public class OrderCreatedResponse implements Serializable {
    private Integer orderID;
    private Integer accountID;

    public OrderCreatedResponse() {
    }

    public OrderCreatedResponse(Integer orderID, Integer accountID) {
        this.orderID = orderID;
        this.accountID = accountID;
    }

    public Integer getOrderID() {
        return orderID;
    }

    public void setOrderID(Integer orderID) {
        this.orderID = orderID;
    }

    public Integer getAccountID() {
        return accountID;
    }

    public void setAccountID(Integer accountID) {
        this.accountID = accountID;
    }
}
