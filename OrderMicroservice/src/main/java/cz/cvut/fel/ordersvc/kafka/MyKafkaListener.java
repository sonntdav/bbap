package cz.cvut.fel.ordersvc.kafka;

import com.fasterxml.jackson.core.JsonProcessingException;
import cz.cvut.fel.ordersvc.kafka.transfer.CartResponse;
import cz.cvut.fel.ordersvc.kafka.transfer.PaymentCreatedResponse;
import cz.cvut.fel.ordersvc.kafka.transfer.ProductItemResponse;
import cz.cvut.fel.ordersvc.model.OrderEntity;
import cz.cvut.fel.ordersvc.model.OrderItemEntity;
import cz.cvut.fel.ordersvc.model.OrderStatus;
import cz.cvut.fel.ordersvc.service.OrderService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.stereotype.Component;


@Component
public class MyKafkaListener {
    private final OrderService orderService;
    private final JsonSerializer jsonSerializer = new JsonSerializer();

    @Autowired
    public MyKafkaListener(OrderService cartService, MyKafkaProducer kafkaProducer) {
        this.orderService = cartService;
    }

    @KafkaListener(topics = "paymentWasCreated", groupId = "payment_group_id")
    public void paymentWasCreated(String paymentCreatedJson){
        try {
            PaymentCreatedResponse paymentCreatedResponse = jsonSerializer.paymentCreatedResponse(paymentCreatedJson);
            OrderEntity order = orderService.getOrder(paymentCreatedResponse.getOrderID());
            order.setPayment(paymentCreatedResponse.getPaymentID());
            order.setOrderStatus(OrderStatus.IN_PROGRESS);
            orderService.updateOrder(order);
        } catch (JsonProcessingException e) {
            e.printStackTrace();
        }
    }

    @KafkaListener(topics = "createOrder", groupId = "payment_group_id")
    public void createOrder(String cartResponseJson){
        try {
            CartResponse cartResponse = jsonSerializer.getCartResponse(cartResponseJson);
            OrderEntity order = new OrderEntity(cartResponse.getAccID());
            for (ProductItemResponse productItemResponse : cartResponse.getProductItemResponses()) {
                OrderItemEntity orderItemEntity = new OrderItemEntity(productItemResponse.getAmount(), productItemResponse.getProductEntityID(),order);
                order.getOrderItems().add(orderItemEntity);
            }
            orderService.createOrder(order);
        } catch (JsonProcessingException e) {
            e.printStackTrace();
        }
    }
}
