package cz.cvut.fel.ordersvc.service.impl;

import com.fasterxml.jackson.core.JsonProcessingException;
import cz.cvut.fel.ordersvc.exception.BalanceException;
import cz.cvut.fel.ordersvc.exception.EntityAlreadyExistsException;
import cz.cvut.fel.ordersvc.exception.OrderAlreadyPayedException;
import cz.cvut.fel.ordersvc.kafka.JsonSerializer;
import cz.cvut.fel.ordersvc.kafka.MyKafkaProducer;
import cz.cvut.fel.ordersvc.kafka.transfer.OrderCreatedResponse;
import cz.cvut.fel.ordersvc.model.OrderEntity;
import cz.cvut.fel.ordersvc.model.OrderItemEntity;
import cz.cvut.fel.ordersvc.model.OrderStatus;
import cz.cvut.fel.ordersvc.repository.OrderRepository;
import cz.cvut.fel.ordersvc.service.OrderService;
import jakarta.persistence.EntityNotFoundException;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import java.util.List;
import java.util.stream.Collectors;

/**
 * Implementation of the {@link OrderService} interface that provides methods
 * to manage orders.
 */
@Service
@Slf4j
public class OrderServiceImpl implements OrderService {
    private final OrderRepository orderRepository;
    private final RestTemplate restTemplate;
    private final MyKafkaProducer kafkaProducer;
//    private final String productUrl = "http://product-service-service:8082/product/valueOfProducts";
//    private final String accountUrl = "http://account-svc-service:8081/account";
    private final String productUrl = "http://localhost:8082/product/valueOfProducts";
    private final String accountUrl = "http://localhost:8081/account";
    @Autowired
    public OrderServiceImpl(OrderRepository orderRepository, RestTemplate restTemplate, MyKafkaProducer kafkaProducer) {
        this.orderRepository = orderRepository;
        this.restTemplate = restTemplate;
        this.kafkaProducer = kafkaProducer;
    }

    /**
     * Retrieves the order with the specified ID.
     *
     * @param id the ID of the order to retrieve
     * @return the {@link OrderEntity} object with the specified ID
     * @throws EntityNotFoundException if the order with the specified ID does not exist
     */
    @Override
    public OrderEntity getOrder(Integer id) throws EntityNotFoundException {
        return orderRepository.findById(id).orElseThrow(() -> new EntityNotFoundException("Order with this ID does not exist. Order ID: " + id));
    }


    /**
     * Creates a new order.
     *
     * @param order the {@link OrderEntity} object representing the order to create
     * @throws EntityAlreadyExistsException if an order with the same ID already exists
     */
    @Override
    public void createOrder(OrderEntity order) throws EntityAlreadyExistsException {
        if (order.getOrderID() == null || orderRepository.findById(order.getOrderID()).isEmpty()) {
            orderRepository.save(order);
            kafkaProducer.orderCreated(new OrderCreatedResponse(order.getOrderID(),order.getAccount()));
        } else {
            throw new EntityAlreadyExistsException("Order with this ID already exists.");
        }
    }

    /**
     * Retrieves all orders.
     *
     * @return a list of all {@link OrderEntity} objects
     */
    @Override
    public List<OrderEntity> getAllOrders() {
        return orderRepository.findAll();
    }

    /**
     * Deletes the order with the specified ID.
     *
     * @param id the ID of the order to delete
     * @throws EntityNotFoundException if the order with the specified ID does not exist
     */
    @Override
    public void deleteOrder(Integer id) throws EntityNotFoundException {
        final OrderEntity order = getOrder(id);
        orderRepository.delete(order);
    }

    /**
     * Pays for the order with the specified ID.
     *
     * @param id the ID of the order to pay for
     * @throws OrderAlreadyPayedException if the order was already paid for
     * @throws BalanceException if the account does not have enough balance to pay for the order
     */
    @Override
    public void payForOrder(Integer id) throws OrderAlreadyPayedException {
        final OrderEntity order = getOrder(id);
        if (order.getOrderStatus() == OrderStatus.PENDING) {
            Integer accountID = order.getAccount();
            int accBalance = getAccountBalance(accountID);
            double priceOfOrder = getTotalPriceOfOrder(order);
            if (accBalance >= priceOfOrder){
                //async
                kafkaProducer.withdrawMoney(accountID,priceOfOrder);
                kafkaProducer.createPayment(order.getOrderID());
            }
            else {
                throw new BalanceException("Not enough money to pay for order!");
            }
            return;
        }
        throw new OrderAlreadyPayedException("Order was already paid for!");
    }
    private Integer getAccountBalance(Integer accountID) throws EntityNotFoundException{
        ResponseEntity<Integer> responseEntity = restTemplate.getForEntity(accountUrl + "/" + accountID + "/balance",Integer.class);
        if (responseEntity.getStatusCode() == HttpStatus.NOT_FOUND){
            throw  new EntityNotFoundException(String.valueOf(responseEntity.getBody()));
        }
        return responseEntity.getBody();
    }

    /**
     * Marks the order with the specified ID as completed.
     *
     * @param id the ID of the order to mark as completed
     * @throws OrderAlreadyPayedException if the order is already completed or not paid for at all
     */
    @Override
    public void finishOrder(Integer id) throws OrderAlreadyPayedException {
        final OrderEntity order = getOrder(id);
        if (order.getOrderStatus() == OrderStatus.IN_PROGRESS) {
            order.markAsCompleted();
            orderRepository.save(order);
            return;
        }
        throw new OrderAlreadyPayedException("Order is already completed or not paid for at all");
    }

    @Override
    public void updateOrder(OrderEntity order) {
        orderRepository.save(order);
    }


    /**
     * Retrieves all items in the order with the specified ID.
     *
     * @param id the ID of the order
     * @return a list of {@link OrderItemEntity} objects representing the items in the order
     * @throws EntityNotFoundException if the order with the specified ID does not exist
     */
    @Override
    public List<OrderItemEntity> getAllItemsInOrder(Integer id) throws EntityNotFoundException {
        return getOrder(id).getOrderItems();
    }

    private double getTotalPriceOfOrder(OrderEntity order){
        List<Integer> productsInOrder = order.getOrderItems().stream().map(OrderItemEntity::getProductEntity).collect(Collectors.toList());
        ResponseEntity<Double> doubleResponseEntity = restTemplate.postForEntity(productUrl, productsInOrder, Double.class);
        return doubleResponseEntity.getBody();
    }
}
