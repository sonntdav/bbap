package cz.cvut.fel.ordersvc.exception;

public class OrderAlreadyPayedException extends RuntimeException{
    public OrderAlreadyPayedException(String message) {
        super(message);
    }
}
