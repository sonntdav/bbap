from locust import FastHttpUser, task, between
import json
import uuid
import random

class AccountControllerUser(FastHttpUser):
    #-------------------------------------------------------------------------------  Account ------------------------------------------------------------------------------ 
    #-------------------------------------------------------------------------------  Account ------------------------------------------------------------------------------ 
    #-------------------------------------------------------------------------------  Account ------------------------------------------------------------------------------ 


    wait_time = between(1, 3)  # Wait between 1 and 3 seconds between tasks

    @task(20)
    def get_account_by_name(self):
        response = self.client.get("/api/account/byName/spring_name", name="GetAccByName")
        print(f"Get Account By Name - Status Code: {response.status_code}")

    @task(20)
    def get_all_accounts(self):
        response = self.client.get("/api/account", name="Get All Accounts")
        print(f"Get All Accounts - Status Code: {response.status_code}")

    @task(20)
    def get_account(self):
        response = self.client.get("/api/account/1", name="Get Account")
        """ print(f"Get Account - Status Code: {response.status_code}") """

    # @task
    # def create_account(self):
    #     headers = {'Content-Type': 'application/json'}
    #     username = f"user_{uuid.uuid4().hex[:8]}"  # Generate a random username using uuid4
    #     account_data = {
    #         "username": username,
    #         "password": "hashed_password_here",
    #         "email": "john.doe@example.com",
    #         "cityName": "New York",
    #         "street": "123 Main Street",
    #         "balance": 10000.0
    #     }
    #     response = self.client.post("/api/account", data=json.dumps(account_data), headers=headers, name="Create Account")
        """ print(f"Create Account - Status Code: {response.status_code}") """

    # @task
    # def update_account(self):
    #     headers = {'Content-Type': 'application/json'}
    #     account_data = {
    #         # Provide valid account data in the AccountDto format for the update
    #         "accountName": "updated_account",
    #         "balance": 200.0
    #     }
    #     response = self.client.put("/api/account/1", data=json.dumps(account_data), headers=headers, name="Update Account")
        #print(f"Update Account - Status Code: {response.status_code}")

    @task
    def get_account_balance(self):
        response = self.client.get("/api/account/1/balance", name="Get Account Balance")
        #print(f"Get Account Balance - Status Code: {response.status_code}")

    # @task
    # def add_funds(self):
    #     response = self.client.post("/api/account/1/addFunds/1", name="Add Funds")
    #     #print(f"Add Funds - Status Code: {response.status_code}")


    #-------------------------------------------------------------------------------  Cart ------------------------------------------------------------------------------ 
    #-------------------------------------------------------------------------------  Cart ------------------------------------------------------------------------------ 
    #-------------------------------------------------------------------------------  Cart ------------------------------------------------------------------------------ 

    @task
    def get_cart(self):
        self.client.get(f"/api/cart/1", name="Get Cart")

    # @task
    # def add_product_to_cart(self):
    #     self.client.post(f"/api/cart/1/products/1", name="Add Product to Cart")

    @task
    def get_all_items_in_cart(self):
        self.client.get(f"/api/cart/1/items", name="Get All Items in Cart")

    #-------------------------------------------------------------------------------  Order ------------------------------------------------------------------------------ 
    #-------------------------------------------------------------------------------  Order ------------------------------------------------------------------------------ 
    #-------------------------------------------------------------------------------  Order ------------------------------------------------------------------------------ 


    @task
    def get_order(self):
        self.client.get(f"/api/order/1", name="Get Order")

    # @task
    # def create_order(self):
    #     # Replace this with valid order data in the OrderDto format
    #     order_data = {
    #         "account": 1,
    #         "payment": 1,
    #         "orderItems": [
    #             {
    #                 "amount": 1,
    #                 "productEntity": 1
    #             },
    #             {
    #                 "amount": 2,
    #                 "productEntity": 2
    #             }
    #         ]
    #     }
    #     self.client.post("/api/order", json=order_data, name="Create Order")

    @task
    def get_all_items_in_order(self):
        self.client.get(f"/api/order/1/items", name="Get All Items in Order")


    #-------------------------------------------------------------------------------  Payment ------------------------------------------------------------------------------ 
    #-------------------------------------------------------------------------------  Payment ------------------------------------------------------------------------------ 
    #-------------------------------------------------------------------------------  Payment ------------------------------------------------------------------------------ 


    @task
    def get_payment(self):
        self.client.get(f"/api/payment/1", name="Get Payment")

    @task
    def get_order_of_payment(self):
        self.client.get(f"/api/payment/1/order", name="Get Order of Payment")



    #-------------------------------------------------------------------------------  Product ------------------------------------------------------------------------------ 
    #-------------------------------------------------------------------------------  Product ------------------------------------------------------------------------------ 
    #-------------------------------------------------------------------------------  Product ------------------------------------------------------------------------------ 


    @task
    def get_product(self):
        self.client.get(f"/api/product/1", name="Get Product")

    @task
    def get_product_by_name(self):
        self.client.get(f"/api/product/byName/spring_product", name="Get Product by Name")

    # @task
    # def create_product(self):
    #     productName = f"product_{uuid.uuid4().hex[:8]}"  # Generate a random username using uuid4
    #     # Replace this with valid product data in the ProductDto format
    #     product_data = {
    #         "name": productName,
    #         "description": "This is an example product.",
    #         "price": 9.99
    #     }
    #     self.client.post("/api/product", json=product_data, name="Create Product")

    # @task
    # def update_product(self):
    #     productName = "spring_product"
    #     # Replace this with valid product data in the ProductDto format
    #     product_data = {
    #         "name": productName,
    #         "description": "This is an example product. with updated description",
    #         "price": 9.99
    #     }
    #     self.client.put(f"/api/product/1", json=product_data, name="Update Product")


    #-------------------------------------------------------------------------------  Review ------------------------------------------------------------------------------ 
    #-------------------------------------------------------------------------------  Review ------------------------------------------------------------------------------ 
    #-------------------------------------------------------------------------------  Review ------------------------------------------------------------------------------     

    

    @task
    def get_review(self):
        self.client.get(f"/api/review/1", name="Get Review")

    # @task
    # def add_review(self):
    #     review_message = "This is a test review."  # Replace with an actual review message
    #     self.client.post(f"/api/review/1/1", json=review_message, name="Add Review")

    # @task
    # def update_review(self):
    #     review_data = {
    #         "reviewID": 1,
    #         "message": "This is a updated review message.",
    #         "author": 1,
    #         "product": 1
    #     }
    #     self.client.put("/api/review", json=review_data, name="Update Review")



    #-------------------------------------------------------------------------------  Wishlist ------------------------------------------------------------------------------ 
    #-------------------------------------------------------------------------------  Wishlist ------------------------------------------------------------------------------ 
    #-------------------------------------------------------------------------------  Wishlist ------------------------------------------------------------------------------
    

    # @task
    # def add_item_to_wishlist(self):
    #     response = self.client.post(f"/api/wishlist/1/items/1", name="Add Item to Wishlist")
    #     #print(f"Add Item to Wishlist - Status Code: {response.status_code}")

    @task
    def get_wishlist_details(self):
        response = self.client.get(f"/api/wishlist/1", name="Get Wishlist Details")
        #print(f"Get Wishlist Details - Status Code: {response.status_code}")